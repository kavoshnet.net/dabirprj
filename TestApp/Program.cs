﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using DataLayer.DataAccessLayer;
namespace TestApp
{
    class Program
    {
        static void Main()
        {
            using (DataBaseContext _edbcontext = new DataBaseContext())
            {
                //var userdata = (from e in _edbcontext.Users.Include("MyTells")
                //            //where e.ID == 1
                //            orderby e.ID ascending
                //            select e).ToList();
                foreach (var itemud in _edbcontext.Users.Include("MyTells"))
                {

                    Console.WriteLine($@"{itemud.Name}");
                    foreach (var itemmt in itemud.MyTells)
                    {
                        Console.WriteLine($@"   {itemmt.Number}");
                    }
                }


                //var telldata = (from e in _edbcontext.Tells.Include("MyUser")
                //                    //where e.ID == 1
                //                orderby e.ID ascending
                //                select e).ToList();
                foreach (var itemtd in _edbcontext.Tells)
                {
                    Console.WriteLine($@"{itemtd.Number}    {itemtd.MyUser.Name}");
                }


            }
            Console.ReadKey();
        }
    }
}
