﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
namespace TestApp
{
    class DataBaseContext:DbContext
    {
        public DataBaseContext() : base("TestDB")
        {
            Configuration.LazyLoadingEnabled = false;
        }
        static DataBaseContext()
        {
            Database.SetInitializer(new initializer());
        }
        public DbSet<User> Users { get; set; }
        public DbSet<Tell> Tells { get; set; }
    }
   
}
