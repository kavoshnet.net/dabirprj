﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
namespace TestApp
{
    class initializer : DropCreateDatabaseIfModelChanges<DataBaseContext>
    {
        protected override void Seed(DataBaseContext context)
        {
            context.Users.Add(new User
            {
                Name = "Test1",
                MyTells = new List<Tell>
                {
                        new Tell
                        {
                            Number = "1111"
                        },
                         new Tell
                        {
                            Number = "2222"
                        },
                          new Tell
                        {
                            Number = "3333"
                        }
                }
            });
            context.Users.Add(new User
            {
                Name = "Test2",
                MyTells = new List<Tell>
                {
                        new Tell
                        {
                            Number = "1111"
                        },
                         new Tell
                        {
                            Number = "2222"
                        },
                          new Tell
                        {
                            Number = "3333"
                        },
                          new Tell
                        {
                            Number = "4444"
                        }

                }
            });
            base.Seed(context);
        }
    }
}
