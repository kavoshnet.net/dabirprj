﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestApp
{
    class User
    {
        public User()
        {
            MyTells = new HashSet<Tell>();
        }
        public int ID { get; set; }
        public string Name { get; set; }
        public virtual ICollection<Tell> MyTells { get; set; }

        //[InverseProperty("BookAsFirstAuthor")]
        //[InverseProperty("BookAsSecondAuthor")]
        //public Author SecondtAuthor { get; set; }
    }
}
