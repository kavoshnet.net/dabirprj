﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestApp
{
    class Tell
    {
        public int ID { get; set; }
        public string Number { get; set; }
        //public int UserID { get; set; }
        public int UserID { get; set; }
        [ForeignKey("UserID")]
        public virtual User MyUser { get; set; }
        
        //[InverseProperty("FirstAuthor")]
        //[InverseProperty("SecondAuthor")]
        //public virtual ICollection<Book> BooksAsSecondAuthor { get; set; }
    }
}
