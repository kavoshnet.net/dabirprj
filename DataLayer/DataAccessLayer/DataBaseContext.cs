﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using DomainClass.DomainModel;
using DomainClass.DomainModel.Mapping;

namespace DataLayer.DataAccessLayer
{
    public class DataBaseContext:DbContext
    {
        public DataBaseContext():base("DabirDB")
        {
            Configuration.LazyLoadingEnabled = true;
        }
        static DataBaseContext()
        {
            Database.SetInitializer(new DataBaseContextInitializer());
        }
        public DbSet<Letter> Letters { get; set; }
        public DbSet<Reference> References { get; set; }
        public DbSet<Office> Offices { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Page> Pages { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new LetterMap());
            modelBuilder.Configurations.Add(new ReferenceMap());
            modelBuilder.Configurations.Add(new OfficeMap());
            modelBuilder.Configurations.Add(new UserMap());
            modelBuilder.Configurations.Add(new RoleMap());
            modelBuilder.Configurations.Add(new PageMap());
        }
    }
}
