namespace DataLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            var script =
            @"

                        CREATE VIEW [dbo].[V_tbl_letter]
            AS
            SELECT        dbo.Reference.ID, dbo.Reference.Subject, dbo.Reference.DateReference, dbo.Reference.DateResponse, dbo.Reference.LetterID, dbo.Reference.UserIDSender, dbo.Reference.UserIDReceiver, 
                                     dbo.Letter.Number AS [Letter.Number], dbo.Letter.DateLetter AS [Letter.DateLetter], dbo.Letter.Subject AS [Letter.Subject], dbo.Letter.DateRegister AS [Letter.DateRegister], dbo.Letter.[Content] AS [Letter.Content], 
                                     dbo.Letter.Attribute AS [Letter.Attribute], dbo.Letter.KindLetter AS [Letter.KindLetter], dbo.Letter.PageID AS [Letter.PageID], dbo.Letter.SenderOfficeID AS [Letter.SenderOfficeID], 
                                     dbo.Letter.ReceiverOfficeID AS [Letter.ReceiverOfficeID], SenderOffice.NameOffice AS [SenderOffice.NameOffice], ReceiverOffice.NameOffice AS [ReceiverOffice.NameOffice]
            FROM            dbo.Reference INNER JOIN
                                     dbo.Letter ON dbo.Reference.LetterID = dbo.Letter.ID INNER JOIN
                                     dbo.Office AS SenderOffice ON dbo.Letter.SenderOfficeID = SenderOffice.ID INNER JOIN
                                     dbo.Office AS ReceiverOffice ON dbo.Letter.ReceiverOfficeID = ReceiverOffice.ID";
            using (var ctx = new DataAccessLayer.DataBaseContext())
            {
                ctx.Database.ExecuteSqlCommand(script);
            }
        }

        public override void Down()
        {
            using (var ctx = new DataAccessLayer.DataBaseContext())
            {
                ctx.Database.ExecuteSqlCommand("DROP VIEW [dbo].[V_tbl_letter]");
            }
        }
    }
}
