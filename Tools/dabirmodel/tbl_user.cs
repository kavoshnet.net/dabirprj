//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace dabirmodel
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_user
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbl_user()
        {
            this.tbl_erja_fer = new HashSet<tbl_erja>();
            this.tbl_erja_gir = new HashSet<tbl_erja>();
            this.tbl_letter = new HashSet<tbl_letter>();
        }
    
        public decimal id_user { get; set; }
        public string fname_user { get; set; }
        public string lname_user { get; set; }
        public string shmeli_user { get; set; }
        public string username_user { get; set; }
        public string password_user { get; set; }
        public Nullable<decimal> role_id { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_erja> tbl_erja_fer { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_erja> tbl_erja_gir { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_letter> tbl_letter { get; set; }
        public virtual tbl_role tbl_role { get; set; }
    }
}
