using System;
using System.Collections.Generic;

namespace dabirmodel.Models
{
    public partial class tbl_erja
    {
        public decimal id_erja { get; set; }
        public string moz_erja { get; set; }
        public Nullable<decimal> letter_erja_id { get; set; }
        public Nullable<decimal> ufer_erja_id { get; set; }
        public Nullable<decimal> ugir_erja_id { get; set; }
        public string dte_erja { get; set; }
        public string dte_req { get; set; }
        public virtual tbl_letter tbl_letter { get; set; }
        public virtual tbl_user tbl_user { get; set; }
        public virtual tbl_user tbl_user1 { get; set; }
    }
}
