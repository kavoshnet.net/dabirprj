using System;
using System.Collections.Generic;

namespace dabirmodel.Models
{
    public partial class tbl_role
    {
        public tbl_role()
        {
            this.tbl_user = new List<tbl_user>();
        }

        public decimal id_role { get; set; }
        public string role_name { get; set; }
        public string role_name_fa { get; set; }
        public virtual ICollection<tbl_user> tbl_user { get; set; }
    }
}
