using System;
using System.Collections.Generic;

namespace dabirmodel.Models
{
    public partial class tbl_letter
    {
        public tbl_letter()
        {
            this.tbl_erja = new List<tbl_erja>();
        }

        public decimal id_letter { get; set; }
        public string sh_letter { get; set; }
        public string dte_letter { get; set; }
        public string moz_letter { get; set; }
        public Nullable<decimal> fer_id_letter { get; set; }
        public Nullable<decimal> gir_id_letter { get; set; }
        public Nullable<decimal> user_id_letter { get; set; }
        public Nullable<decimal> page_id_letter { get; set; }
        public string dte_sabt_letter { get; set; }
        public string con_letter { get; set; }
        public string att_letter { get; set; }
        public Nullable<int> kind_letter { get; set; }
        public virtual ICollection<tbl_erja> tbl_erja { get; set; }
        public virtual tbl_fergir tbl_fergir { get; set; }
        public virtual tbl_fergir tbl_fergir1 { get; set; }
        public virtual tbl_page tbl_page { get; set; }
        public virtual tbl_user tbl_user { get; set; }
    }
}
