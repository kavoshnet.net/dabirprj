using System;
using System.Collections.Generic;

namespace dabirmodel.Models
{
    public partial class tbl_page
    {
        public tbl_page()
        {
            this.tbl_letter = new List<tbl_letter>();
        }

        public decimal id_page { get; set; }
        public string page_name { get; set; }
        public Nullable<int> page_width { get; set; }
        public Nullable<int> page_height { get; set; }
        public Nullable<int> orientation { get; set; }
        public Nullable<int> margin_top { get; set; }
        public Nullable<int> margin_bottom { get; set; }
        public Nullable<int> margin_right { get; set; }
        public Nullable<int> margin_left { get; set; }
        public string header { get; set; }
        public string message { get; set; }
        public string body { get; set; }
        public string footer { get; set; }
        public virtual ICollection<tbl_letter> tbl_letter { get; set; }
    }
}
