using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace dabirmodel.Models.Mapping
{
    public class tbl_fergirMap : EntityTypeConfiguration<tbl_fergir>
    {
        public tbl_fergirMap()
        {
            // Primary Key
            this.HasKey(t => t.id_fergir);

            // Properties
            // Table & Column Mappings
            this.ToTable("tbl_fergir");
            this.Property(t => t.id_fergir).HasColumnName("id_fergir");
            this.Property(t => t.nam_fergir).HasColumnName("nam_fergir");
        }
    }
}
