using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace dabirmodel.Models.Mapping
{
    public class V_tbl_letterMap : EntityTypeConfiguration<V_tbl_letter>
    {
        public V_tbl_letterMap()
        {
            // Primary Key
            this.HasKey(t => t.id_letter);

            // Properties
            this.Property(t => t.id_letter)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.dte_letter)
                .IsFixedLength()
                .HasMaxLength(10);

            this.Property(t => t.dte_sabt_letter)
                .IsFixedLength()
                .HasMaxLength(10);

            // Table & Column Mappings
            this.ToTable("V_tbl_letter");
            this.Property(t => t.id_letter).HasColumnName("id_letter");
            this.Property(t => t.sh_letter).HasColumnName("sh_letter");
            this.Property(t => t.dte_letter).HasColumnName("dte_letter");
            this.Property(t => t.moz_letter).HasColumnName("moz_letter");
            this.Property(t => t.fer_id_letter).HasColumnName("fer_id_letter");
            this.Property(t => t.gir_id_letter).HasColumnName("gir_id_letter");
            this.Property(t => t.user_id_letter).HasColumnName("user_id_letter");
            this.Property(t => t.dte_sabt_letter).HasColumnName("dte_sabt_letter");
            this.Property(t => t.con_letter).HasColumnName("con_letter");
            this.Property(t => t.att_letter).HasColumnName("att_letter");
            this.Property(t => t.page_id_letter).HasColumnName("page_id_letter");
            this.Property(t => t.kind_letter).HasColumnName("kind_letter");
            this.Property(t => t.tbl_fer_nam_fergir).HasColumnName("tbl_fer.nam_fergir");
            this.Property(t => t.tbl_gir_nam_fergir).HasColumnName("tbl_gir.nam_fergir");
        }
    }
}
