using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace dabirmodel.Models.Mapping
{
    public class tbl_letterMap : EntityTypeConfiguration<tbl_letter>
    {
        public tbl_letterMap()
        {
            // Primary Key
            this.HasKey(t => t.id_letter);

            // Properties
            this.Property(t => t.dte_letter)
                .IsFixedLength()
                .HasMaxLength(10);

            this.Property(t => t.dte_sabt_letter)
                .IsFixedLength()
                .HasMaxLength(10);

            // Table & Column Mappings
            this.ToTable("tbl_letter");
            this.Property(t => t.id_letter).HasColumnName("id_letter");
            this.Property(t => t.sh_letter).HasColumnName("sh_letter");
            this.Property(t => t.dte_letter).HasColumnName("dte_letter");
            this.Property(t => t.moz_letter).HasColumnName("moz_letter");
            this.Property(t => t.fer_id_letter).HasColumnName("fer_id_letter");
            this.Property(t => t.gir_id_letter).HasColumnName("gir_id_letter");
            this.Property(t => t.user_id_letter).HasColumnName("user_id_letter");
            this.Property(t => t.page_id_letter).HasColumnName("page_id_letter");
            this.Property(t => t.dte_sabt_letter).HasColumnName("dte_sabt_letter");
            this.Property(t => t.con_letter).HasColumnName("con_letter");
            this.Property(t => t.att_letter).HasColumnName("att_letter");
            this.Property(t => t.kind_letter).HasColumnName("kind_letter");

            // Relationships
            this.HasOptional(t => t.tbl_fergir)
                .WithMany(t => t.tbl_letter)
                .HasForeignKey(d => d.fer_id_letter);
            this.HasOptional(t => t.tbl_fergir1)
                .WithMany(t => t.tbl_letter1)
                .HasForeignKey(d => d.gir_id_letter);
            this.HasOptional(t => t.tbl_page)
                .WithMany(t => t.tbl_letter)
                .HasForeignKey(d => d.page_id_letter);
            this.HasOptional(t => t.tbl_user)
                .WithMany(t => t.tbl_letter)
                .HasForeignKey(d => d.user_id_letter);

        }
    }
}
