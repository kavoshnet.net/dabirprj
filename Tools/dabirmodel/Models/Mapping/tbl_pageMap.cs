using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace dabirmodel.Models.Mapping
{
    public class tbl_pageMap : EntityTypeConfiguration<tbl_page>
    {
        public tbl_pageMap()
        {
            // Primary Key
            this.HasKey(t => t.id_page);

            // Properties
            // Table & Column Mappings
            this.ToTable("tbl_page");
            this.Property(t => t.id_page).HasColumnName("id_page");
            this.Property(t => t.page_name).HasColumnName("page_name");
            this.Property(t => t.page_width).HasColumnName("page_width");
            this.Property(t => t.page_height).HasColumnName("page_height");
            this.Property(t => t.orientation).HasColumnName("orientation");
            this.Property(t => t.margin_top).HasColumnName("margin_top");
            this.Property(t => t.margin_bottom).HasColumnName("margin_bottom");
            this.Property(t => t.margin_right).HasColumnName("margin_right");
            this.Property(t => t.margin_left).HasColumnName("margin_left");
            this.Property(t => t.header).HasColumnName("header");
            this.Property(t => t.message).HasColumnName("message");
            this.Property(t => t.body).HasColumnName("body");
            this.Property(t => t.footer).HasColumnName("footer");
        }
    }
}
