using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace dabirmodel.Models.Mapping
{
    public class tbl_userMap : EntityTypeConfiguration<tbl_user>
    {
        public tbl_userMap()
        {
            // Primary Key
            this.HasKey(t => t.id_user);

            // Properties
            this.Property(t => t.username_user)
                .IsRequired();

            this.Property(t => t.password_user)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("tbl_user");
            this.Property(t => t.id_user).HasColumnName("id_user");
            this.Property(t => t.fname_user).HasColumnName("fname_user");
            this.Property(t => t.lname_user).HasColumnName("lname_user");
            this.Property(t => t.shmeli_user).HasColumnName("shmeli_user");
            this.Property(t => t.username_user).HasColumnName("username_user");
            this.Property(t => t.password_user).HasColumnName("password_user");
            this.Property(t => t.role_id).HasColumnName("role_id");

            // Relationships
            this.HasOptional(t => t.tbl_role)
                .WithMany(t => t.tbl_user)
                .HasForeignKey(d => d.role_id);

        }
    }
}
