using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace dabirmodel.Models.Mapping
{
    public class tbl_erjaMap : EntityTypeConfiguration<tbl_erja>
    {
        public tbl_erjaMap()
        {
            // Primary Key
            this.HasKey(t => t.id_erja);

            // Properties
            this.Property(t => t.dte_erja)
                .IsFixedLength()
                .HasMaxLength(10);

            this.Property(t => t.dte_req)
                .IsFixedLength()
                .HasMaxLength(10);

            // Table & Column Mappings
            this.ToTable("tbl_erja");
            this.Property(t => t.id_erja).HasColumnName("id_erja");
            this.Property(t => t.moz_erja).HasColumnName("moz_erja");
            this.Property(t => t.letter_erja_id).HasColumnName("letter_erja_id");
            this.Property(t => t.ufer_erja_id).HasColumnName("ufer_erja_id");
            this.Property(t => t.ugir_erja_id).HasColumnName("ugir_erja_id");
            this.Property(t => t.dte_erja).HasColumnName("dte_erja");
            this.Property(t => t.dte_req).HasColumnName("dte_req");

            // Relationships
            this.HasOptional(t => t.tbl_letter)
                .WithMany(t => t.tbl_erja)
                .HasForeignKey(d => d.letter_erja_id);
            this.HasOptional(t => t.tbl_user)
                .WithMany(t => t.tbl_erja)
                .HasForeignKey(d => d.ufer_erja_id);
            this.HasOptional(t => t.tbl_user1)
                .WithMany(t => t.tbl_erja1)
                .HasForeignKey(d => d.ugir_erja_id);

        }
    }
}
