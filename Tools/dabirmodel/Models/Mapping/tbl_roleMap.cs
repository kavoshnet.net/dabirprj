using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace dabirmodel.Models.Mapping
{
    public class tbl_roleMap : EntityTypeConfiguration<tbl_role>
    {
        public tbl_roleMap()
        {
            // Primary Key
            this.HasKey(t => t.id_role);

            // Properties
            // Table & Column Mappings
            this.ToTable("tbl_role");
            this.Property(t => t.id_role).HasColumnName("id_role");
            this.Property(t => t.role_name).HasColumnName("role_name");
            this.Property(t => t.role_name_fa).HasColumnName("role_name_fa");
        }
    }
}
