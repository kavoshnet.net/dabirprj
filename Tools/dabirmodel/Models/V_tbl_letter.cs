using System;
using System.Collections.Generic;

namespace dabirmodel.Models
{
    public partial class V_tbl_letter
    {
        public decimal id_letter { get; set; }
        public string sh_letter { get; set; }
        public string dte_letter { get; set; }
        public string moz_letter { get; set; }
        public Nullable<decimal> fer_id_letter { get; set; }
        public Nullable<decimal> gir_id_letter { get; set; }
        public Nullable<decimal> user_id_letter { get; set; }
        public string dte_sabt_letter { get; set; }
        public string con_letter { get; set; }
        public string att_letter { get; set; }
        public Nullable<decimal> page_id_letter { get; set; }
        public Nullable<int> kind_letter { get; set; }
        public string tbl_fer_nam_fergir { get; set; }
        public string tbl_gir_nam_fergir { get; set; }
    }
}
