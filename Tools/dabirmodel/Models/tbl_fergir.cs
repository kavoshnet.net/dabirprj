using System;
using System.Collections.Generic;

namespace dabirmodel.Models
{
    public partial class tbl_fergir
    {
        public tbl_fergir()
        {
            this.tbl_letter = new List<tbl_letter>();
            this.tbl_letter1 = new List<tbl_letter>();
        }

        public decimal id_fergir { get; set; }
        public string nam_fergir { get; set; }
        public virtual ICollection<tbl_letter> tbl_letter { get; set; }
        public virtual ICollection<tbl_letter> tbl_letter1 { get; set; }
    }
}
