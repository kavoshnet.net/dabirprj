using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using dabirmodel.Models.Mapping;

namespace dabirmodel.Models
{
    public partial class dabirdbContext : DbContext
    {
        static dabirdbContext()
        {
            Database.SetInitializer<dabirdbContext>(null);
        }

        public dabirdbContext()
            : base("Name=dabirdbContext")
        {
        }

        public DbSet<sysdiagram> sysdiagrams { get; set; }
        public DbSet<tbl_erja> tbl_erja { get; set; }
        public DbSet<tbl_fergir> tbl_fergir { get; set; }
        public DbSet<tbl_letter> tbl_letter { get; set; }
        public DbSet<tbl_page> tbl_page { get; set; }
        public DbSet<tbl_role> tbl_role { get; set; }
        public DbSet<tbl_user> tbl_user { get; set; }
        public DbSet<V_tbl_letter> V_tbl_letter { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new sysdiagramMap());
            modelBuilder.Configurations.Add(new tbl_erjaMap());
            modelBuilder.Configurations.Add(new tbl_fergirMap());
            modelBuilder.Configurations.Add(new tbl_letterMap());
            modelBuilder.Configurations.Add(new tbl_pageMap());
            modelBuilder.Configurations.Add(new tbl_roleMap());
            modelBuilder.Configurations.Add(new tbl_userMap());
            modelBuilder.Configurations.Add(new V_tbl_letterMap());
        }
    }
}
