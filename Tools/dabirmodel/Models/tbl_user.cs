using System;
using System.Collections.Generic;

namespace dabirmodel.Models
{
    public partial class tbl_user
    {
        public tbl_user()
        {
            this.tbl_erja = new List<tbl_erja>();
            this.tbl_erja1 = new List<tbl_erja>();
            this.tbl_letter = new List<tbl_letter>();
        }

        public decimal id_user { get; set; }
        public string fname_user { get; set; }
        public string lname_user { get; set; }
        public string shmeli_user { get; set; }
        public string username_user { get; set; }
        public string password_user { get; set; }
        public Nullable<decimal> role_id { get; set; }
        public virtual ICollection<tbl_erja> tbl_erja { get; set; }
        public virtual ICollection<tbl_erja> tbl_erja1 { get; set; }
        public virtual ICollection<tbl_letter> tbl_letter { get; set; }
        public virtual tbl_role tbl_role { get; set; }
    }
}
