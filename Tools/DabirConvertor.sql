use DabirDB1;

insert into DabirDB1.dbo.[Page] select
 			[page_name]
           ,[page_width]
           ,[page_height]
           ,[orientation]
           ,[margin_top]
           ,[margin_bottom]
           ,[margin_right]
           ,[margin_left]
           ,[header]
           ,[message]
           ,[body]
           ,[footer]
            from dabirdb.dbo.tbl_page;

insert into DabirDB1.dbo.Office select
 			[nam_fergir]
            from dabirdb.dbo.tbl_fergir;
			
set identity_insert Office on;
insert into DabirDB.dbo.Office (ID,NameOffice) values (0,'-----')
            
set identity_insert Office off;	
	
insert into DabirDB1.dbo.[Role] select
 			[role_name]
           ,[role_name_fa],
		   0,0,0,0,0,0,0,0,0,0,0,0,0
            from dabirdb.dbo.tbl_role;

insert into DabirDB1.dbo.[User] select
 			[fname_user]
           ,[lname_user]
           ,[shmeli_user]
           ,[username_user]
           ,[password_user]
           ,[role_id]
            from dabirdb.dbo.tbl_user;

update dabirdb.dbo.tbl_letter set fer_id_letter = fer_id_letter +1,gir_id_letter = gir_id_letter +1;

set identity_insert letter on;


insert into DabirDB1.dbo.Letter (
		 [ID]
		,[Number]
		,[DateLetter]
		,[Subject]
		,[DateRegister]
		,[Content]
		,[Attribute]
		,[KindLetter]
		,[PageID]
		,[SenderOfficeID]
		,[ReceiverOfficeID]) 
	  select
		 [id_letter]
 		,[sh_letter]
		,[dte_letter]
		,[moz_letter]
		,[dte_sabt_letter]
		,[con_letter]
		,[att_letter]
		,[kind_letter]
		,[page_id_letter]
		,[fer_id_letter]+1
		,[gir_id_letter]+1
      from dabirdb.dbo.tbl_letter;
set identity_insert letter off;

set identity_insert Reference on;

insert into DabirDB1.dbo.Reference (
			[ID]	
		   ,[Subject]
           ,[DateReference]
           ,[DateResponse]
           ,[LetterID]
           ,[UserIDSender]
           ,[UserIDReceiver])
		   select
		    [id_erja]
 		   ,[moz_erja]
		   ,[dte_erja]
		   ,[dte_req]
           ,[letter_erja_id]
           ,[ufer_erja_id]
           ,[ugir_erja_id]
            from dabirdb.dbo.tbl_erja;
set identity_insert Reference off;
