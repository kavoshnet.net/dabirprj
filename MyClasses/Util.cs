﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyClasses
{
    public static class Util
    {
        static Util()
        {

        }
        public static string SubString(object Text, object Length)
        {
            string StringText = Text.ToString();
            int StringLength = int.Parse(Length.ToString());
            if (StringText.Length > StringLength)
            {
                return StringText.Substring(0, StringLength) + "...";
            }
            else
            {
                return StringText;
            }
        }
    }
}
