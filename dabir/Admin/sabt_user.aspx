﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPageAdmin.Master" AutoEventWireup="true" CodeBehind="sabt_user.aspx.cs" Inherits="dabir.Admin.sabt_user" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!---------------------------------------Display Errore Message-------------------------------------------->
    <%-- Start Dialog Function                 --------------%>
    <%-- Dialog Sabt user    --%>
    <script type="text/javascript">
        // cal ajax methode for save data
        $(document).ready(function () {
            $("#sabt_user_dialog").dialog(
          {
              autoOpen: false,
              resizable: false,
              modal: true,
              bgiframe: true,
              width: 700,
              height: 300,
              title: "ثبت کاربران",
              open: function (event, ui) { $(".ui-dialog-titlebar-close").hide(); },
              show: {
                  effect: "fade",
                  duration: 50
              },
              hide: {
                  effect: "fade",
                  duration: 50
              },
              close: function (event, ui) {
                  var UpdatePanel1 = '<%=UpdatePanel1.ClientID%>';
                  if (UpdatePanel1 != null)
                      __doPostBack(UpdatePanel1, '');
              },
              buttons: {
                  'ثبت و ادامه': function () {
                      $.ajax({
                          type: 'POST',
                          contentType: "application/json; charset=utf-8",
                          url: 'sabt_user.aspx/sabt_user_method',
                          data: "{" +
                              "'ID':'" + $('#hfid_user').val() + "'," +
                              "'Fname':'" + $('#txt_fname_user').val() + "'," +
                               "'Lname':'" + $('#txt_lname_user').val() + "'," +
                               "'Shmeli':'" + $('#txt_shmeli_user').val() + "'," +
                               "'UserName':'" + $('#txt_username_user').val() + "'," +
                               "'Password':'" + $('#txt_password_user').val() + "'," +
                               "'RoleID':'" + $('#ddl_role_id_user').val() + "'" +
                           "}",
                          async: false,
                          success: function (response) {
                              if (response.d != '-1') {
                                  alert("کاربر با کد << " + response.d + " >> ثبت شد");
                                  var UpdatePanel1 = '<%=UpdatePanel1.ClientID%>';
                                  if (UpdatePanel1 != null)
                                      __doPostBack(UpdatePanel1, '');
                                  jQuery('#sabt_user_dialog').dialog('close');
                                  return true;
                              }
                              else {
                                  alert("خطا در ثبت داده ها");
                                  return false;
                              }

                          },
                          error: function ()
                          { alert("خطا در ثبت داده ها"); jQuery('#sabt_user_dialog').dialog('close'); return false; }
                      });

                  },
                  'انصراف': function () {
                      var UpdatePanel1 = '<%=UpdatePanel1.ClientID%>';
                      if (UpdatePanel1 != null)
                          __doPostBack(UpdatePanel1, '');
                      jQuery('#sabt_user_dialog').dialog('close');
                  }
              }
          });
        });
    </script>
    <%-- Dialog Delete      --%>
    <script type="text/javascript">
        // cal ajax methode for delete data
        $(document).ready(function () {
            $('#delete_user_dialog').dialog(
{
    autoOpen: false,
    resizable: true,
    modal: true,
    bgiframe: true,
    width: 300,
    height: 175,
    title: "حذف داده ها",
    open: function (event, ui) { $(".ui-dialog-titlebar-close").hide(); },
    show: {
        effect: "fade",
        duration: 50
    },
    hide: {
        effect: "explode",
        //effect: "fade",
        duration: 50
    },
    close: function (event, ui) {
        var UpdatePanel1 = '<%=UpdatePanel1.ClientID%>';
        if (UpdatePanel1 != null)
            __doPostBack(UpdatePanel1, '');
        //$("#form1").data("bootstrapValidator").resetForm(true);
    },
    buttons: {
        'حذف': function () {
            $.ajax({
                type: 'POST',
                contentType: "application/json; charset=utf-8",
                url: 'sabt_user.aspx/delete_user_method',
                data: "{'ID':'" + $('#hfid_user').val() + "'" +
                           "}",
                async: false,
                success: function (response) {
                    if (response.d == 'true') {
                        alert("داده با موفقیت حذف شد");
                        jQuery('#delete_user_dialog').dialog('close');
                        return true;
                    }
                    else {
                        alert("خطا در حذف داده ها");
                        jQuery('#delete_user_dialog').dialog('close');
                        return false;
                    }

                },
                error: function ()
                { alert("خطا در حذف داده ها"); jQuery('#delete_user_dialog').dialog('close'); return false; }
            });

        },
        'انصراف': function () {
            var UpdatePanel1 = '<%=UpdatePanel1.ClientID%>';
            if (UpdatePanel1 != null)
                __doPostBack(UpdatePanel1, '');
            //$("#form1").data("bootstrapValidator").resetForm(true);
            jQuery('#delete_user_dialog').dialog('close');
        }
    }
});

        });
    </script>
    <%-- Start Call Dialog Update,Save,Delete   -------------%>
    <%-- SaveDialog Call user    --%>
    <script type="text/javascript">
        $(document).ready(function () {
            //Sabt Dialog Call 
            //
            $(document).on("click", "[id*=lbtn_sabt_user]", function () {
                $('#txt_fname_user').val('');
                $('#txt_lname_user').val('');
                $('#txt_shmeli_user').val('');
                $('#txt_username_user').val('');
                $('#txt_password_user').val('');
                $('#ddl_role_id_user').val(-1);

                $('#txt_fname_user').focus();
                $('#hfid_user').val('-1');  //ClientIDMode="Static"
                $('#sabt_user_dialog').dialog('open'); return false;
            });
        });
    </script>
    <%-- Update Dialog Call     --%>
    <script type="text/javascript">
        // cal ajax methode for save data
        $(document).ready(function () {

            //Update Dialog Call 
            //

            $(document).on("click", "[id*=lbtn_edit_user]", function (e) {
                var mydata = ($(this).attr('data-id'));
                $.ajax({
                    type: 'POST',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    url: 'sabt_user.aspx/get_user_byid',
                    data: "{" +
                        "'ID':'" + mydata + "'" +
                     "}",
                    async: false,
                    success: function (response) {
                        $.map($.parseJSON(response.d), function (item) {
                            $("[id*=txt_fname_user]").val(item.Fname);
                            $("[id*=txt_lname_user]").val(item.Lname);
                            $("[id*=txt_shmeli_user]").val(item.Shmeli);
                            $("[id*=txt_username_user]").val(item.UserName);
                            $("[id*=txt_password_user]").val(item.Password);
                            $("[id*=ddl_role_id_user] option").filter(function () {
                                return $(this).text() == item.RoleName;
                            }).prop('selected', true);
                            $("[id*=hfid_user]").val(mydata);
                            $('#sabt_user_dialog').dialog('open');
                            return true;
                        });
                    },
                    error: function () {
                        alert("خطای نا شناخته در باز کردن پنجره ویرایش");
                        return false;
                    }
                });
            });
        });
    </script>
    <%-- Delete Dialog Call     --%>
    <script type="text/javascript">
        // cal ajax methode for save data
        $(document).ready(function () {
            //Delete Dialog Call 
            //
            $(document).on("click", "[id*=lbtn_delete_user]", function (e) {
                var mydata = ($(this).attr('data-id'));
                $("[id*=hfid_user]").val(mydata);
                $('#delete_user_dialog').dialog('open'); return false;
            });
        });
    </script>
    <asp:HiddenField ID="hfid_user" runat="server" ClientIDMode="Static" />
    <%-- Start Message Box Sabt,Update          -------------%>
    <div id="sabt_user_dialog" style="display: none">
        <div class="form-group  form-group-sm">
            <div class="row">
                <div class="col-sm-6">
                    <asp:Label ID="Label9" runat="server" Text="نام کاربر"></asp:Label>
                    <asp:TextBox ID="txt_fname_user" runat="server" Width="220px" CssClass="form-control" ClientIDMode="Static" placeholder="نام کاربر را وارد کنید" lang="fa"></asp:TextBox>
                </div>
                <div class="col-sm-6">
                    <asp:Label ID="Label10" runat="server" Text="نام خانوادگی کاربر"></asp:Label>
                    <asp:TextBox ID="txt_lname_user" runat="server" Width="220px" CssClass="form-control" ClientIDMode="Static" placeholder="نام خانوادگی کاربر را وارد کنید" lang="fa"></asp:TextBox>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <asp:Label ID="Label3" runat="server" Text="شماره ملی"></asp:Label>
                    <asp:TextBox ID="txt_shmeli_user" runat="server" Width="220px" CssClass="form-control" ClientIDMode="Static" placeholder="شماره ملی را وارد کنید" lang="fa"></asp:TextBox>
                </div>
                <div class="col-sm-6">
                    <asp:Label ID="Label4" runat="server" Text="نقش کاربری"></asp:Label>
                    <asp:DropDownList ID="ddl_role_id_user" runat="server" Width="220px" CssClass="form-control" ClientIDMode="Static" lang="fa"></asp:DropDownList>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <asp:Label ID="Label7" runat="server" Text="نام کاربری"></asp:Label>
                    <asp:TextBox ID="txt_username_user" runat="server" Width="220px" CssClass="form-control" ClientIDMode="Static" placeholder="نام کاربری را وارد کنید" lang="fa"></asp:TextBox>
                </div>
                <div class="col-sm-6">
                    <asp:Label ID="Label12" runat="server" Text="کلمه عبور"></asp:Label>
                    <asp:TextBox ID="txt_password_user" runat="server" Width="220px" CssClass="form-control" ClientIDMode="Static" placeholder="کلمه عبور را وارد کنید" lang="fa"></asp:TextBox>
                </div>
            </div>

        </div>
    </div>
    <%-- End Message Box Sabt,Update            -------------%>
    <%-- Start Message Box Delete               -------------%>
    <div id="delete_user_dialog" style="display: none">
        <p>
            <br />
            آیا برای حذف اطمینان دارید؟
        </p>
    </div>
    <%-- End Message Box Delete                 -------------%>
    <%-- Start Grid view                        -------------%>
    <div class="container full-width">
        <!--------------------------------------------------Message Box--------------------------------------------->
        <div class="row">
            <asp:HiddenField ID="messagetexthf" runat="server" />
            <div class="col-sm-12" id="message" style="display: none">
                <div id="messagetext" class="alert-danger text-center">پیام خطا</div>
            </div>
        </div>
        <!--------------------------------------------------------------------------------------------------------->
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-info">
                    <div class="panel-body">
                        <div class="row">
                            <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <div class="form-inline">
                                        <div class="form-group form-group-sm">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <asp:Label ID="Label11" runat="server" Text="نام خانوادگی کاربر"></asp:Label>
                                                    <asp:TextBox ID="txt_search" runat="server" CssClass="form-control" Width="300px"></asp:TextBox>
                                                    <asp:LinkButton ID="lbtn_search" runat="server" CssClass="btn btn-primary btn-sm glyphicon glyphicon-search" data-toggle="tooltip" data-placement="top" title="جستجو" OnClick="lbtn_search_Click"></asp:LinkButton>
                                                    <asp:LinkButton ID="lbtn_sabt_user" runat="server" CssClass="btn  btn-success btn-sm glyphicon glyphicon-cog" data-toggle="tooltip" data-placement="top" title="ثبت کاربر"></asp:LinkButton>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <asp:GridView ID="gvdata" runat="server" CellPadding="3" GridLines="Horizontal" AutoGenerateColumns="False"
                                        Width="98%" UseAccessibleHeader="False" DataKeyNames="ID" BackColor="White" BorderColor="#E7E7FF"
                                        BorderStyle="None" BorderWidth="1px" AllowPaging="True"
                                        OnPageIndexChanging="gvdata_PageIndexChanging"
                                        AllowSorting="True"  PageSize="7">
                                        <AlternatingRowStyle BackColor="#F7F7F7" />
                                        <Columns>
                                            <asp:TemplateField HeaderText="کد کاربر" SortExpression="ID">
                                                <ItemTemplate><%# (Eval("ID").ToString())%></ItemTemplate>
                                                <ItemStyle Width="5%" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="نام کاربر" SortExpression="Fname">
                                                <ItemTemplate><%# (Eval("Fname").ToString())%></ItemTemplate>
                                                <ItemStyle Width="10%" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="نام خانوادگی" SortExpression="Lname">
                                                <ItemTemplate><%# (Eval("Lname").ToString())%></ItemTemplate>
                                                <ItemStyle Width="10%" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="شماره ملی" SortExpression="Shmeli">
                                                <ItemTemplate><%#Eval("Shmeli").ToString()%></ItemTemplate>
                                                <ItemStyle Width="10%" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="نقش کاربری" SortExpression="Role.RoleName">
                                                <ItemTemplate><%#Eval("Role.RoleName").ToString()%></ItemTemplate>
                                                <ItemStyle Width="15%" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="نام کاربری" SortExpression="UserName">
                                                <ItemTemplate><%#Eval("UserName").ToString()%></ItemTemplate>
                                                <ItemStyle Width="10%" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="کلمه عبور" SortExpression="Password">
                                                <ItemTemplate><%#Eval("Password").ToString()%></ItemTemplate>
                                                <ItemStyle Width="10%" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="عملیات">
                                                <ItemTemplate>
                                                    <asp:LinkButton runat="server" ID="lbtn_edit_user" CommandArgument='<%#Eval("ID")%>' CommandName="do_edit_user" CssClass="btn btn-success btn-xs glyphicon glyphicon-edit" data-id='<%#Eval("ID")%>' data-toggle="tooltip" data-placement="top" title="ویرایش کاربر" Width="30px"></asp:LinkButton>
                                                    <asp:LinkButton runat="server" ID="lbtn_delete_user" CommandArgument='<%#Eval("ID")%>' CommandName="do_delete_user" CssClass="btn btn-danger btn-xs glyphicon glyphicon-trash" data-id='<%#Eval("ID")%>' data-toggle="tooltip" data-placement="top" title="حذف کاربر" Width="30px"></asp:LinkButton>
                                                </ItemTemplate>
                                                <ItemStyle Width="13%" />
                                            </asp:TemplateField>
                                        </Columns>
                                        <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
                                        <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" HorizontalAlign="Center" VerticalAlign="Middle" Font-Size="Small" />
                                        <PagerSettings Position="TopAndBottom" />
                                        <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Center" VerticalAlign="Middle" Font-Bold="true" CssClass="mypager" />
                                        <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Center" VerticalAlign="Middle" Font-Size="small" Font-Bold="true" Height="30px" />
                                        <SelectedRowStyle BackColor="#738A9C" ForeColor="#F7F7F7" Font-Bold="True" />
                                        <SortedAscendingCellStyle BackColor="#F4F4FD" />
                                        <SortedAscendingHeaderStyle BackColor="#5A4C9D" />
                                        <SortedDescendingCellStyle BackColor="#D8D8F0" />
                                        <SortedDescendingHeaderStyle BackColor="#3E3277" />
                                    </asp:GridView>
                                </ContentTemplate>
                                <Triggers>
                                    <%--                                        <asp:PostBackTrigger ControlID="lbt_print_user" />--%>
                                </Triggers>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
            <%--</div>--%>
        </div>
    </div>
    <%-- End Grid view                          -------------%>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0px; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <%--<span style="border-width: 0px; position: fixed; padding: 50px; background-color: #FFFFFF; font-size: 36px; left: 40%; top: 40%;">Loading ...</span>--%>
                <%--<img alt="" src="../images/loader.gif" />--%>
                <div class="loader" style="top: 300px">Loading...</div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
