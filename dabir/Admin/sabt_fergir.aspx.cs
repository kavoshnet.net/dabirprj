﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Globalization;
using EntityFramework.Extensions;
using MyObjects;
using DataLayer;
using DataLayer.DataAccessLayer;
using DomainClass;
using DomainClass.DomainModel;
using NReco.PdfGenerator;
using System.Web.Script.Serialization;
using System.IO;

namespace dabir.Admin
{
    /// <summary>
    /// کنترل صفحات در این قسمت انجام میگیرد
    /// </summary>
    public partial class sabt_fergir : System.Web.UI.Page
    {
        #region userfunction
        /// <summary>
        /// </summary>
        /// <param name="NameOffice"></param>
        protected void SetGVdata(string NameOffice = "")
        {
            using (var _edbcontext = new DataBaseContext())
            {
                if (NameOffice.Equals(string.Empty))
                {
                    gvdata.DataSource =
                            (from t in _edbcontext.Offices
                             orderby t.ID ascending
                             select t).ToList();
                    gvdata.DataBind();
                }
                else
                {
                    gvdata.DataSource =
                            (from t in _edbcontext.Offices
                             where t.NameOffice.Contains(NameOffice)
                             orderby t.ID ascending
                             select t).ToList();
                    gvdata.DataBind();

                }
            }
        }
        protected void initialstate()
        {
            gvdata.PageSize = int.Parse(System.Configuration.ConfigurationManager.AppSettings["PageSize"]);
            SetGVdata(txt_search.Text);
        }
        [WebMethod]
        public static string sabt_fergir_method(string ID, string NameOffice)
        {
            using (var _edbcontext = new DataBaseContext())
            {
                if (int.Parse(ID) == -1)
                {
                    try
                    {
                        var _fergir = new Office
                        {
                            NameOffice = NameOffice
                        };
                        _edbcontext.Offices.Add(_fergir);
                        _edbcontext.SaveChanges();
                        return _fergir.ID.ToString();
                    }
                    catch (Exception ex)
                    {
                        Log.LogErrorMessage(ex.Message);
                        return "-1";
                    }
                }
                else
                {
                    try
                    {
                        var myid_fergir = long.Parse(ID);
                        var query = from l in _edbcontext.Offices
                                    where (l.ID == myid_fergir)
                                    select l;
                        var _fergir = query.FirstOrDefault();
                        if (_fergir != null)
                        {
                            _fergir.NameOffice = NameOffice;
                        }
                        _edbcontext.SaveChanges();
                        return _fergir.ID.ToString();
                    }
                    catch (Exception ex)
                    {
                        Log.LogErrorMessage(ex.Message);
                        return "-1";
                    }
                }
            }

        }
        [WebMethod]
        public static string get_fergir_byid(string ID)
        {
            var myid_fergir = long.Parse(ID);
            using (var _edbcontext = new DataBaseContext())
            {
                var query = from l in _edbcontext.Offices
                            where (l.ID == myid_fergir)
                            select new
                            {
                                l.ID,
                                l.NameOffice,
                            };
                var _fergir = query.ToList();
                if (_fergir != null)
                {
                    var s = new JavaScriptSerializer
                    {
                        MaxJsonLength = int.MaxValue
                    };
                    return s.Serialize(_fergir);
                }
                return null;
            }
        }
        [WebMethod]
        public static string delete_fergir_method(string ID)
        {
            using (var _edbcontext = new DataBaseContext())
            {
                if (int.Parse(ID) != -1)
                {
                    var myid_fergir = long.Parse(ID);
                    var query = from c in _edbcontext.Offices
                                where (c.ID == myid_fergir)
                                select c;
                    var _fergir = query.FirstOrDefault();
                    if (_fergir != null)
                    {
                        _edbcontext.Offices.Remove(_fergir);
                        _edbcontext.SaveChanges();
                        return "true";
                    }
                    return "false";

                }
                return "false";
            }
        }
        #endregion
        #region systemmethode
        protected void Page_Load(object sender, EventArgs e)
        {
            if ((Session["vorod"] != null))
            {
                if ((Session["vorod"].ToString() == "ok"))
                {
                    if ((int.Parse(Session["role_id"].ToString()) == (int)MyObjects.RolName.Admin) ||
                        (int.Parse(Session["role_id"].ToString()) == (int)MyObjects.RolName.SuperAdmin)
                        )
                    {
                        if (!IsPostBack)
                        {
                            //ScriptManager.GetCurrent(this.Page).RegisterPostBackControl();
                            initialstate();
                        }
                        else
                        {
                            SetGVdata(txt_search.Text);
                        }
                    }
                }
                else
                {
                    Response.Redirect("../login.aspx");
                }
            }
        }

        protected void gvdata_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            SetGVdata(txt_search.Text);
            gvdata.PageIndex = e.NewPageIndex;
            gvdata.DataBind();
            System.Threading.Thread.Sleep(500);
        }

        #endregion

        protected void lbtn_search_Click(object sender, EventArgs e)
        {
            SetGVdata(txt_search.Text);
        }
    }
}