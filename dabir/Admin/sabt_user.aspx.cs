﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Globalization;
using EntityFramework.Extensions;
using MyObjects;
using DataLayer.DataAccessLayer;
using DomainClass.DomainModel;
using NReco.PdfGenerator;
using System.Web.Script.Serialization;
using System.IO;

namespace dabir.Admin
{
    /// <summary>
    /// کنترل صفحات در این قسمت انجام میگیرد
    /// </summary>
    public partial class sabt_user : System.Web.UI.Page
    {
        #region userfunction
        /// <summary>
        /// تابعی برای پر کردن اطلاعات گرید ویو
        /// که بر اساس عنوان صفحه مرتب می گردد
        /// </summary>
        /// <param name="lname_user">todo: describe lname_user parameter on SetGVdata</param>
        protected void SetGVdata(string lname_user="")
        {
            using (DataBaseContext _edbcontext = new DataBaseContext())
            {

                if (lname_user.Equals(string.Empty))
                {
                    gvdata.DataSource =
                            (from t in _edbcontext.Users
                             orderby t.ID ascending
                             select t).ToList();
                    gvdata.DataBind();
                }
                else
                {
                    gvdata.DataSource =
                        (from t in _edbcontext.Users
                         where t.Lname.Contains(lname_user)
                         orderby t.ID ascending
                         select t).ToList();
                    gvdata.DataBind();
                }
            }
        }
        protected void setddl_role_id_user()
        {
            using (DataBaseContext _edbcontext = new DataBaseContext())
            {

                ddl_role_id_user.DataSource = (from e in _edbcontext.Roles
                                               orderby e.ID ascending
                                               select e).ToList();
                ddl_role_id_user.DataTextField = "RoleName";
                ddl_role_id_user.DataValueField = "ID";
                ddl_role_id_user.DataBind();
                ddl_role_id_user.Items.Insert(0, new ListItem("انتخاب شود", "-1"));
                ddl_role_id_user.SelectedIndex = -1;
            }
        }
        protected void initialstate()
        {
            gvdata.PageSize = int.Parse(System.Configuration.ConfigurationManager.AppSettings["PageSize"]);
            setddl_role_id_user();
            SetGVdata(txt_search.Text);
        }
        [WebMethod]
        public static string SetSession(string session_name, string session_value)
        {
            HttpContext.Current.Session[session_name] = session_value;
            return "true";
        }
        /// <summary>
        /// تابعی جهت وارد کردن داده ها در بانک اطلاعاتی به صورت ایجکس
        /// </summary>
        /// <param name="ID">todo: describe ID parameter on sabt_user_method</param>
        /// <param name="Fname">todo: describe Fname parameter on sabt_user_method</param>
        /// <param name="Lname">todo: describe Lname parameter on sabt_user_method</param>
        /// <param name="Shmeli">todo: describe Shmeli parameter on sabt_user_method</param>
        /// <param name="UserName">todo: describe UserName parameter on sabt_user_method</param>
        /// <param name="Password">todo: describe Password parameter on sabt_user_method</param>
        /// <param name="RoleID">todo: describe RoleID parameter on sabt_user_method</param>
        /// <returns></returns>
        [WebMethod]
        public static string sabt_user_method(string ID,
            string Fname, string Lname,
            string Shmeli, string UserName, string Password,string RoleID)
        {
            using (DataBaseContext _edbcontext = new DataBaseContext())
            {
                if (int.Parse(ID) == -1)
                {
                    try
                    {
                        var _user = new User
                        {
                            Fname = Fname,
                            Lname = Lname,
                            Shmeli = Shmeli,
                            UserName = UserName,
                            Password = Password,

                            RoleID = Convert.ToInt64(RoleID)
                        };

                        _edbcontext.Users.Add(_user);
                        _edbcontext.SaveChanges();
                        return _user.ID.ToString();
                    }
                    catch (Exception ex)
                    {
                        Log.LogErrorMessage(ex.Message);
                        return "-1";
                    }
                }
                else
                {
                    try
                    {
                        var myid_user = long.Parse(ID);
                        var query = from l in _edbcontext.Users
                                    where (l.ID == myid_user)
                                    select l;
                        var _user = query.FirstOrDefault();
                        if (_user != null)
                        {
                            _user.Fname = Fname;
                            _user.Lname = Lname;
                            _user.Shmeli = Shmeli;
                            _user.UserName = UserName;
                            _user.Password = Password;
                            _user.RoleID = Convert.ToInt64(RoleID);
                        }
                        _edbcontext.SaveChanges();
                        return _user.ID.ToString();
                    }
                    catch (Exception ex)
                    {
                        Log.LogErrorMessage(ex.Message);
                        return "-1";
                    }
                }
            }
        }
        [WebMethod]
        public static string get_user_byid(string ID)
        {
            var myid_user = long.Parse(ID);
            using (DataBaseContext _edbcontext = new DataBaseContext())
            {
                var query = from l in _edbcontext.Users
                            where (l.ID == myid_user)
                            select new
                            {
                                l.ID,
                                l.Lname,
                                l.Fname,
                                l.Shmeli,
                                l.UserName,
                                l.Password,
                                l.RoleID,
                                l.Role.RoleName
                            };
                var _user = query.ToList();
                if (_user != null)
                {
                    var s = new JavaScriptSerializer
                    {
                        MaxJsonLength = int.MaxValue
                    };
                    return s.Serialize(_user);
                }
                return null;
            }
        }
        [WebMethod]
        public static string delete_user_method(string ID)
        {
            using (var _edbcontext = new DataBaseContext())
            {
                if (int.Parse(ID) != -1)
                {
                    var myid_user = long.Parse(ID);
                    var query = from c in _edbcontext.Users
                                where (c.ID == myid_user)
                                select c;
                    var _user = query.FirstOrDefault();
                    if (_user != null)
                    {
                        _edbcontext.Users.Remove(_user);
                        _edbcontext.SaveChanges();
                        return "true";
                    }
                    return "false";
                }
                return "false";
            }
        }
        #endregion
        #region systemmethode
        protected void Page_Load(object sender, EventArgs e)
        {
            if ((Session["vorod"] != null))
            {
                if ((Session["vorod"].ToString() == "ok"))
                {
                    if ((int.Parse(Session["role_id"].ToString()) == (int)MyObjects.RolName.Admin) ||
                        (int.Parse(Session["role_id"].ToString()) == (int)MyObjects.RolName.SuperAdmin)
                        )
                    {
                        if (!IsPostBack)
                        {
                            //ScriptManager.GetCurrent(this.Page).RegisterPostBackControl();
                            Log.LogErrorMessage("ok");
                            initialstate();
                        }
                        else
                        {
                            Log.LogErrorMessage("ok");
                            SetGVdata(txt_search.Text);
                        }
                    }
                }
                else
                {
                    Log.LogErrorMessage("exit");
                    Response.Redirect("../login.aspx");
                }
            }
        }

        protected void gvdata_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            SetGVdata(txt_search.Text);
            gvdata.PageIndex = e.NewPageIndex;
            gvdata.DataBind();
            System.Threading.Thread.Sleep(500);
        }
        #endregion

        protected void lbtn_search_Click(object sender, EventArgs e)
        {
            SetGVdata(txt_search.Text);
        }
    }
}