﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPageAdmin.Master" AutoEventWireup="true" CodeBehind="sabt_page.aspx.cs" Inherits="dabir.Admin.sabt_page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!---------------------------------------Display Errore Message-------------------------------------------->
    <%-- Start Dialog Function                 --------------%>
    <%-- Dialog Sabt     --%>
    <script type="text/javascript">
        // cal ajax methode for save data
        $(document).ready(function () {
            $("#sabt_page_dialog").dialog(
          {
              autoOpen: false,
              resizable: false,
              modal: false,
              bgiframe: true,
              width: 800,
              height: 600,
              title: "ثبت الگوی صفحه",
              open: function (event, ui) { $(".ui-dialog-titlebar-close").hide(); },
              show: {
                  effect: "fade",
                  duration: 50
              },
              hide: {
                  effect: "fade",
                  duration: 50
              },
              close: function (event, ui) {
                  var UpdatePanel1 = '<%=UpdatePanel1.ClientID%>';
                  if (UpdatePanel1 != null)
                      __doPostBack(UpdatePanel1, '');
              },
              buttons: {
                  'ثبت و ادامه': function () {
                      var theFileName = $('#txt_bgpath').val().replace(/\\/g, "/");
                      var fileNameIndex = theFileName.lastIndexOf("/") + 1;
                      var fileName = theFileName.substr(fileNameIndex);
                      $.ajax({
                          type: 'POST',
                          contentType: "application/json; charset=utf-8",
                          url: 'sabt_page.aspx/sabt_page_method',
                          data: "{" +
                              "'ID':'" + $('#hfid_page').val() + "'," +
                              "'Name':'" + $('#txt_page_name').val() + "'," +
                              "'Width':'" + $('#txt_page_width').val() + "'," +
                              "'Height':'" + $('#txt_page_height').val() + "'," +
                              "'Orientation':'" + $('#ddl_orientation').val() + "'," +
                              "'Margin_Top':'" + $('#txt_margin_top').val() + "'," +
                              "'Margin_Bottom':'" + $('#txt_margin_bottom').val() + "'," +
                              "'Margin_Right':'" + $('#txt_margin_right').val() + "'," +
                              "'Margin_Left':'" + $('#txt_margin_left').val() + "'," +
                              "'Header':'" + CKEDITOR.instances['txt_header'].getData() + "'," +
                              "'Body':'" + CKEDITOR.instances['txt_body'].getData() + "'," +
                              "'Message':'" + CKEDITOR.instances['txt_message'].getData() + "'," +
                              "'Footer':'" + CKEDITOR.instances['txt_footer'].getData() + "'," +
                              "'BGPath':'" + fileName + "'" +
                          "}",
                          async: false,
                          success: function (response) {
                              if (response.d != '-1') {
                                  alert(" الگوی صفحه با کد << " + response.d + " >> ثبت شد");
                                  var UpdatePanel1 = '<%=UpdatePanel1.ClientID%>';
                                  if (UpdatePanel1 != null)
                                      __doPostBack(UpdatePanel1, '');
                                  jQuery('#sabt_page_dialog').dialog('close');
                                  return true;
                              }
                              else {
                                  alert("خطا در ثبت داده ها");
                                  return false;
                              }

                          },
                          error: function ()
                          {
                              alert("خطا در ثبت داده ها"); jQuery('#sabt_page_dialog').dialog('close'); return false;
                          }
                      });

                  },
                  'انصراف': function () {
                      var UpdatePanel1 = '<%=UpdatePanel1.ClientID%>';
                      if (UpdatePanel1 != null)
                          __doPostBack(UpdatePanel1, '');
                      jQuery('#sabt_page_dialog').dialog('close');
                  }
              }
          });
        });
    </script>
    <%-- Dialog Delete      --%>
    <script type="text/javascript">
        // cal ajax methode for delete data
        $(document).ready(function () {
            $('#delete_page_dialog').dialog(
{
    autoOpen: false,
    resizable: true,
    modal: true,
    bgiframe: true,
    width: 300,
    height: 175,
    title: "حذف داده ها",
    open: function (event, ui) { $(".ui-dialog-titlebar-close").hide(); },
    show: {
        effect: "fade",
        duration: 50
    },
    hide: {
        effect: "explode",
        duration: 50
    },
    close: function (event, ui) {
        var UpdatePanel1 = '<%=UpdatePanel1.ClientID%>';
        if (UpdatePanel1 != null)
            __doPostBack(UpdatePanel1, '');
    },
    buttons: {
        'حذف': function () {
            $.ajax({
                type: 'POST',
                contentType: "application/json; charset=utf-8",
                url: 'sabt_page.aspx/delete_page_method',
                data: "{'ID':'" + $('#hfid_page').val() + "'" +
                           "}",
                async: false,
                success: function (response) {
                    if (response.d == 'true') {
                        alert("داده با موفقیت حذف شد");
                        jQuery('#delete_page_dialog').dialog('close');
                        return true;
                    }
                    else {
                        alert("خطا در حذف داده ها");
                        jQuery('#delete_page_dialog').dialog('close');
                        return false;
                    }

                },
                error: function ()
                { alert("خطا در حذف داده ها"); jQuery('#delete_page_dialog').dialog('close'); return false; }
            });

        },
        'انصراف': function () {
            var UpdatePanel1 = '<%=UpdatePanel1.ClientID%>';
            if (UpdatePanel1 != null)
                __doPostBack(UpdatePanel1, '');
            jQuery('#delete_page_dialog').dialog('close');
        }
    }
});

        });
    </script>
    <%-- Start Call Dialog Update,Save,Delete   -------------%>
    <%-- SaveDialog Call     --%>
    <script type="text/javascript">
        $(document).ready(function () {
            //Sabt Dialog Call 
            //
            $(document).on("click", "[id*=lbtn_sabt_page]", function () {
                $('#txt_page_name').val('');
                $('#txt_page_width').val('');
                $('#txt_page_height').val('');
                $('ddl_orientation').val(0);
                $('#txt_margin_top').val('');
                $('#txt_margin_bottom').val('');
                $('#txt_margin_right').val('');
                $('#txt_margin_left').val('');
                CKEDITOR.instances['txt_header'].setData('');
                CKEDITOR.instances['txt_message'].setData('');
                CKEDITOR.instances['txt_body'].setData('');
                CKEDITOR.instances['txt_footer'].setData('');
                $('#txt_bgpath').val('');

                $('#txt_page_name').focus();
                $('#hfid_page').val('-1');  //ClientIDMode="Static"
                $('#sabt_page_dialog').dialog('open'); return false;
            });
        });
    </script>
    <%-- Update Dialog Call     --%>
    <script type="text/javascript">
        // cal ajax methode for save data
        $(document).ready(function () {

            //Update Dialog Call 
            //

            $(document).on("click", "[id*=lbtn_edit_page]", function (e) {
                var mydata = ($(this).attr('data-id'));
                $.ajax({
                    type: 'POST',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    url: 'sabt_page.aspx/get_page_byid',
                    data: "{" +
                        "'ID':'" + mydata + "'" +
                     "}",
                    async: false,
                    success: function (response) {
                        $.map($.parseJSON(response.d), function (item) {
                            $("[id*=txt_page_name]").val(item.Name);
                            $("[id*=txt_page_width]").val(item.Width);
                            $("[id*=txt_page_height]").val(item.Height);
                            $("[id*=ddl_orientation] option").filter(function () {
                                return $(this).val() == item.Orientation;
                            }).prop('selected', true);
                            $("[id*=txt_margin_top]").val(item.Margin_Top);
                            $("[id*=txt_margin_bottom]").val(item.Margin_Bottom);
                            $("[id*=txt_margin_right]").val(item.Margin_Right);
                            $("[id*=txt_margin_left]").val(item.Margin_Left);
                            CKEDITOR.instances['txt_header'].setData(item.Header);
                            CKEDITOR.instances['txt_message'].setData(item.Message);
                            CKEDITOR.instances['txt_body'].setData(item.Body);
                            CKEDITOR.instances['txt_footer'].setData(item.Footer);
                            //$("[id*=txt_bgpath]").val(item.BGPath);
                            //$("[id*=txt_header]").val(item.Header);
                            //$("[id*=txt_message]").val(item.Message);
                            //$("[id*=txt_footer]").val(item.Footer);

                            $("[id*=hfid_page]").val(mydata);
                            $('#sabt_page_dialog').dialog('open');
                            return true;
                        });
                    },
                    error: function () {
                        alert("خطای نا شناخته در باز کردن پنجره ویرایش");
                        return false;
                    }
                });
            });
        });
    </script>
    <%-- Delete Dialog Call     --%>
    <script type="text/javascript">
        // cal ajax methode for save data
        $(document).ready(function () {
            //Delete Dialog Call 
            //
            $(document).on("click", "[id*=lbtn_delete_page]", function (e) {
                var mydata = ($(this).attr('data-id'));
                $("[id*=hfid_page]").val(mydata);
                $('#delete_page_dialog').dialog('open'); return false;
            });
        });
    </script>
    <asp:HiddenField ID="hfid_page" runat="server" ClientIDMode="Static" />
    <%-- Start Message Box Sabt,Update          -------------%>
    <div id="sabt_page_dialog" style="display: none">
        <div class="form-group  form-group-sm">
            <div class="row">
                <div class="col-sm-12">
                    <asp:Label ID="Label10" runat="server" Text="نام صفحه"></asp:Label>
                    <asp:TextBox ID="txt_page_name" runat="server" Width="300px" CssClass="form-control" ClientIDMode="Static" placeholder="نام صفحه را وارد کنید" lang="fa"></asp:TextBox>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-2">
                    <asp:Label ID="Label1" runat="server" Text="طول صفحه"></asp:Label>
                    <asp:TextBox ID="txt_page_width" runat="server" Width="100px" CssClass="form-control" ClientIDMode="Static" placeholder="طول صفحه" lang="fa"></asp:TextBox>
                </div>
                <div class="col-sm-2">
                    <asp:Label ID="Label2" runat="server" Text="عرض صفحه"></asp:Label>
                    <asp:TextBox ID="txt_page_height" runat="server" Width="100px" CssClass="form-control" ClientIDMode="Static" placeholder="عرض صفحه" lang="fa"></asp:TextBox>
                </div>
                <div class="col-sm-2">
                    <asp:Label ID="Label5" runat="server" Text="فاصله از بالا"></asp:Label>
                    <asp:TextBox ID="txt_margin_top" runat="server" Width="100px" CssClass="form-control" ClientIDMode="Static" placeholder="فاصله از بالا" lang="fa"></asp:TextBox>
                </div>
                <div class="col-sm-2">
                    <asp:Label ID="Label6" runat="server" Text="فاصله از پایین"></asp:Label>
                    <asp:TextBox ID="txt_margin_bottom" runat="server" Width="100px" CssClass="form-control" ClientIDMode="Static" placeholder="فاصله از پایین" lang="fa"></asp:TextBox>
                </div>
                <div class="col-sm-2">
                    <asp:Label ID="Label8" runat="server" Text="فاصله از راست"></asp:Label>
                    <asp:TextBox ID="txt_margin_right" runat="server" Width="100px" CssClass="form-control" ClientIDMode="Static" placeholder="فاصله از راست" lang="fa"></asp:TextBox>
                </div>
                <div class="col-sm-2">
                    <asp:Label ID="Label9" runat="server" Text="فاصله از چپ"></asp:Label>
                    <asp:TextBox ID="txt_margin_left" runat="server" Width="100px" CssClass="form-control" ClientIDMode="Static" placeholder="فاصله از چپ" lang="fa"></asp:TextBox>
                </div>
                <div class="col-sm-2">
                    <asp:Label ID="Label13" runat="server" Text="صفحه زمینه"></asp:Label>
                    <asp:FileUpload ID="txt_bgpath" runat="server" Width="100px" CssClass="form-control" ClientIDMode="Static" placeholder="صفحه زمینه" lang="fa"></asp:FileUpload>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <asp:Label ID="Label12" runat="server" Text="جهت صفحه"></asp:Label>
                    <asp:DropDownList ID="ddl_orientation" runat="server" Width="200px" CssClass="form-control" ClientIDMode="Static" lang="fa"></asp:DropDownList>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <asp:Label ID="Label3" runat="server" Text="سر صفحه"></asp:Label>
                    <CKEditor:CKEditorControl ID="txt_header" runat="server" Height="150" ClientIDMode="Static" Skin="moono-lisa" lang="fa"></CKEditor:CKEditorControl>
                </div>
                <div class="col-sm-12">
                    <asp:Label ID="Label4" runat="server" Text="عبارت آغازین"></asp:Label>
                    <CKEditor:CKEditorControl ID="txt_message" runat="server" Height="150" ClientIDMode="Static" Skin="moono-lisa" lang="fa"></CKEditor:CKEditorControl>
                </div>
                <div class="col-sm-12">
                    <asp:Label ID="Label11" runat="server" Text="متن الگو"></asp:Label>
                    <CKEditor:CKEditorControl ID="txt_body" runat="server" Height="150" ClientIDMode="Static" Skin="moono-lisa" lang="fa"></CKEditor:CKEditorControl>
                </div>
                <div class="col-sm-12">
                    <asp:Label ID="Label7" runat="server" Text="پایین صفحه"></asp:Label>
                    <CKEditor:CKEditorControl ID="txt_footer" runat="server" Height="150" ClientIDMode="Static" Skin="moono-lisa" lang="fa"></CKEditor:CKEditorControl>
                </div>
            </div>
        </div>
    </div>
    <%-- End Message Box Sabt,Update            -------------%>
    <%-- Start Message Box Delete               -------------%>
    <div id="delete_page_dialog" style="display: none">
        <p>
            <br />
            آیا برای حذف اطمینان دارید؟
        </p>
    </div>
    <%-- End Message Box Delete                 -------------%>
    <%-- Start Grid view                        -------------%>
    <div class="container full-width">
        <!--------------------------------------------------Message Box--------------------------------------------->
        <div class="row">
            <asp:HiddenField ID="messagetexthf" runat="server" />
            <div class="col-sm-12" id="message" style="display: none">
                <div id="messagetext" class="alert-danger text-center">پیام خطا</div>
            </div>
        </div>
        <!--------------------------------------------------------------------------------------------------------->
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-info">
                    <div class="panel-body">
                        <div class="row">
                            <%--                            <div class="col-sm-12">--%>
                            <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <div class="form-inline">
                                        <div class="form-group form-group-sm">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <asp:LinkButton ID="lbtn_sabt_page" runat="server" CssClass="btn  btn-success btn-sm glyphicon glyphicon-open-file" data-toggle="tooltip" data-placement="top" title="ثبت الگوی صفحه"></asp:LinkButton>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <asp:GridView ID="gvdata" runat="server" CellPadding="3" GridLines="Horizontal" AutoGenerateColumns="False"
                                        Width="98%" UseAccessibleHeader="False" DataKeyNames="ID" BackColor="White" BorderColor="#E7E7FF"
                                        BorderStyle="None" BorderWidth="1px" AllowPaging="True"
                                        OnPageIndexChanging="gvdata_PageIndexChanging"
                                        AllowSorting="True" PageSize="7">
                                        <AlternatingRowStyle BackColor="#F7F7F7" />
                                        <Columns>
                                            <asp:TemplateField HeaderText="نام صفحه" SortExpression="ID">
                                                <ItemTemplate><%# (Eval("Name").ToString())%></ItemTemplate>
                                                <ItemStyle Width="8%" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="طول صفحه" SortExpression="ID">
                                                <ItemTemplate><%# (Eval("Width").ToString())%></ItemTemplate>
                                                <ItemStyle Width="8%" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="صفحه عرض" SortExpression="ID">
                                                <ItemTemplate><%# (Eval("Height").ToString())%></ItemTemplate>
                                                <ItemStyle Width="8%" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="جهت صفحه" SortExpression="ID">
                                                <ItemTemplate><%# MyObjects.myclass.stand_orientation(Eval("Orientation").ToString())%></ItemTemplate>
                                                <ItemStyle Width="8%" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="فاصله از بالا" SortExpression="ID">
                                                <ItemTemplate><%# (Eval("Margin_Top").ToString())%></ItemTemplate>
                                                <ItemStyle Width="8%" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="فاصله از پایین" SortExpression="ID">
                                                <ItemTemplate><%# (Eval("Margin_Bottom").ToString())%></ItemTemplate>
                                                <ItemStyle Width="8%" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="فاصله از راست" SortExpression="ID">
                                                <ItemTemplate><%# (Eval("Margin_Right").ToString())%></ItemTemplate>
                                                <ItemStyle Width="8%" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="فاصله از چپ" SortExpression="ID">
                                                <ItemTemplate><%# (Eval("Margin_Left").ToString())%></ItemTemplate>
                                                <ItemStyle Width="8%" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="عملیات">
                                                <ItemTemplate>
                                                    <asp:LinkButton runat="server" ID="lbtn_edit_page" CommandArgument='<%#Eval("ID")%>' CommandName="do_edit_page" CssClass="btn btn-success btn-xs glyphicon glyphicon-edit" data-id='<%#Eval("ID")%>' data-toggle="tooltip" data-placement="top" title="ویرایش الگو" Width="30px"></asp:LinkButton>
                                                    <asp:LinkButton runat="server" ID="lbtn_delete_page" CommandArgument='<%#Eval("ID")%>' CommandName="do_delete_page" CssClass="btn btn-danger btn-xs glyphicon glyphicon-trash" data-id='<%#Eval("ID")%>' data-toggle="tooltip" data-placement="top" title="حذف الگو" Width="30px"></asp:LinkButton>
                                                </ItemTemplate>
                                                <ItemStyle Width="13%" />
                                            </asp:TemplateField>
                                        </Columns>
                                        <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
                                        <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" HorizontalAlign="Center" VerticalAlign="Middle" Font-Size="Small" />
                                        <PagerSettings Position="TopAndBottom" />
                                        <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Center" VerticalAlign="Middle" Font-Bold="true" CssClass="mypager" />
                                        <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Center" VerticalAlign="Middle" Font-Size="small" Font-Bold="true" Height="30px" />
                                        <SelectedRowStyle BackColor="#738A9C" ForeColor="#F7F7F7" Font-Bold="True" />
                                        <SortedAscendingCellStyle BackColor="#F4F4FD" />
                                        <SortedAscendingHeaderStyle BackColor="#5A4C9D" />
                                        <SortedDescendingCellStyle BackColor="#D8D8F0" />
                                        <SortedDescendingHeaderStyle BackColor="#3E3277" />
                                    </asp:GridView>
                                </ContentTemplate>
                                <Triggers>
                                    <%--                                        <asp:PostBackTrigger ControlID="lbt_print_page" />--%>
                                </Triggers>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
            <%--</div>--%>
        </div>
    </div>
    <%-- End Grid view                          -------------%>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0px; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <%--<span style="border-width: 0px; position: fixed; padding: 50px; background-color: #FFFFFF; font-size: 36px; left: 40%; top: 40%;">Loading ...</span>--%>
                <%--<img alt="" src="../images/loader.gif" />--%>
                <div class="loader" style="top: 300px">Loading...</div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
