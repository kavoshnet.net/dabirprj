﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Globalization;
using EntityFramework.Extensions;
using MyObjects;
using DataLayer.DataAccessLayer;
using DomainClass.DomainModel;
using NReco.PdfGenerator;
using System.Web.Script.Serialization;
using System.IO;

namespace dabir.Admin
{
    /// <summary>
    /// کنترل صفحات در این قسمت انجام میگیرد
    /// </summary>
    public partial class sabt_letter : System.Web.UI.Page
    {
        /// <summary>
        /// متغیری برای دسترسی به کل مدل تولید شده
        /// </summary>
        #region userfunction
        /// <summary>
        /// تابعی برای پر کردن اطلاعات گرید ویو
        /// که بر اساس عنوان صفحه مرتب می گردد
        /// </summary>
        protected void CKEditorConfig()
        {
            //CKEditor1.config.uiColor = "#BFEE62";
            //txt_con_letter.config.language = "fa";
            //            txt_con_letter_ed.config.font_names = "Tahoma";
            txt_con_letter_ed.config.font_defaultLabel = "Tahoma";
            txt_con_letter_ed.config.fontSize_defaultLabel = "16";
            txt_con_letter_ed.config.language = "fa";
            txt_con_letter_ed.config.bodyClass = "{font-family:Tahoma}";
            txt_con_letter_ed.ResizeEnabled = false;
            txt_con_letter_ed.config.toolbar = new object[]
            {
                new object[]{ "Source", "NewPage", "Preview", "-", "Templates"},
                new object[]{ "Cut","Copy","Paste","PasteText","PasteWord","-","Undo","Redo"},
                new object[]{ "SelectAll","RemoveFormat"},
                new object[]{ "Bold","Italic","Underline"},
                new object[]{ "NumberedList", "BulletedList", "-", "Outdent", "Indent","-","Blockquote"},
                new object[]{ "JustifyLeft","JustifyCenter","JustifyRight", "JustifyBlock", "JustifyFull","-","BidiLtr","BidiRtl"},
                new object[]{ "Styles","Format","Font","FontSize"},
                new object[]{ "TextColor","BGColor"},
                new object[]{ "Table"}
            };
            //CKEditor1.config.removePlugins = "link";
            //CKEditor1.config.enterMode = EnterMode.BR;
            //CKEditor1.config.toolbar = new object[]
            //{
            // new object[] { "Source", "-", "Save", "NewPage", "Preview", "-", "Templates" },
            // new object[] { "Link", "Unlink", "Anchor" },
            //};

            //txt_con_letter_ed.config.toolbar = new object[]
            //{
            //    new object[]{ "Source", "DocProps", "-", "Save", "NewPage", "Preview", "-", "Templates"},
            //    new object[]{ "Cut","Copy","Paste","PasteText","PasteWord","-","Print","SpellCheck"},
            //    new object[]{"Undo","Redo","-","Find","Replace","-","SelectAll","RemoveFormat"},
            //    new object[]{"Form","Checkbox","Radio","TextField","Textarea","Select","Button","ImageButton","HiddenField"},
            //    "/",
            //    new object[]{"Bold","Italic","Underline","StrikeThrough","-","Subscript","Superscript"},
            //    new object[]{"OrderedList","UnorderedList","-","Outdent","Indent","Blockquote"},
            //    new object[]{"JustifyLeft","JustifyCenter","JustifyRight","JustifyFull"},
            //    new object[]{"Link","Unlink","Anchor"},
            //    new object[]{"Image","Flash","Table","Rule","Smiley","SpecialChar","PageBreak"},
            //    "/",
            //    new object[]{"Style","FontFormat","FontName","FontSize"},
            //    new object[]{"TextColor","BGColor"},
            //    new object[]{"FitWindow","ShowBlocks","-","About"}, // No comma for the last row.
            //};

            //CKEditor1.config.toolbar = new object[]
            //{
            //   new object[] { "Cut", "Copy", "Paste", "PasteText", "PasteWord" },
            //   new object[] { "Undo","Redo","-","Bold","Italic","Underline","StrikeThrough" },
            //   "/",
            //   new object[] {"OrderedList","UnorderedList","-","Outdent","Indent" },
            //   new object[] {"Link","Unlink","Anchor" },
            //   "/",
            //   new object[] {"Style" },
            //   new object[] {"Table","Image","Flash","Rule","SpecialChar" },
            //   new object[] {"About" }
            //};
        }
        protected void SetGVdata(string FromDate, string ToDate)
        {
            using (DataBaseContext _edbcontext = new DataBaseContext())
            {
                var id_user = long.Parse(Session["id_user"].ToString());
                IQueryable<Reference> temp = null;
                if (IsRole(RolName.SuperAdmin, id_user))
                {
                    temp = (from t in _edbcontext.References.Include(nameof(Letter))
                            select t);

                }
                else
                {
                    temp = (from t in _edbcontext.References.Include(nameof(Letter))
                            where
                            (t.UserIDSender == id_user && t.UserIDReceiver == id_user) ||
                            (t.UserIDReceiver == id_user)
                            select t);

                }
                if (ViewState["search"] != null && ViewState["search"].ToString() != string.Empty)
                {
                    var VStateSearch = ViewState["search"].ToString();
                    var laststring = VStateSearch.Substring(0, VStateSearch.Length - 1).Split(' ').Last();
                    if (laststring != "AND" && laststring != "OR")
                    {
                        string sqlquery = null;
                        if (IsRole(RolName.SuperAdmin, id_user))
                        {
                            sqlquery = "SELECT  * FROM V_tbl_letter WHERE " + ViewState["search"];

                        }
                        else
                        {
                            sqlquery = "SELECT  * FROM V_tbl_letter WHERE " + ViewState["search"] +
                            " AND( " + "(" + "UserIDSender = " + id_user + " AND UserIDReceiver = " + id_user + ")" + " OR UserIDReceiver = " + id_user + ")";

                        }
                        temp = _edbcontext.References.SqlQuery(sqlquery).AsQueryable<Reference>();
                    }
                }
                else
                {
                    if (IsRole(RolName.SuperAdmin, id_user))
                    {
                        temp = FromDate != string.Empty && ToDate != string.Empty
                        ? (from t in _edbcontext.References.Include(nameof(Letter))
                           where
                           (string.Compare(t.DateReference, FromDate, StringComparison.Ordinal) >= 0 && string.Compare(t.DateReference, ToDate, StringComparison.Ordinal) <= 0)
                           select t)
                        : (from t in _edbcontext.References.Include(nameof(Letter))
                           select t);

                    }
                    else
                    {
                        temp = FromDate != string.Empty && ToDate != string.Empty
                        ? (from t in _edbcontext.References.Include(nameof(Letter))
                           where
                           (string.Compare(t.DateReference, FromDate, StringComparison.Ordinal) >= 0 && string.Compare(t.DateReference, ToDate, StringComparison.Ordinal) <= 0)
                           &&
                           (
                           (t.UserIDSender == id_user && t.UserIDReceiver == id_user) ||
                           (t.UserIDReceiver == id_user)
                           )
                           select t)
                        : (from t in _edbcontext.References.Include(nameof(Letter))
                           where
                           (t.UserIDSender == id_user && t.UserIDReceiver == id_user) ||
                           (t.UserIDReceiver == id_user)
                           select t);

                    }

                }
                if (ViewState["sortexpression"] != null)
                {
                    switch (ViewState["sortexpression"].ToString().ToLower())
                    {
                        case "letterid asc":
                            temp = temp.OrderBy(u => u.LetterID);
                            break;
                        case "letterid desc":
                            temp = temp.OrderByDescending(u => u.LetterID);
                            break;
                        case "letter.attribute asc":
                            temp = temp.OrderBy(u => u.Letter.Attribute);
                            break;
                        case "letter.attribute desc":
                            temp = temp.OrderByDescending(u => u.Letter.Attribute);
                            break;
                        case "letter.subject asc":
                            temp = temp.OrderBy(u => u.Letter.Subject);
                            break;
                        case "letter.subject desc":
                            temp = temp.OrderByDescending(u => u.Letter.Subject);
                            break;
                        case "letter.senderoffice.nameoffice asc":
                            temp = temp.OrderBy(u => u.Letter.SenderOffice.NameOffice);
                            break;
                        case "letter.senderoffice.nameoffice desc":
                            temp = temp.OrderByDescending(u => u.Letter.SenderOffice.NameOffice);
                            break;
                        case "letter.receiveroffice.nameoffice asc":
                            temp = temp.OrderBy(u => u.Letter.ReceiverOffice.NameOffice);
                            break;
                        case "letter.receiveroffice.nameoffice desc":
                            temp = temp.OrderByDescending(u => u.Letter.ReceiverOffice.NameOffice);
                            break;
                        case "datereference asc":
                            temp = temp.OrderBy(u => u.DateReference);
                            break;
                        case "datereference desc":
                            temp = temp.OrderByDescending(u => u.DateReference);
                            break;
                    }
                }
                gvdata.DataSource = temp.ToList();
                gvdata.DataBind();
            }
        }
        protected void setddl_fergir_letter_out()
        {
            using (DataBaseContext _edbcontext = new DataBaseContext())
            {
                ddl_gir_id_letter_out.DataSource = (from e in _edbcontext.Offices
                                                    where e.ID != 0
                                                    orderby e.ID ascending
                                                    select e).ToList();
                ddl_gir_id_letter_out.DataTextField = "NameOffice";
                ddl_gir_id_letter_out.DataValueField = "ID";
                ddl_gir_id_letter_out.DataBind();
                ddl_gir_id_letter_out.Items.Insert(0, new ListItem("انتخاب شود", "-1"));
                ddl_gir_id_letter_out.SelectedIndex = -1;
            }
        }
        protected void setddl_fergir_letter_in()
        {
            using (DataBaseContext _edbcontext = new DataBaseContext())
            {
                ddl_fer_id_letter_in.DataSource = (from e in _edbcontext.Offices
                                                   where e.ID != 0
                                                   orderby e.ID ascending
                                                   select e).ToList();
                ddl_fer_id_letter_in.DataTextField = "NameOffice";
                ddl_fer_id_letter_in.DataValueField = "ID";
                ddl_fer_id_letter_in.DataBind();
                ddl_fer_id_letter_in.Items.Insert(0, new ListItem("انتخاب شود", "-1"));
                ddl_fer_id_letter_in.SelectedIndex = -1;
            }
        }
        protected void setddl_page_id_letter_out()
        {
            using (DataBaseContext _edbcontext = new DataBaseContext())
            {
                ddl_page_id_letter_out.DataSource = (from e in _edbcontext.Pages
                                                     orderby e.ID ascending
                                                     select e).ToList();
                ddl_page_id_letter_out.DataTextField = "Name";
                ddl_page_id_letter_out.DataValueField = "ID";
                ddl_page_id_letter_out.DataBind();
                ddl_page_id_letter_out.Items.Insert(0, new ListItem("انتخاب شود", "-1"));
                ddl_page_id_letter_out.SelectedIndex = -1;
            }
        }
        protected void setddl_page_id_letter_in()
        {
            using (DataBaseContext _edbcontext = new DataBaseContext())
            {
                ddl_page_id_letter_in.DataSource = (from e in _edbcontext.Pages
                                                    orderby e.ID ascending
                                                    select e).ToList();
                ddl_page_id_letter_in.DataTextField = "Name";
                ddl_page_id_letter_in.DataValueField = "ID";
                ddl_page_id_letter_in.DataBind();
                ddl_page_id_letter_in.Items.Insert(0, new ListItem("انتخاب شود", "-1"));
                ddl_page_id_letter_in.SelectedIndex = -1;
            }
        }
        protected void setddl_ugir_erja_id()
        {
            using (DataBaseContext _edbcontext = new DataBaseContext())
            {
                var UserID = long.Parse(Session["id_user"].ToString());
                ddl_ugir_erja_id.DataSource = (from e in _edbcontext.Users
                                               where e.ID != UserID
                                               orderby e.ID ascending
                                               select new { e.ID, flnameuser = e.Fname + " " + e.Lname + "(" + e.Role.RoleNameFa + ")" }).ToList();
                ddl_ugir_erja_id.DataTextField = "flnameuser";
                ddl_ugir_erja_id.DataValueField = "ID";
                ddl_ugir_erja_id.DataBind();
                ddl_ugir_erja_id.Items.Insert(0, new ListItem("انتخاب شود", "-1"));
                ddl_ugir_erja_id.SelectedIndex = -1;
            }
        }
        /// <summary>
        /// متدی برای پر کردن لیست کشویی فیلدها
        /// </summary>
        protected void setddl_search_field()
        {
            //ddl_search_field.DataSource = (from e in _edbcontext.tbl_fergir
            //                                orderby e.id_fergir ascending
            //                                select e).ToList();
            //ddl_search_field.DataTextField = "nam_fergir";
            //ddl_search_field.DataValueField = "id_fergir";
            //ddl_search_field.DataBind();
            ddl_search_field.Items.Insert(0, new ListItem("انتخاب شود", "-1"));
            ddl_search_field.Items.Insert(1, new ListItem("شماره اندیکاتور", "LetterID"));
            ddl_search_field.Items.Insert(2, new ListItem("شماره نامه", "[Letter.Number]"));
            ddl_search_field.Items.Insert(3, new ListItem("موضوع نامه", "[Letter.Subject]"));
            ddl_search_field.Items.Insert(4, new ListItem("فرستنده", "[SenderOffice.NameOffice]"));
            ddl_search_field.Items.Insert(5, new ListItem("گیرنده", "[ReceiverOffice.NameOffice]"));
            ddl_search_field.Items.Insert(6, new ListItem("تاریخ ثبت نامه", "DateReference"));
            ddl_search_field.SelectedIndex = 0;
        }
        /// <summary>
        /// متدی برای پر کردن لیست کشویی گزینه های مقایسه ای
        /// </summary>
        protected void setddl_search_option()
        {
            //ddl_search_field.DataSource = (from e in _edbcontext.tbl_fergir
            //                                orderby e.id_fergir ascending
            //                                select e).ToList();
            //ddl_search_field.DataTextField = "nam_fergir";
            //ddl_search_field.DataValueField = "id_fergir";
            //ddl_search_field.DataBind();
            ddl_search_option.Items.Insert(0, new ListItem("انتخاب شود", "-1"));
            ddl_search_option.Items.Insert(1, new ListItem("شامل", "LIKE"));
            ddl_search_option.Items.Insert(2, new ListItem("برابر", "="));
            ddl_search_option.Items.Insert(3, new ListItem("نابرابر", "<>"));
            ddl_search_option.Items.Insert(4, new ListItem("کوچکتراز", "<"));
            ddl_search_option.Items.Insert(5, new ListItem("کوچکترمساوی", "<="));
            ddl_search_option.Items.Insert(6, new ListItem("بزرگتراز", ">"));
            ddl_search_option.Items.Insert(7, new ListItem("بزرگترمساوی", ">="));
            //ddl_search_option.Items.Insert(0, new ListItem("شروع با", "7"));
            //ddl_search_option.Items.Insert(0, new ListItem("شروع نشود با", "8"));
            //ddl_search_option.Items.Insert(0, new ListItem("اتمام با", "9"));
            //ddl_search_option.Items.Insert(0, new ListItem("تمام نشود با", "10"));
            //ddl_search_option.Items.Insert(0, new ListItem("نباشد حاوی", "11"));
            //ddl_search_option.Items.Insert(0, new ListItem("خالی باشد", "12"));
            //ddl_search_option.Items.Insert(0, new ListItem("خالی نباشد", "13"));
            //ddl_search_option.Items.Insert(0, new ListItem("عضو این باشد", "14"));
            //ddl_search_option.Items.Insert(0, new ListItem("عضو این نباشد", "15"));
            ddl_search_option.SelectedIndex = 0;
        }
        /// <summary>
        /// متدی برای پر کردن لیست کشویی ترکیب جستجو ها
        /// </summary>
        protected void setddl_search_combine()
        {
            //ddl_search_field.DataSource = (from e in _edbcontext.tbl_fergir
            //                                orderby e.id_fergir ascending
            //                                select e).ToList();
            //ddl_search_field.DataTextField = "nam_fergir";
            //ddl_search_field.DataValueField = "id_fergir";
            //ddl_search_field.DataBind();
            ddl_search_combine.Items.Insert(0, new ListItem("انتخاب شود", "-1"));
            ddl_search_combine.Items.Insert(1, new ListItem("و", "AND"));
            ddl_search_combine.Items.Insert(2, new ListItem("یا", "OR"));
            ddl_search_combine.SelectedIndex = 0;
        }

        /// <summary>
        /// تابعی جهت انجام اعمال مورد نیاز در هنگام لود اولیه صفحه
        /// </summary>
        public void inittextweek()
        {
            if (Session["addday"] == null)
            {
                Session["addday"] = 0;
            }
            var DayRange = int.Parse(System.Configuration.ConfigurationManager.AppSettings["DayRange"]);
            var nextweek = PersianDateTime.Now.AddDays((int)Session["addday"]).ToString(PersianDateTimeFormat.Date);
            var lastweek = PersianDateTime.Now.AddDays(((int)Session["addday"]) - DayRange).ToString(PersianDateTimeFormat.Date);
            txt_nextweek.Attributes["onclick"] = "PersianDatePicker.Show(this,'" + nextweek + "');";
            txt_lastweek.Attributes["onclick"] = "PersianDatePicker.Show(this,'" + lastweek + "');";
            txt_lastweek.Attributes["onclick"] = "PersianDatePicker.Show(this,'" + lastweek + "');";
            txt_dte_letter_in.Attributes["onclick"] = "PersianDatePicker.Show(this,'" + nextweek + "');";
            txt_dte_sabt_letter_in.Attributes["onclick"] = "PersianDatePicker.Show(this,'" + nextweek + "');";
            txt_dte_sabt_letter_out.Attributes["onclick"] = "PersianDatePicker.Show(this,'" + nextweek + "');";

            settextweek();
        }
        public void settextweek()
        {
            if (Session["addday"] == null)
            {
                Session["addday"] = 0;
            }
            var DayRange = int.Parse(System.Configuration.ConfigurationManager.AppSettings["DayRange"]);
            //txt_lastweek.Text = MyObjects.myclass.shortshamsidate(DateTime.Now.AddDays((int)Session["addday"]));
            //txt_nextweek.Text = MyObjects.myclass.shortshamsidate(DateTime.Now.AddDays(((int)Session["addday"]) + 7));
            var nextweek = PersianDateTime.Now.AddDays((int)Session["addday"]).ToString(PersianDateTimeFormat.Date);
            var lastweek = PersianDateTime.Now.AddDays(((int)Session["addday"]) - DayRange).ToString(PersianDateTimeFormat.Date);
            txt_nextweek.Text = nextweek;
            txt_lastweek.Text = lastweek;

        }
        protected void initialstate()
        {
            gvdata.PageSize = int.Parse(System.Configuration.ConfigurationManager.AppSettings["PageSize"]);
            setddl_fergir_letter_out();
            setddl_fergir_letter_in();
            setddl_page_id_letter_in();
            setddl_page_id_letter_out();
            setddl_ugir_erja_id();
            setddl_search_field();
            setddl_search_option();
            setddl_search_combine();
            Session["addday"] = 0;
            ViewState["search"] = "";
            CKEditorConfig();
            inittextweek();
            SetGVdata(txt_lastweek.Text, txt_nextweek.Text);
            lbtn_output_letter.Visible = Convert.ToBoolean(Session["Sabt_Sadere"].ToString());
            lbtn_input_letter.Visible = Convert.ToBoolean(Session["Sabt_Varede"].ToString());
        }
        [WebMethod]
        public static string SetSession(string session_name, string session_value)
        {
            HttpContext.Current.Session[session_name] = session_value;
            return "true";
        }

        [WebMethod]
        public static string sabt_out_letter_method(string ID, string Subject, string ReceiverOfficeID,
             string user_id_letter, string DateRegister, string PageID)
        {
            using (DataBaseContext _edbcontext = new DataBaseContext())
            {
                using (var dbContextTransaction = _edbcontext.Database.BeginTransaction())
                {
                    try
                    {
                        if (Int64.Parse(ID) == -1)
                        {
                            var _letter = new Letter
                            {
                                Number = "صادره",
                                DateLetter = "9999/99/99",
                                Subject = Subject,
                                ReceiverOfficeID = long.Parse(ReceiverOfficeID),
                                SenderOfficeID = 0,
                                DateRegister = DateRegister,// MyObjects.myclass.shortshamsidate(DateTime.Now);
                                PageID = int.Parse(PageID),
                                Content = string.Empty,
                                Attribute = (int)MyObjects.Attribute.Sadere,
                                KindLetter = (int)MyObjects.LetterKind.Adi
                            };

                            //set page content
                            var myid_page = Int64.Parse(PageID);
                            var querypage = from l in _edbcontext.Pages
                                            where (l.ID == myid_page)
                                            select l;
                            var _page = querypage.FirstOrDefault();
                            if (_page != null)
                            {
                                _letter.Content = _page.Body;
                            }
                            //set page content

                            _edbcontext.Letters.Add(_letter);
                            _edbcontext.SaveChanges();
                            //set Reference
                            var _reference = new Reference
                            {
                                Subject = "ایجاد کننده",
                                DateReference = _letter.DateRegister,
                                DateResponse = null,
                                LetterID = _letter.ID,
                                UserIDSender = long.Parse(user_id_letter),
                                UserIDReceiver = long.Parse(user_id_letter)
                            };
                            _edbcontext.References.Add(_reference);
                            _edbcontext.SaveChanges();
                            dbContextTransaction.Commit();
                            return _letter.ID.ToString();

                        }
                        else
                        {
                            var myid_letter = long.Parse(ID);
                            var query = from l in _edbcontext.Letters
                                        where (l.ID == myid_letter)
                                        select l;
                            var _letter = query.FirstOrDefault();
                            if (_letter != null)
                            {
                                _letter.Subject = Subject;
                                _letter.ReceiverOfficeID = long.Parse(ReceiverOfficeID);
                                _letter.SenderOfficeID = 0;
                                _letter.DateRegister = DateRegister;
                                _letter.PageID = int.Parse(PageID);
                            }
                            _edbcontext.SaveChanges();
                            dbContextTransaction.Commit();
                            return _letter.ID.ToString();
                        }

                        //return "false";

                    }
                    catch (Exception ex)
                    {
                        dbContextTransaction.Rollback();
                        Log.LogErrorMessage(ex);
                        return "false";
                    }
                }

            }
        }
        [WebMethod]
        public static string sabt_in_letter_method(string ID, string Number, string DateLetter,
            string Subject, string SenderOfficeID, string user_id_letter,
            string DateRegister, string PageID)
        {
            using (var _edbcontext = new DataBaseContext())
            {
                using (var dbContextTransaction = _edbcontext.Database.BeginTransaction())
                {
                    try
                    {
                        if (long.Parse(ID) == -1)
                        {
                            var _letter = new Letter
                            {
                                Number = Number,
                                DateLetter = DateLetter,
                                Subject = Subject,
                                SenderOfficeID = long.Parse(SenderOfficeID),
                                ReceiverOfficeID = 0,
                                DateRegister = DateRegister,
                                PageID = int.Parse(PageID),
                                Content = string.Empty,
                                Attribute = (int)MyObjects.Attribute.Varede,
                                KindLetter = (int)MyObjects.LetterKind.Adi
                            };


                            //set page content
                            var myid_page = long.Parse(PageID);
                            var querypage = from l in _edbcontext.Pages
                                            where (l.ID == myid_page)
                                            select l;
                            var _page = querypage.FirstOrDefault();
                            if (_page != null)
                            {
                                _letter.Content = _page.Body;
                            }
                            //save letter
                            _edbcontext.Letters.Add(_letter);
                            _edbcontext.SaveChanges();
                            //set Reference
                            var _reference = new Reference
                            {
                                Subject = "ایجاد کننده",
                                DateReference = _letter.DateRegister,
                                DateResponse = null,
                                LetterID = _letter.ID,
                                UserIDSender = long.Parse(user_id_letter),
                                UserIDReceiver = long.Parse(user_id_letter)
                            };
                            _edbcontext.References.Add(_reference);
                            _edbcontext.SaveChanges();
                            dbContextTransaction.Commit();
                            return _letter.ID.ToString();
                        }
                        else
                        {
                            var myid_letter = long.Parse(ID);
                            var query = from l in _edbcontext.Letters
                                        where (l.ID == myid_letter)
                                        select l;
                            var _letter = query.FirstOrDefault();
                            if (_letter != null)
                            {
                                _letter.Number = Number;
                                _letter.DateLetter = DateLetter;
                                _letter.Subject = Subject;
                                _letter.SenderOfficeID = long.Parse(SenderOfficeID);
                                _letter.ReceiverOfficeID = 0;
                                _letter.DateRegister = DateRegister;
                                _letter.PageID = int.Parse(PageID);
                            }
                            _edbcontext.SaveChanges();
                            dbContextTransaction.Commit();
                            return _letter.ID.ToString();
                        }
                    }
                    catch (Exception ex)
                    {
                        dbContextTransaction.Rollback();
                        Log.LogErrorMessage(ex);
                        return "false";
                    }
                }

            }
        }
        //TODO: ایجاد لینک و نمایش کارتابل
        //TODO: کارتابل نمایش نامه های ارجاع شده به کاربر
        //todo: ایجاد لینک همه نامه ها نمایش نامه های ارجاع شده و ایجاد شده توسط خود کاربر
        //todo: پس از ارجاع تا وقتی کاربر بعدی نامه را باز نکرده کاربر ارجاع دهنده بتواند نامه خودرا اصلاح کند مهم نیست
        //todo: خاتمه نامه دیگر در لیت نامه ها قابل مشاهده نباشد و ایجاد لینک نامه های خاتمه یافته
        //یادداشت:اصلاح برگشت از نمایش نامه به صفحه لاگین اصلی
        [WebMethod]
        public static string sabt_out_content_method(string ID, string Content)
        {

            using (DataBaseContext _edbcontext = new DataBaseContext())
            {
                var myid_letter = long.Parse(ID);
                var query = from l in _edbcontext.Letters
                            where (l.ID == myid_letter)
                            select l;
                var _letter = query.FirstOrDefault();
                if (_letter != null)
                {
                    _letter.Content = Content;
                    _letter.KindLetter = 0;

                    //_letter.gir_id_letter = 0;
                    //_letter.DateRegister = MyObjects.myclass.shortshamsidate(DateTime.Now);
                }
                _edbcontext.SaveChanges();
                return "true";
                //return "false";
            }
        }
        [WebMethod]
        public static string sabt_erja_letter_method(string ID, string Subject, string UserIDReceiver, string UserIDSender)
        {
            using (DataBaseContext _edbcontext = new DataBaseContext())
            {
                var myid_letter = long.Parse(ID);
                var UIDSender = long.Parse(UserIDSender);
                var query = from l in _edbcontext.Letters
                            where (l.ID == myid_letter)
                            select l;
                var _letter = query.FirstOrDefault();
                if (_letter != null)
                {
                    _letter.KindLetter = (int)LetterKind.Erja;
                    _edbcontext.SaveChanges();
                }

                //sabt dateresponse in reference table
                if (true)//تایید و ارجاع
                {
                    var referencequery = from r in _edbcontext.References
                                         where (r.LetterID == myid_letter
                                         && r.UserIDReceiver == UIDSender
                                         && r.DateResponse == null)
                                         select r;
                    if (referencequery.Count() >= 1)
                    {
                        var refid = referencequery.OrderByDescending(o => o.ID).FirstOrDefault();
                        refid.DateResponse = MyObjects.myclass.shortshamsidate(DateTime.Now);
                        _edbcontext.SaveChanges();
                    }
                }

                //sabt in tbl_erja
                var _erja = new Reference
                {
                    Subject = Subject,
                    UserIDReceiver = Convert.ToInt64(UserIDReceiver),
                    UserIDSender = Convert.ToInt64(UserIDSender),
                    LetterID = long.Parse(ID),
                    DateReference = MyObjects.myclass.shortshamsidate(DateTime.Now)
                };

                _edbcontext.References.Add(_erja);
                _edbcontext.SaveChanges();
                return ID;
            }
        }
        [WebMethod]
        public static string get_content_method(string ID)
        {

            using (DataBaseContext _edbcontext = new DataBaseContext())
            {
                var myid_letter = long.Parse(ID);
                var query = from l in _edbcontext.Letters
                            where (l.ID == myid_letter)
                            select l;
                var _letter = query.FirstOrDefault();
                if (_letter != null)
                {
                    return _letter.Content;
                }
                return string.Empty;
            }
        }
        [WebMethod]
        public static string get_letter_byid(string ID)
        {
            using (DataBaseContext _edbcontext = new DataBaseContext())
            {
                var myid_letter = long.Parse(ID);
                var query = from l in _edbcontext.Letters
                            where (l.ID == myid_letter)
                            select new
                            {
                                l.ID,
                                l.Number,
                                l.DateLetter,
                                l.Subject,
                                l.SenderOfficeID,
                                l.ReceiverOfficeID,
                                //l.user_id_letter,
                                l.DateRegister,
                                l.Content,
                                l.Attribute,
                                nam_fer = l.ReceiverOffice.NameOffice,
                                nam_gir = l.SenderOffice.NameOffice,
                                //fname_user = l.tbl_user.fname_user,
                                //name_user = l.tbl_user.lname_user,
                                page_name = l.Page.Name
                            };
                var _letter = query.ToList();
                if (_letter != null)
                {
                    var s = new JavaScriptSerializer
                    {
                        MaxJsonLength = int.MaxValue
                    };
                    return s.Serialize(_letter);
                }
                return null;
            }
        }
        [WebMethod]
        public static string get_references_letter_byid(string ID)
        {
            using (DataBaseContext _edbcontext = new DataBaseContext())
            {
                var myid_letter = long.Parse(ID);
                var query = from r in _edbcontext.References
                            where (r.LetterID == myid_letter)
                            orderby r.ID descending
                            select new
                            {
                                r.Subject,
                                r.DateReference,
                                r.DateResponse,
                                SenderName = r.UserSender.Fname + " " + r.UserSender.Lname,
                                ReceiverName = r.UserReceiver.Fname + " " + r.UserReceiver.Lname
                            };
                var _references = query.ToList();
                if (_references != null)
                {
                    var s = new JavaScriptSerializer
                    {
                        MaxJsonLength = int.MaxValue
                    };
                    return s.Serialize(_references);
                }
                return null;
            }
        }
        [WebMethod]
        public static string delete_letter_method(string ID)
        {
            using (DataBaseContext _edbcontext = new DataBaseContext())
            {
                if (long.Parse(ID) != -1)
                {
                    var myid_letter = long.Parse(ID);
                    var query = from c in _edbcontext.Letters.Include("References")
                                where (c.ID == myid_letter)
                                select c;
                    var _letter = query.FirstOrDefault();
                    if (_letter != null)
                    {
                        if (_letter.References.Count <= 1)
                        {
                            _edbcontext.Letters.Remove(_letter);
                            _edbcontext.SaveChanges();
                            return "true";
                        }
                        else
                        {
                            return "-1";
                        }
                    }
                    return "-2";

                }
                return "-2";
            }
        }
        protected static string Summary(string ID)
        {
            using (DataBaseContext _edbcontext = new DataBaseContext())
            {
                var myid_letter = long.Parse(ID);
                var query = (from r in _edbcontext.References
                             where (r.ID == myid_letter)
                             select new
                             {
                                 r.Subject,
                                 r.DateReference,
                                 r.DateResponse,
                                 SenderName = r.UserSender.Fname + " " + r.UserSender.Lname,
                                 ReceiverName = r.UserReceiver.Fname + " " + r.UserReceiver.Lname
                             }).FirstOrDefault();
                return
                      " از: " + query.SenderName +
                      " به: " + query.ReceiverName +
                      " تاریخ ارسال: " + query.DateReference +
                      " تاریخ پاسخ: " + query.DateResponse +
                      " موضوع: " + query.Subject;
            }
        }
        protected static Boolean EnabledEdit(string ID, string UserID)
        {
            using (DataBaseContext _edbcontext = new DataBaseContext())
            {
                var myid_references = long.Parse(ID);
                var _UserID = long.Parse(UserID);
                try
                {
                    var query = (from r in _edbcontext.References
                                 where (r.ID == myid_references)
                                 select new
                                 {
                                     r.Subject,
                                     r.DateReference,
                                     r.DateResponse,
                                     r.UserIDSender,
                                     r.UserIDReceiver
                                 }).FirstOrDefault();
                    var temp = query.DateResponse == null &&
                           (
                           query.UserIDSender == _UserID ||
                           query.UserIDReceiver == _UserID
                           );
                    return temp;

                }
                catch (Exception ex)
                {
                    Log.LogErrorMessage(ex);
                    return false;
                    throw;
                }
            }
        }
        protected static Boolean IsRole(RolName role, Int64 id_user)
        {
            using (DataBaseContext _edbcontext = new DataBaseContext())
            {
                var temp = (from t in _edbcontext.Users.Include(nameof(Role))
                            where
                            (t.ID == id_user)
                            select t.Role.RoleName).FirstOrDefault();
                if (role == (RolName)Enum.Parse(typeof(RolName), temp))
                    return true;
                else
                    return false;
            }
        }
        #endregion
        #region systemmethode
        protected void Page_Load(object sender, EventArgs e)
        {
            if ((Session["vorod"] != null))
            {
                if ((Session["vorod"].ToString() == "ok"))
                {
                    //if ((int.Parse(Session["role_id"].ToString()) == (int)MyObjects.RolName.Admin) ||
                    //    (int.Parse(Session["role_id"].ToString()) == (int)MyObjects.RolName.SuperAdmin)
                    //    )
                    {
                        if (!IsPostBack)
                        {
                            //ScriptManager.GetCurrent(this.Page).RegisterPostBackControl();
                            initialstate();
                        }
                        else
                        {
                            SetGVdata(txt_lastweek.Text, txt_nextweek.Text);
                        }
                    }
                }
                else
                {
                    Response.Redirect("../login.aspx");
                }
            }
        }

        protected void gvdata_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvdata.PageIndex = e.NewPageIndex;
            SetGVdata(txt_lastweek.Text, txt_nextweek.Text);
            gvdata.DataBind();
            System.Threading.Thread.Sleep(100);
        }

        protected void gvdata_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            using (DataBaseContext _edbcontext = new DataBaseContext())
            {
                if (e.CommandName == "do_print_letter")
                {
                    var index = Convert.ToInt64(e.CommandArgument.ToString());
                    var myid_letter = index;
                    var query = from l in _edbcontext.Letters
                                where (l.ID == myid_letter)
                                select l;
                    var _letter = query.FirstOrDefault();
                    if (_letter != null)
                    {
                        //unicode header
                        //string orginal_header = "<html><head><meta http-equiv='content-type'content='text/html;charset=UTF-8'></head><body dir='rtl' background='" + x + "'" +">";
                        //set olgo
                        var page = from p in _edbcontext.Pages
                                   where (p.ID == _letter.PageID)
                                   select p;
                        var _page = page.FirstOrDefault();
                        if (_page != null)
                        {
                            //var x = Server.MapPath("~/images/a4.jpg");
                            var orginal_header = string.Format("<html><head>" +
                                "<meta http-equiv='content-type'content='text/html;charset=UTF-8'>" +
                                "<style>" +
                                "body{{" +
                                "padding-top:{0}px;" +
                                "padding-right:{1}px;" +
                                "padding-left:{2}px;" +
                                "padding-buttom:{3}px;" +
                                "}}" +
                                "</style>" +
                                "</head>" +
                                "<body dir='rtl'>",
                                _page.Margin_Top, _page.Margin_Right, _page.Margin_Left, _page.Margin_Bottom);
                            const string orginal_footer = "</body></html>";
                            //merge field replace
                            var page_header = _page.Header
                                .Replace("MYDATE", _letter.DateRegister)
                                .Replace("MYNUMBER", _letter.ID.ToString())
                                .Replace("MYATT", "ندارد")
                                ;

                            //set olg
                            var html_content = orginal_header + page_header + _page.Message + _letter.Content + _page.Footer + orginal_footer;
                            var htmlToPdf = new NReco.PdfGenerator.HtmlToPdfConverter
                            {
                                //page size
                                PageWidth = _page.Width,//210//148
                                PageHeight = _page.Height//296//210

                            };
                            //margin
                            htmlToPdf.Margins.Top = _page.Margin_Top;
                            htmlToPdf.Margins.Bottom = _page.Margin_Bottom;
                            htmlToPdf.Margins.Right = _page.Margin_Right;
                            htmlToPdf.Margins.Left = _page.Margin_Left;

                            htmlToPdf.Margins.Top = 0;
                            htmlToPdf.Margins.Bottom = 0;
                            htmlToPdf.Margins.Right = 0;
                            htmlToPdf.Margins.Left = 0;
                            htmlToPdf.Orientation = (NReco.PdfGenerator.PageOrientation)_page.Orientation;
                            //header and footer
                            //htmlToPdf.PageHeaderHtml = header + _page.header;
                            //htmlToPdf.PageFooterHtml = _page.footer + footer;

                            //create pdf
                            var pdfBytes = htmlToPdf.GeneratePdf(html_content);

                            //open by browser or pdf reader
                            Response.Buffer = true;
                            Response.Cache.SetCacheability(HttpCacheability.NoCache);

                            this.Context.ApplicationInstance.CompleteRequest();
                            Response.Clear();
                            Response.ClearContent();
                            Response.ClearHeaders();
                            Response.ContentType = "application/pdf";
                            var filename = _letter.ID.ToString();
                            //Response.AddHeader("content-disposition", "attachment;filename=" + filename);
                            //Response.AddHeader("Content-Disposition", "attachment; filename="+filename+".pdf");
                            Response.AppendHeader("content-length", pdfBytes.Length.ToString());
                            Response.BinaryWrite(pdfBytes);
                            Response.Flush();
                            Response.Close();
                            Response.End();
                        }
                    }
                }
                if (e.CommandName == "do_preview_letter")
                {
                    var index = Convert.ToInt64(e.CommandArgument.ToString());
                    var myid_letter = index;
                    var query = from l in _edbcontext.Letters
                                where (l.ID == myid_letter)
                                select l;
                    var _letter = query.FirstOrDefault();
                    if (_letter != null)
                    {
                        //unicode header
                        //string orginal_header = "<html><head><meta http-equiv='content-type'content='text/html;charset=UTF-8'></head><body dir='rtl' background='" + x + "'" +">";
                        //set olgo
                        var page = from p in _edbcontext.Pages
                                   where (p.ID == _letter.PageID)
                                   select p;
                        var _page = page.FirstOrDefault();
                        if (_page != null)
                        {
                            //var x = Server.MapPath("~/images/a4.jpg");
                            string bg;
                            if (_page.BGPath != string.Empty)
                                bg = Server.MapPath("~/BGPath/" + _page.BGPath);
                            else
                                bg = "";
                            var orginal_header = string.Format("<html><head>" +
                                "<meta http-equiv='content-type'content='text/html;charset=UTF-8'>" +
                                "<style>" +
                                "body{{" +
                                "padding-top:{0}px;" +
                                "padding-right:{1}px;" +
                                "padding-left:{2}px;" +
                                "padding-buttom:{3}px;" +
                                "}}" +
                                "</style>" +
                                "</head>" +
                                "<body dir='rtl' background='{4}'>",
                                _page.Margin_Top, _page.Margin_Right, _page.Margin_Left, _page.Margin_Bottom, bg);
                            const string orginal_footer = "</body></html>";
                            //merge field replace
                            var page_header = _page.Header
                                .Replace("MYDATE", _letter.DateRegister)
                                .Replace("MYNUMBER", _letter.ID.ToString())
                                .Replace("MYATT", "ندارد")
                                ;

                            //set olg
                            var html_content = orginal_header + page_header + _page.Message + _letter.Content + _page.Footer + orginal_footer;
                            var htmlToPdf = new NReco.PdfGenerator.HtmlToPdfConverter
                            {
                                //page size
                                PageWidth = _page.Width,//210//148
                                PageHeight = _page.Height//296//210

                            };
                            //margin
                            htmlToPdf.Margins.Top = _page.Margin_Top;
                            htmlToPdf.Margins.Bottom = _page.Margin_Bottom;
                            htmlToPdf.Margins.Right = _page.Margin_Right;
                            htmlToPdf.Margins.Left = _page.Margin_Left;

                            htmlToPdf.Margins.Top = 0;
                            htmlToPdf.Margins.Bottom = 0;
                            htmlToPdf.Margins.Right = 0;
                            htmlToPdf.Margins.Left = 0;
                            htmlToPdf.Orientation = (NReco.PdfGenerator.PageOrientation)_page.Orientation;
                            //header and footer
                            //htmlToPdf.PageHeaderHtml = header + _page.header;
                            //htmlToPdf.PageFooterHtml = _page.footer + footer;

                            //create pdf
                            var pdfBytes = htmlToPdf.GeneratePdf(html_content);

                            //open by browser or pdf reader
                            Response.Buffer = true;
                            Response.Cache.SetCacheability(HttpCacheability.NoCache);

                            this.Context.ApplicationInstance.CompleteRequest();
                            Response.Clear();
                            Response.ClearContent();
                            Response.ClearHeaders();
                            Response.ContentType = "application/pdf";
                            var filename = _letter.ID.ToString();
                            //Response.AddHeader("content-disposition", "attachment;filename=" + filename);
                            //Response.AddHeader("Content-Disposition", "attachment; filename="+filename+".pdf");
                            Response.AppendHeader("content-length", pdfBytes.Length.ToString());
                            //Response.Write(string.Format("<script>window.open('{0}','_blank')</script>", pdfBytes));
                            Response.BinaryWrite(pdfBytes);
                            Response.Flush();
                            Response.Close();
                            Response.End();
                        }
                    }
                }
            }
        }

        protected void gvdata_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var _lbtn_edit_letter = (LinkButton)e.Row.FindControl("lbtn_edit_letter");
                _lbtn_edit_letter.Visible = Convert.ToBoolean(Session["Virayeshe_Moshakhasat"].ToString());

                var _lbtn_delete_letter = (LinkButton)e.Row.FindControl("lbtn_delete_letter");
                _lbtn_delete_letter.Visible = Convert.ToBoolean(Session["Hazf"].ToString());

                var _lbtn_content_letter_dialog = (LinkButton)e.Row.FindControl("lbtn_content_letter_dialog");
                _lbtn_content_letter_dialog.Visible = Convert.ToBoolean(Session["Virayeshe_Matn"].ToString());

                var _lbtn_erja_letter = (LinkButton)e.Row.FindControl("lbtn_erja_letter");
                _lbtn_erja_letter.Visible = Convert.ToBoolean(Session["Erja"].ToString());

                var _lbtn_print_letter = (LinkButton)e.Row.FindControl("lbtn_print_letter");
                _lbtn_print_letter.Visible = Convert.ToBoolean(Session["Chap"].ToString());

                var _lbtn_preview_letter = (LinkButton)e.Row.FindControl("lbtn_preview_letter");
                _lbtn_preview_letter.Visible = Convert.ToBoolean(Session["Preview"].ToString());

                var _IDReference = DataBinder.Eval(e.Row.DataItem, "ID").ToString();
                var UserID = Session["id_user"].ToString();
                if (Convert.ToBoolean(Session["Edit_Referenced_Letter"].ToString()))
                {
                    if (EnabledEdit(_IDReference, UserID))
                    {
                        _lbtn_edit_letter.Enabled = true;
                        _lbtn_content_letter_dialog.Enabled = true;
                    }
                    else
                    {
                        _lbtn_edit_letter.Enabled = false;
                        _lbtn_content_letter_dialog.Enabled = false;
                    }
                }
                if (IsRole(RolName.SuperAdmin, long.Parse(UserID)))
                {
                    _lbtn_edit_letter.Enabled = true;
                    _lbtn_content_letter_dialog.Enabled = true;
                }
                var linkbtnPRN = (LinkButton)e.Row.FindControl("lbtn_print_letter");
                var scriptManagerPRN = ScriptManager.GetCurrent(this.Page);
                if (scriptManagerPRN != null && linkbtnPRN != null)
                {
                    scriptManagerPRN.RegisterPostBackControl(linkbtnPRN);
                }
                var linkbtnPRV = (LinkButton)e.Row.FindControl("lbtn_preview_letter");
                var scriptManagerPRV = ScriptManager.GetCurrent(this.Page);
                if (scriptManagerPRV != null && linkbtnPRV != null)
                {
                    scriptManagerPRV.RegisterPostBackControl(linkbtnPRV);
                }
            }
            //if (Convert.ToInt16(DataBinder.Eval(e.Row.DataItem, "Letter.KindLetter")) == (int)LetterKind.Taed)
            //{
            //    e.Row.BackColor = System.Drawing.Color.PowderBlue;
            //}
            //if (Convert.ToInt16(DataBinder.Eval(e.Row.DataItem, "Letter.KindLetter")) == (int)LetterKind.Bargasht)
            //{
            //    e.Row.BackColor = System.Drawing.Color.Gray;
            //}

        }

        protected void lbtn_lastweek_Click(object sender, EventArgs e)
        {
            if (Session["addday"] == null)
            {
                Session["addday"] = 0;
            }
            var DayRange = int.Parse(System.Configuration.ConfigurationManager.AppSettings["DayRange"]);
            System.Threading.Thread.Sleep(500);
            Session["addday"] = ((int)Session["addday"]) - DayRange;
            settextweek();
            SetGVdata(txt_lastweek.Text, txt_nextweek.Text);
        }

        protected void lbtn_nextweek_Click(object sender, EventArgs e)
        {
            if (Session["addday"] == null)
            {
                Session["addday"] = 0;
            }
            var DayRange = int.Parse(System.Configuration.ConfigurationManager.AppSettings["DayRange"]);
            System.Threading.Thread.Sleep(500);
            Session["addday"] = ((int)Session["addday"]) + DayRange;
            settextweek();
            SetGVdata(txt_lastweek.Text, txt_nextweek.Text);
        }
        protected void lbtn_okweek_Click(object sender, EventArgs e)
        {
            if (!MyObjects.myclass.PersianDateValid(txt_lastweek.Text) || !MyObjects.myclass.PersianDateValid(txt_nextweek.Text))
                return;
            else
            {
                System.Threading.Thread.Sleep(500);
                SetGVdata(txt_lastweek.Text, txt_nextweek.Text);
            }
        }

        protected void gvdata_Sorting(object sender, GridViewSortEventArgs e)
        {
            if (ViewState["sortdirection"] == null)
            {
                ViewState["sortdirection"] = "ASC";
            }
            if (ViewState["sortdirection"].ToString() == "ASC")
                ViewState["sortdirection"] = "DESC";
            else
                ViewState["sortdirection"] = "ASC";
            ViewState["sortexpression"] = e.SortExpression + " " + ViewState["sortdirection"];
            //if(ViewState["search"]!=null && ViewState["search"].ToString()!=string.Empty)
            //    SetGVdata("","");
            //else
            SetGVdata(txt_lastweek.Text, txt_nextweek.Text);
        }
        protected void lbtn_add_search_Click(object sender, EventArgs e)
        {
            if (ViewState["search"].ToString() != string.Empty)
            {
                var laststring = ViewState["search"].ToString().Substring(0, ViewState["search"].ToString().Length - 1).Split(' ').Last();
                if (laststring != "AND" && laststring != "OR")
                {
                    ViewState["search"] = "";
                    lbl_search_text.Text = "";
                }
            }

            if (ddl_search_combine.SelectedValue != "-1")
            {
                if (ddl_search_field.SelectedValue != "-1" && ddl_search_option.SelectedValue != "-1" && txt_search.Text != "")
                {
                    if (ddl_search_option.SelectedValue.ToString() == "LIKE")
                    {
                        ViewState["search"] = ViewState["search"] + ddl_search_field.SelectedValue.ToString() + " " + ddl_search_option.SelectedValue + " " + "N'%" + txt_search.Text + "%'" + " " + ddl_search_combine.SelectedValue + " ";
                    }
                    else
                    {
                        ViewState["search"] = ViewState["search"] + ddl_search_field.SelectedValue.ToString() + " " + ddl_search_option.SelectedValue + " " + "N'" + txt_search.Text + "'" + " " + ddl_search_combine.SelectedValue + " ";
                    }
                    lbl_search_text.Text = lbl_search_text.Text + ddl_search_field.SelectedItem + " " + ddl_search_option.SelectedItem + " " + "'" + txt_search.Text + "'" + " " + ddl_search_combine.SelectedItem + " ";
                }
            }
            else
            {
                if (ddl_search_field.SelectedValue != "-1" && ddl_search_option.SelectedValue != "-1" && txt_search.Text != "")
                {
                    if (ddl_search_option.SelectedValue.ToString() == "LIKE")
                    {
                        ViewState["search"] = ViewState["search"] + ddl_search_field.SelectedValue.ToString() + " " + ddl_search_option.SelectedValue + " " + "N'%" + txt_search.Text + "%'" + " ";
                    }
                    else
                    {
                        ViewState["search"] = ViewState["search"] + ddl_search_field.SelectedValue.ToString() + " " + ddl_search_option.SelectedValue + " " + "N'" + txt_search.Text + "'" + " ";
                    }
                    lbl_search_text.Text = lbl_search_text.Text + ddl_search_field.SelectedItem + " " + ddl_search_option.SelectedItem + " " + "'" + txt_search.Text + "'" + " " + ddl_search_combine.SelectedItem + " ";
                }
            }
            ddl_search_field.SelectedIndex = -1;
            ddl_search_option.SelectedIndex = -1;
            ddl_search_combine.SelectedIndex = -1;
            txt_search.Text = "";

        }

        /// <summary>
        /// پاک کردن جستجوی قبلی
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lbtn_search_clear_Click(object sender, EventArgs e)
        {
            if (ViewState["search"].ToString() != "")
            {
                ddl_search_field.SelectedIndex = -1;
                ddl_search_option.SelectedIndex = -1;
                ddl_search_combine.SelectedIndex = -1;
                txt_search.Text = "";
                ViewState["search"] = "";
                SetGVdata(txt_lastweek.Text, txt_nextweek.Text);
                lbl_search_text.Text = "";
                System.Threading.Thread.Sleep(500);
            }
        }
        protected void lbtn_search_Click(object sender, EventArgs e)
        {
            if (ViewState["search"].ToString() != string.Empty)
            {
                SetGVdata("", "");
                System.Threading.Thread.Sleep(500);

            }
        }

        #endregion

    }
}