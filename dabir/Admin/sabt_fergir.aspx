﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPageAdmin.Master" AutoEventWireup="true" CodeBehind="sabt_fergir.aspx.cs" Inherits="dabir.Admin.sabt_fergir" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!---------------------------------------Display Errore Message-------------------------------------------->
    <%-- Start Dialog Function                 --------------%>
    <%-- Dialog Sabt FerGir    --%>
    <script type="text/javascript">
        // cal ajax methode for save data
        $(document).ready(function () {
            $("#sabt_fergir_dialog").dialog(
          {
              autoOpen: false,
              resizable: false,
              modal: true,
              bgiframe: true,
              width: 700,
              height: 200,
              title: "ثبت ادارات",
              open: function (event, ui) { $(".ui-dialog-titlebar-close").hide(); },
              show: {
                  effect: "fade",
                  duration: 50
              },
              hide: {
                  effect: "fade",
                  duration: 50
              },
              close: function (event, ui) {
                  var UpdatePanel1 = '<%=UpdatePanel1.ClientID%>';
                  if (UpdatePanel1 != null)
                      __doPostBack(UpdatePanel1, '');
              },
              buttons: {
                  'ثبت و ادامه': function () {
                      $.ajax({
                          type: 'POST',
                          contentType: "application/json; charset=utf-8",
                          url: 'sabt_fergir.aspx/sabt_fergir_method',
                          data: "{" +
                              "'ID':'" + $('#hfid_fergir').val() + "'," +
                               "'NameOffice':'" + $('#txt_nam_fergir').val() + "'" +
                           "}",
                          async: false,
                          success: function (response) {
                              if (response.d != '-1') {
                                  alert("اداره با کد << " + response.d + " >> ثبت شد");
                                  var UpdatePanel1 = '<%=UpdatePanel1.ClientID%>';
                                  if (UpdatePanel1 != null)
                                      __doPostBack(UpdatePanel1, '');
                                  jQuery('#sabt_fergir_dialog').dialog('close');
                                  return true;
                              }
                              else {
                                  alert("خطا در ثبت داده ها");
                                  return false;
                              }

                          },
                          error: function ()
                          { alert("خطا در ثبت داده ها"); jQuery('#sabt_fergir_dialog').dialog('close'); return false; }
                      });

                  },
                  'انصراف': function () {
                      var UpdatePanel1 = '<%=UpdatePanel1.ClientID%>';
                      if (UpdatePanel1 != null)
                          __doPostBack(UpdatePanel1, '');
                      jQuery('#sabt_fergir_dialog').dialog('close');
                  }
              }
          });
        });
    </script>
    <%-- Dialog Delete      --%>
    <script type="text/javascript">
        // cal ajax methode for delete data
        $(document).ready(function () {
            $('#delete_fergir_dialog').dialog(
{
    autoOpen: false,
    resizable: true,
    modal: true,
    bgiframe: true,
    width: 300,
    height: 175,
    title: "حذف داده ها",
    open: function (event, ui) { $(".ui-dialog-titlebar-close").hide(); },
    show: {
        effect: "fade",
        duration: 50
    },
    hide: {
        effect: "explode",
        //effect: "fade",
        duration: 50
    },
    close: function (event, ui) {
        var UpdatePanel1 = '<%=UpdatePanel1.ClientID%>';
        if (UpdatePanel1 != null)
            __doPostBack(UpdatePanel1, '');
        //$("#form1").data("bootstrapValidator").resetForm(true);
    },
    buttons: {
        'حذف': function () {
            $.ajax({
                type: 'POST',
                contentType: "application/json; charset=utf-8",
                url: 'sabt_fergir.aspx/delete_fergir_method',
                data: "{'ID':'" + $('#hfid_fergir').val() + "'" +
                           "}",
                async: false,
                success: function (response) {
                    if (response.d == 'true') {
                        alert("داده با موفقیت حذف شد");
                        jQuery('#delete_fergir_dialog').dialog('close');
                        return true;
                    }
                    else {
                        alert("خطا در حذف داده ها");
                        jQuery('#delete_fergir_dialog').dialog('close');
                        return false;
                    }

                },
                error: function ()
                { alert("خطا در حذف داده ها"); jQuery('#delete_fergir_dialog').dialog('close'); return false; }
            });

        },
        'انصراف': function () {
            var UpdatePanel1 = '<%=UpdatePanel1.ClientID%>';
            if (UpdatePanel1 != null)
                __doPostBack(UpdatePanel1, '');
            //$("#form1").data("bootstrapValidator").resetForm(true);
            jQuery('#delete_fergir_dialog').dialog('close');
        }
    }
});

        });
    </script>
    <%-- Start Call Dialog Update,Save,Delete   -------------%>
    <%-- SaveDialog Call Out    --%>
    <script type="text/javascript">
        $(document).ready(function () {
            //Sabt Dialog Call 
            //
            $(document).on("click", "[id*=lbtn_sabt_fergir]", function () {
                $('#txt_nam_fergir').val('');
                $('#hfid_fergir').val('-1');  //ClientIDMode="Static"
                $('#sabt_fergir_dialog').dialog('open'); return false;
            });
        });
    </script>
    <%-- Update Dialog Call     --%>
    <script type="text/javascript">
        // cal ajax methode for save data
        $(document).ready(function () {

            //Update Dialog Call 
            //

            $(document).on("click", "[id*=lbtn_edit_fergir]", function (e) {
                var mydata = ($(this).attr('data-id'));
                $.ajax({
                    type: 'POST',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    url: 'sabt_fergir.aspx/get_fergir_byid',
                    data: "{" +
                        "'ID':'" + mydata + "'" +
                     "}",
                    async: false,
                    success: function (response) {
                        $.map($.parseJSON(response.d), function (item) {
                            //$("[id*=txt_id_fergir]").val(item.id_fergir);
                            $("[id*=txt_nam_fergir]").val(item.NameOffice);
                            $("[id*=hfid_fergir]").val(mydata);
                            $('#sabt_fergir_dialog').dialog('open');
                            return true;
                        });
                    },
                    error: function () {
                        alert("خطای نا شناخته در باز کردن پنجره ویرایش");
                        return false;
                    }
                });
            });
        });
    </script>
    <%-- Delete Dialog Call     --%>
    <script type="text/javascript">
        // cal ajax methode for save data
        $(document).ready(function () {
            //Delete Dialog Call 
            //
            $(document).on("click", "[id*=lbtn_delete_fergir]", function (e) {
                var mydata = ($(this).attr('data-id'));
                $("[id*=hfid_fergir]").val(mydata);
                $('#delete_fergir_dialog').dialog('open'); return false;
            });
        });
    </script>
    <asp:HiddenField ID="hfid_fergir" runat="server" ClientIDMode="Static" />
    <%-- Start Message Box Sabt,Update          -------------%>
    <div id="sabt_fergir_dialog" style="display: none">
        <div class="form-group  form-group-sm">
            <div class="row">
                <div class="col-sm-12">
                    <asp:Label ID="Label3" runat="server" Text="نام اداره یا سازمان"></asp:Label>
                    <asp:TextBox ID="txt_nam_fergir" runat="server" Width="555px" CssClass="form-control" ClientIDMode="Static" placeholder="نام اداره یا سازمان را وارد کنید" lang="fa"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <%-- End Message Box Sabt,Update            -------------%>
    <%-- Start Message Box Delete               -------------%>
    <div id="delete_fergir_dialog" style="display: none">
        <p>
            <br />
            آیا برای حذف اطمینان دارید؟
        </p>
    </div>
    <%-- End Message Box Delete                 -------------%>
    <%-- Start Grid view                        -------------%>
    <div class="container full-width">
        <!--------------------------------------------------Message Box--------------------------------------------->
        <div class="row">
            <asp:HiddenField ID="messagetexthf" runat="server" />
            <div class="col-sm-12" id="message" style="display: none">
                <div id="messagetext" class="alert-danger text-center">پیام خطا</div>
            </div>
        </div>
        <!--------------------------------------------------------------------------------------------------------->
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-info">
                    <div class="panel-body">
                        <div class="row">
                            <%--                            <div class="col-sm-12">--%>
                            <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <div class="form-inline">
                                        <div class="form-group form-group-sm">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <asp:Label ID="Label11" runat="server" Text="نام اداره یا سازمان"></asp:Label>
                                                    <asp:TextBox ID="txt_search" runat="server" CssClass="form-control" Width="300px"></asp:TextBox>
                                                    <asp:LinkButton ID="lbtn_search" runat="server" CssClass="btn btn-primary btn-sm glyphicon glyphicon-search" data-toggle="tooltip" data-placement="top" title="جستجو" OnClick="lbtn_search_Click"></asp:LinkButton>
                                                    <asp:LinkButton ID="lbtn_sabt_fergir" runat="server" CssClass="btn  btn-success btn-sm glyphicon glyphicon-cog" data-toggle="tooltip" data-placement="top" title="ثبت ادارات"></asp:LinkButton>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <asp:GridView ID="gvdata" runat="server" CellPadding="3" GridLines="Horizontal" AutoGenerateColumns="False"
                                        Width="98%" UseAccessibleHeader="False" DataKeyNames="ID" BackColor="White" BorderColor="#E7E7FF"
                                        BorderStyle="None" BorderWidth="1px" AllowPaging="True"
                                        OnPageIndexChanging="gvdata_PageIndexChanging"
                                        AllowSorting="True" PageSize="50">
                                        <AlternatingRowStyle BackColor="#F7F7F7" />
                                        <Columns>
                                            <asp:TemplateField HeaderText="کد اداره یا سازمان" SortExpression="ID">
                                                <ItemTemplate><%# (Eval("ID").ToString())%></ItemTemplate>
                                                <ItemStyle Width="8%" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="نام اداره یا سازمان" SortExpression="NameOffice">
                                                <ItemTemplate><%#MyObjects.myclass.SubString(Eval("NameOffice").ToString(),50)%></ItemTemplate>
                                                <ItemStyle Width="50%" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="عملیات">
                                                <ItemTemplate>
                                                    <asp:LinkButton runat="server" ID="lbtn_edit_fergir" CommandArgument='<%#Eval("ID")%>' CommandName="do_edit_fergir" CssClass="btn btn-success btn-xs glyphicon glyphicon-edit" data-id='<%#Eval("ID")%>' data-toggle="tooltip" data-placement="top" title="ویرایش اداره یا سازمان" Width="30px"></asp:LinkButton>
                                                    <asp:LinkButton runat="server" ID="lbtn_delete_fergir" CommandArgument='<%#Eval("ID")%>' CommandName="do_delete_fergir" CssClass="btn btn-danger btn-xs glyphicon glyphicon-trash" data-id='<%#Eval("ID")%>' data-toggle="tooltip" data-placement="top" title="حذف اداره یا سازمان" Width="30px"></asp:LinkButton>
                                                </ItemTemplate>
                                                <ItemStyle Width="10%" />
                                            </asp:TemplateField>
                                        </Columns>
                                        <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
                                        <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" HorizontalAlign="Center" VerticalAlign="Middle" Font-Size="Small" />
                                        <PagerSettings Position="TopAndBottom" />
                                        <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Center" VerticalAlign="Middle" Font-Bold="true" CssClass="mypager" />
                                        <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Center" VerticalAlign="Middle" Font-Size="small" Font-Bold="true" Height="30px" />
                                        <SelectedRowStyle BackColor="#738A9C" ForeColor="#F7F7F7" Font-Bold="True" />
                                        <SortedAscendingCellStyle BackColor="#F4F4FD" />
                                        <SortedAscendingHeaderStyle BackColor="#5A4C9D" />
                                        <SortedDescendingCellStyle BackColor="#D8D8F0" />
                                        <SortedDescendingHeaderStyle BackColor="#3E3277" />
                                    </asp:GridView>
                                </ContentTemplate>
                                <Triggers>
                                    <%--                                        <asp:PostBackTrigger ControlID="lbt_print_fergir" />--%>
                                </Triggers>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
            <%--</div>--%>
        </div>
    </div>
    <%-- End Grid view                          -------------%>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0px; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <%--<span style="border-width: 0px; position: fixed; padding: 50px; background-color: #FFFFFF; font-size: 36px; left: 40%; top: 40%;">Loading ...</span>--%>
                <%--<img alt="" src="../images/loader.gif" />--%>
                <div class="loader" style="top: 300px">Loading...</div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
