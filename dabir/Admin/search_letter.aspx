﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPageAdmin.Master" AutoEventWireup="true" CodeBehind="search_letter.aspx.cs" Inherits="dabir.Admin.search_letter" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!---------------------------------------Display Errore Message-------------------------------------------->
    <%-- Start Dialog Function                 --------------%>
    <%-- Dialog Sabt Out    --%>
    <script type="text/javascript">
        // cal ajax methode for save data
        $(document).ready(function () {
            $("#sabt_out_letter_dialog").dialog(
          {
              autoOpen: false,
              resizable: false,
              modal: true,
              bgiframe: true,
              width: 700,
              height: 250,
              title: "ثبت نامه صادره",
              open: function (event, ui) { $(".ui-dialog-titlebar-close").hide(); },
              show: {
                  effect: "fade",
                  duration: 50
              },
              hide: {
                  effect: "fade",
                  //effect: "fade",
                  duration: 50
              },
              close: function (event, ui) {
                  var UpdatePanel1 = '<%=UpdatePanel1.ClientID%>';
                  if (UpdatePanel1 != null)
                      __doPostBack(UpdatePanel1, '');
                  //$("#form1").data("bootstrapValidator").resetForm(true);
              },
              buttons: {
                  'ثبت و ادامه': function () {
                      //var ckeditor = $('#txt_con_letter').val();
                      $.ajax({
                          type: 'POST',
                          contentType: "application/json; charset=utf-8",
                          url: 'sabt_letter.aspx/sabt_out_letter_method',
                          data: "{" +
                              "'id_letter':'" + $('#hfid_letter').val() + "'," +
                              //"'sh_leter':'" + $('#txt_sh_letter').val() + "'," +
                              // "'dte_letter':'" + $('#txt_dte_letter').val() + "'," +
                               "'moz_letter':'" + $('#txt_moz_letter_out').val() + "'," +
                               //"'fer_id_letter':'" + $('#ddl_fer_id_letter').val() + "'," +
                               "'gir_id_letter':'" + $('#ddl_gir_id_letter_out').val() + "'," +
                               "'user_id_letter':'" +<%=Session["id_user"].ToString()%> +"'" +
<%--                               "'dte_sabt_letter':'" +<%=Session["id_user"].ToString()%> +"'," +--%>
                               //"'con_letter':'" + CKEDITOR.instances['txt_con_letter'].getData() + "'," +
                               //"'att_letter':'" + $('#txt_att_letter').val() + "'" +
                           "}",
                          async: false,
                          success: function (response) {
                              if (response.d == 'true') {
                                  //$('#txt_sh_letter').val('');
                                  //$('#txt_dte_letter').val('');
                                  //$('#txt_moz_letter_out').val('');
                                  //$('#ddl_gir_id_letter_out').val('');
                                  ////CKEDITOR.instances['txt_con_letter'].setData('');
                                  //$('#txt_att_letter').val('');

                                  alert("داده ها با موفقیت ثبت شد");
                                  var UpdatePanel1 = '<%=UpdatePanel1.ClientID%>';
                                  if (UpdatePanel1 != null)
                                      __doPostBack(UpdatePanel1, '');
                                  //$("#form1").data("bootstrapValidator").resetForm(true);
                                  //if ($('#hfid_letter').val() != '-1')
                                  jQuery('#sabt_out_letter_dialog').dialog('close');
                                  return true;
                              }
                              else {
                                  alert("خطا در ثبت داده ها");
                                  return false;
                              }

                          },
                          error: function ()
                          { alert("خطا در ثبت داده ها"); jQuery('#sabt_out_letter_dialog').dialog('close'); return false; }
                      });

                  },
                  'انصراف': function () {
                      var UpdatePanel1 = '<%=UpdatePanel1.ClientID%>';
                      if (UpdatePanel1 != null)
                          __doPostBack(UpdatePanel1, '');
                      //$("#form1").data("bootstrapValidator").resetForm(true);
                      jQuery('#sabt_out_letter_dialog').dialog('close');
                  }
              }
          });
        });
    </script>
    <%-- Dialog Sabt In     --%>
    <script type="text/javascript">
        // cal ajax methode for save data
        $(document).ready(function () {
            $("#sabt_in_letter_dialog").dialog(
          {
              autoOpen: false,
              resizable: false,
              modal: false,
              bgiframe: true,
              width: 700,
              height: 300,
              title: "ثبت نامه وارده",
              open: function (event, ui) { $(".ui-dialog-titlebar-close").hide(); },
              show: {
                  effect: "fade",
                  duration: 50
              },
              hide: {
                  effect: "explode",
                  //effect: "fade",
                  duration: 50
              },
              close: function (event, ui) {
                  var UpdatePanel1 = '<%=UpdatePanel1.ClientID%>';
                  if (UpdatePanel1 != null)
                      __doPostBack(UpdatePanel1, '');
                  //$("#form1").data("bootstrapValidator").resetForm(true);
              },
              buttons: {
                  'ثبت و ادامه': function () {
                      //var ckeditor = $('#txt_con_letter').val();
                      $.ajax({
                          type: 'POST',
                          contentType: "application/json; charset=utf-8",
                          url: 'sabt_letter.aspx/sabt_in_letter_method',
                          data: "{" +
                              "'id_letter':'" + $('#hfid_letter').val() + "'," +
                              "'sh_leter':'" + $('#txt_sh_letter_in').val() + "'," +
                               "'dte_letter':'" + $('#txt_dte_letter_in').val() + "'," +
                               "'moz_letter':'" + $('#txt_moz_letter_in').val() + "'," +
                               "'fer_id_letter':'" + $('#ddl_fer_id_letter_in').val() + "'," +
                               //"'gir_id_letter':'" + $('#ddl_gir_id_letter_out').val() + "'," +
                               "'user_id_letter':'" +<%=Session["id_user"].ToString()%> +"'" +
<%--                               "'dte_sabt_letter':'" +<%=Session["id_user"].ToString()%> +"'," +--%>
                               //"'con_letter':'" + CKEDITOR.instances['txt_con_letter'].getData() + "'," +
                               //"'att_letter':'" + $('#txt_att_letter').val() + "'" +
                           "}",
                          async: false,
                          success: function (response) {
                              if (response.d == 'true') {
                                  //$('#txt_sh_letter').val('');
                                  //$('#txt_dte_letter').val('');
                                  //$('#txt_moz_letter_out').val('');
                                  //$('#ddl_gir_id_letter_out').val('');
                                  ////CKEDITOR.instances['txt_con_letter'].setData('');
                                  //$('#txt_att_letter').val('');

                                  alert("داده ها با موفقیت ثبت شد");
                                  var UpdatePanel1 = '<%=UpdatePanel1.ClientID%>';
                                  if (UpdatePanel1 != null)
                                      __doPostBack(UpdatePanel1, '');
                                  //$("#form1").data("bootstrapValidator").resetForm(true);
                                  //if ($('#hfid_letter').val() != '-1')
                                  jQuery('#sabt_in_letter_dialog').dialog('close');
                                  return true;
                              }
                              else {
                                  alert("خطا در ثبت داده ها");
                                  return false;
                              }

                          },
                          error: function ()
                          { alert("خطا در ثبت داده ها"); jQuery('#sabt_in_letter_dialog').dialog('close'); return false; }
                      });

                  },
                  'انصراف': function () {
                      var UpdatePanel1 = '<%=UpdatePanel1.ClientID%>';
                      if (UpdatePanel1 != null)
                          __doPostBack(UpdatePanel1, '');
                      //$("#form1").data("bootstrapValidator").resetForm(true);
                      jQuery('#sabt_in_letter_dialog').dialog('close');
                  }
              }
          });
        });
    </script>
    <%-- Dialog Delete      --%>
    <script type="text/javascript">
        // cal ajax methode for delete data
        $(document).ready(function () {
            $('#delete_letter_dialog').dialog(
{
    autoOpen: false,
    resizable: true,
    modal: true,
    bgiframe: true,
    width: 300,
    height: 175,
    title: "حذف داده ها",
    open: function (event, ui) { $(".ui-dialog-titlebar-close").hide(); },
    show: {
        effect: "fade",
        duration: 50
    },
    hide: {
        effect: "explode",
        //effect: "fade",
        duration: 50
    },
    close: function (event, ui) {
        var UpdatePanel1 = '<%=UpdatePanel1.ClientID%>';
        if (UpdatePanel1 != null)
            __doPostBack(UpdatePanel1, '');
        //$("#form1").data("bootstrapValidator").resetForm(true);
    },
    buttons: {
        'حذف': function () {
            $.ajax({
                type: 'POST',
                contentType: "application/json; charset=utf-8",
                url: 'sabt_letter.aspx/delete_letter_method',
                data: "{'id_letter':'" + $('#hfid_letter').val() + "'" +
                           "}",
                async: false,
                success: function (response) {
                    if (response.d == 'true') {
                        alert("داده با موفقیت حذف شد");
                        jQuery('#delete_letter_dialog').dialog('close');
                        return true;
                    }
                    else {
                        alert("خطا در حذف داده ها");
                        jQuery('#delete_letter_dialog').dialog('close');
                        return false;
                    }

                },
                error: function ()
                { alert("خطا در حذف داده ها"); jQuery('#delete_letter_dialog').dialog('close'); return false; }
            });

        },
        'انصراف': function () {
            var UpdatePanel1 = '<%=UpdatePanel1.ClientID%>';
            if (UpdatePanel1 != null)
                __doPostBack(UpdatePanel1, '');
            //$("#form1").data("bootstrapValidator").resetForm(true);
            jQuery('#delete_letter_dialog').dialog('close');
        }
    }
});

        });
    </script>
    <%-- Dialog Content     --%>
    <script type="text/javascript">
        $(document).ready(function () {
            //cal ajax method for content letter
            $("#content_letter_dialog").dialog(
          {
              autoOpen: false,
              resizable: false,
              modal: false,
              bgiframe: true,
              width: 1000,
              height: 600,
              title: "ثبت متن نامه صادره",
              open: function (event, ui) {
                  $(".ui-dialog-titlebar-close").hide();
                  $.ajax({
                      type: "POST",
                      contentType: "application/json; charset=utf-8",
                      url: 'sabt_letter.aspx/get_content_method',
                      data: "{" +
                          "'id_letter':'" + $('#hfid_letter').val() + "'" +
                       "}",
                      async: false,
                      success: function (response) {
                          if (response.d != '') {
                              var con_letter = response.d;
                              CKEDITOR.instances['txt_con_letter_ed'].setData(con_letter);
                          }
                          else
                          {
                              CKEDITOR.instances['txt_con_letter_ed'].setData("");
                          }

                      },
                      error: function ()
                      { alert("خطا در ثبت داده ها"); return false; }
                  });

              },
              show: {
                  effect: "fade",
                  duration: 50
              },
              hide: {
                  effect: "explode",
                  //effect: "fade",
                  duration: 50
              },
              close: function (event, ui) {
<%--                  var UpdatePanel1 = '<%=UpdatePanel1.ClientID%>';
                  if (UpdatePanel1 != null)
                      __doPostBack(UpdatePanel1, '');--%>
                  //$("#form1").data("bootstrapValidator").resetForm(true);
              },
              buttons: {
                  'ثبت و ادامه': function () {
                      //var ckeditor = $('#txt_con_letter').val();
                      $.ajax({
                          type: 'POST',
                          contentType: "application/json; charset=utf-8",
                          url: 'sabt_letter.aspx/sabt_out_content_method',
                          data: "{" +
                              "'id_letter':'" + $('#hfid_letter').val() + "'," +
                               "'con_letter':'" + CKEDITOR.instances['txt_con_letter_ed'].getData() + "'" +
                           "}",
                          async: false,
                          success: function (response) {
                              if (response.d == 'true') {
                                  //CKEDITOR.instances['txt_con_letter_ed'].setData('');
                                  alert("داده ها با موفقیت ثبت شد");
<%--                                  var UpdatePanel1 = '<%=UpdatePanel1.ClientID%>';
                                  if (UpdatePanel1 != null)
                                      __doPostBack(UpdatePanel1, '');--%>
                                  //$("#form1").data("bootstrapValidator").resetForm(true);
                                  //if ($('#hfid_letter').val() != '-1')
                                  jQuery('#content_letter_dialog').dialog('close');
                                  return true;
                              }
                              else {
                                  alert("خطا در ثبت داده ها");
                                  jQuery('#content_letter_dialog').dialog('close');
                                  return false;
                              }

                          },
                          error: function ()
                          { alert("خطا در ثبت داده ها"); jQuery('#content_letter_dialog').dialog('close'); return false; }
                      });

                  },
                  'انصراف': function () {
<%--                      var UpdatePanel1 = '<%=UpdatePanel1.ClientID%>';
                      if (UpdatePanel1 != null)
                          __doPostBack(UpdatePanel1, '');--%>
                      //$("#form1").data("bootstrapValidator").resetForm(true);
                      jQuery('#content_letter_dialog').dialog('close');
                  },

              }
          });

        });
    </script>
    <%-- Start Call Dialog Update,Save,Delete   -------------%>
    <%-- SaveDialog Call Out    --%>
    <script type="text/javascript">
        $(document).ready(function () {
            //Sabt Dialog Call 
            //
            $(document).on("click", "[id*=lbtn_output_letter]", function () {
                //$('#txt_sh_letter').val('');
                //$('#txt_dte_letter').val('');
                $('#txt_moz_letter_out').val('');
                $('#ddl_gir_id_letter_out').val(-1);
                //CKEDITOR.instances['txt_con_letter'].setData('');
                //$('#txt_att_letter').val('');

                $('#txt_moz_letter_out').focus();
                //$('#txt_sh_letter').focus();
                $('#hfid_letter').val('-1');  //ClientIDMode="Static"
                //$("[id*=hfid_carddabir]").val('-1'); without ClientIDMode="Static"
                $('#sabt_out_letter_dialog').dialog('open'); return false;
            });
        });
    </script>
    <%-- SaveDialog Call In     --%>
    <script type="text/javascript">
        $(document).ready(function () {
            //Sabt Dialog Call 
            //
            $(document).on("click", "[id*=lbtn_input_letter]", function () {
                $('#txt_sh_letter_in').val('');
                $('#txt_dte_letter_in').val('');
                $('#txt_moz_letter_in').val('');
                $('#ddl_fer_id_letter_in').val(-1);
                //CKEDITOR.instances['txt_con_letter'].setData('');
                //$('#txt_att_letter').val('');

                $('#txt_moz_letter_in').focus();
                //$('#txt_sh_letter').focus();
                $('#hfid_letter').val('-1');  //ClientIDMode="Static"
                //$("[id*=hfid_carddabir]").val('-1'); without ClientIDMode="Static"
                $('#sabt_in_letter_dialog').dialog('open'); return false;
            });
        });
    </script>
    <%-- Update Dialog Call     --%>
    <script type="text/javascript">
        // cal ajax methode for save data
        $(document).ready(function () {

            //Update Dialog Call 
            //

            $(document).on("click", "[id*=lbt_edit_letter]", function (e) {
                var mydata = ($(this).attr('data-id'));
                $.ajax({
                    type: 'POST',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    url: 'sabt_letter.aspx/get_letter_byid',
                    data: "{" +
                        "'id_letter':'" + mydata + "'" +
                     "}",
                    async: false,
                    success: function (response) {
                        $.map($.parseJSON(response.d), function (item) {
                            $("[id*=txt_sh_letter_in]").val(item.sh_letter);
                            $("[id*=txt_dte_letter_in]").val(item.dte_letter);
                            $("[id*=txt_moz_letter_in]").val(item.moz_letter);
                            $("[id*=txt_moz_letter_out]").val(item.moz_letter);
                            $("[id*=ddl_gir_id_letter_out] option").filter(function () {
                                return $(this).text() == item.nam_gir;
                            }).prop('selected', true);
                            $("[id*=ddl_fer_id_letter_in] option").filter(function () {
                                return $(this).text() == item.nam_fer;
                            }).prop('selected', true);
                            $("[id*=hfid_letter]").val(mydata);
                            if (item.sh_letter == 'صادره' && item.dte_letter == '9999/99/99')
                                $('#sabt_out_letter_dialog').dialog('open');
                            else
                                $('#sabt_in_letter_dialog').dialog('open');

                            return true;
                        });
                    },
                    error: function () {
                        alert("خطای نا شناخته در باز کردن پنجره ویرایش");
                        return false;
                    }
                });
                //var row = $(this).parents("tr:first");
                ////var sh_letter = row.children("td:eq(1)").text();
                ////var dte_letter = row.children("td:eq(2)").text();
                //var moz_letter = row.children("td:eq(3)").text();
                //var gir_id_letter = row.children("td:eq(4)").text();
                ////var con_letter = row.children("td:eq(5)").html();
                ////var att_letter = row.children("td:eq(5)").text();

                ////$("[id*=txt_sh_letter]").val(sh_letter);
                ////$("[id*=txt_dte_letter]").val(dte_letter);
                //$("[id*=txt_moz_letter_out]").val(moz_letter);
                //$("[id*=ddl_gir_id_letter_out] option").filter(function () {
                //    return $(this).text() == gir_id_letter;
                //}).prop('selected', true);

                ////CKEDITOR.instances['txt_con_letter'].setData(con_letter)

                ////$("[id*=txt_att_letter]").val(att_letter);
                //$("[id*=hfid_letter]").val(mydata);
                //$('#sabt_out_letter_dialog').dialog('open'); return false;
            });
        });
    </script>
    <%-- Delete Dialog Call     --%>
    <script type="text/javascript">
        // cal ajax methode for save data
        $(document).ready(function () {
            //Delete Dialog Call 
            //
            $(document).on("click", "[id*=lbt_delete_letter]", function (e) {
                var mydata = ($(this).attr('data-id'));
                $("[id*=hfid_letter]").val(mydata);
                $('#delete_letter_dialog').dialog('open'); return false;
            });
        });
    </script>
    <%-- Content Dialog Call    --%>
    <script type="text/javascript">
        // cal ajax methode for save data
        $(document).ready(function () {

            //Content Dialog Call 
            //

            $(document).on("click", "[id*=lbt_content_letter_dialog]", function (e) {
                var mydata = ($(this).attr('data-id'));
                //var row = $(this).parents("tr:first");
                //var con_letter = row.children("td:eq(5)").html();

                //CKEDITOR.instances['txt_con_letter_ed'].setData(con_letter)

                $("[id*=hfid_letter]").val(mydata);
                $('#content_letter_dialog').dialog('open'); return false;
            });
        });
    </script>
    <asp:HiddenField ID="hfid_letter" runat="server" ClientIDMode="Static" />
    <%-- Start Message Box Sabt,Update          -------------%>
    <div id="sabt_out_letter_dialog" style="display: none">
        <div class="form-group  form-group-sm">
            <%--            <div class="row">
                <div class="col-sm-6">
                    <asp:HiddenField ID="hfid_letter" runat="server" ClientIDMode="Static" />
                    <asp:Label ID="Label1" runat="server" Text="شماره نامه"></asp:Label>
                    <asp:TextBox ID="txt_sh_letter" runat="server" Width="220px" CssClass="form-control" ClientIDMode="Static" placeholder="شماره نامه را وارد کنید" lang="fa"></asp:TextBox>
                </div>
                <div class="col-sm-6">
                    <asp:Label ID="Label2" runat="server" Text="تاریخ نامه"></asp:Label>
                    <asp:TextBox ID="txt_dte_letter" runat="server" Width="220px" CssClass="form-control" ClientIDMode="Static" placeholder="تاریخ نامه را وارد کنید" lang="fa"></asp:TextBox>
                </div>
            </div>--%>
            <div class="row">
                <div class="col-sm-12">
                    <asp:Label ID="Label3" runat="server" Text="موضوع نامه"></asp:Label>
                    <asp:TextBox ID="txt_moz_letter_out" runat="server" Width="555px" CssClass="form-control" ClientIDMode="Static" placeholder="موضوع نامه را وارد کنید" lang="fa"></asp:TextBox>
                </div>
                <div class="col-sm-6">
                    <asp:Label ID="Label4" runat="server" Text="گیرنده نامه"></asp:Label>
                    <asp:DropDownList ID="ddl_gir_id_letter_out" runat="server" Width="555px" CssClass="form-control" ClientIDMode="Static" lang="fa"></asp:DropDownList>
                </div>
            </div>
            <div class="row">
                <%--                <div class="col-sm-12">
                    <asp:Label ID="Label6" runat="server" Text="متن نامه"></asp:Label>
                    <CKEditor:CKEditorControl ID="txt_con_letter" runat="server" Height="200" ClientIDMode="Static"></CKEditor:CKEditorControl>
                </div>--%>
                <%--                <div class="col-sm-6">
                    <asp:Label ID="Label7" runat="server" Text="پیوست نامه"></asp:Label>
                    <asp:TextBox ID="txt_att_letter" runat="server" Width="220px" CssClass="form-control" ClientIDMode="Static" placeholder="پیوست نامه را وارد کنید" MaxLength="10" lang="fa"></asp:TextBox>
                </div>--%>
            </div>

        </div>
    </div>
    <%-- End Message Box Sabt,Update            -------------%>
    <%-- Start Message Box Sabt,Update          -------------%>
    <div id="sabt_in_letter_dialog" style="display: none">
        <div class="form-group  form-group-sm">
            <div class="row">
                <div class="col-sm-6">
                    <asp:Label ID="Label1" runat="server" Text="شماره نامه"></asp:Label>
                    <asp:TextBox ID="txt_sh_letter_in" runat="server" Width="220px" CssClass="form-control" ClientIDMode="Static" placeholder="شماره نامه را وارد کنید" lang="fa"></asp:TextBox>
                </div>
                <div class="col-sm-6">
                    <asp:Label ID="Label2" runat="server" Text="تاریخ نامه"></asp:Label>
                    <asp:TextBox ID="txt_dte_letter_in" runat="server" Width="220px" CssClass="form-control" ClientIDMode="Static" placeholder="تاریخ نامه را وارد کنید" lang="fa"></asp:TextBox>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <asp:Label ID="Label5" runat="server" Text="موضوع نامه"></asp:Label>
                    <asp:TextBox ID="txt_moz_letter_in" runat="server" Width="555px" CssClass="form-control" ClientIDMode="Static" placeholder="موضوع نامه را وارد کنید" lang="fa"></asp:TextBox>
                </div>
                <div class="col-sm-6">
                    <asp:Label ID="Label6" runat="server" Text="فرستنده نامه"></asp:Label>
                    <asp:DropDownList ID="ddl_fer_id_letter_in" runat="server" Width="555px" CssClass="form-control" ClientIDMode="Static" lang="fa"></asp:DropDownList>
                </div>
            </div>
            <div class="row">
                <%--                <div class="col-sm-12">
                    <asp:Label ID="Label6" runat="server" Text="متن نامه"></asp:Label>
                    <CKEditor:CKEditorControl ID="txt_con_letter" runat="server" Height="200" ClientIDMode="Static"></CKEditor:CKEditorControl>
                </div>--%>
                <%--                <div class="col-sm-6">
                    <asp:Label ID="Label7" runat="server" Text="پیوست نامه"></asp:Label>
                    <asp:TextBox ID="txt_att_letter" runat="server" Width="220px" CssClass="form-control" ClientIDMode="Static" placeholder="پیوست نامه را وارد کنید" MaxLength="10" lang="fa"></asp:TextBox>
                </div>--%>
            </div>

        </div>
    </div>
    <%-- End Message Box Sabt,Update            -------------%>
    <%-- Start Message Box Content Letter       -------------%>
    <div id="content_letter_dialog" style="display: none">
        <div class="form-group  form-group-sm">
            <div class="row">
                <div class="col-sm-12">
                    <asp:Label ID="Label11" runat="server" Text="متن نامه"></asp:Label>
                    <CKEditor:CKEditorControl ID="txt_con_letter_ed" runat="server" Height="330" ClientIDMode="Static" Skin="moono" lang="fa"></CKEditor:CKEditorControl>
                    <%--                                        <asp:TextBox ID="txt_con_letter" runat="server" Width="220px" CssClass="form-control" ClientIDMode="Static" placeholder="متن نامه را وارد کنید" MaxLength="10"></asp:TextBox>--%>
                </div>
            </div>

        </div>
    </div>
    <%-- End Message Box Content Letter         -------------%>
    <%-- Start Message Box Delete               -------------%>
    <div id="delete_letter_dialog" style="display: none">
        <p>
            <br />
            آیا برای حذف اطمینان دارید؟
        </p>
    </div>
    <%-- End Message Box Delete                 -------------%>
    <%-- Start Grid view                        -------------%>
    <div class="container full-width">
        <!--------------------------------------------------Message Box--------------------------------------------->
        <div class="row">
            <asp:HiddenField ID="messagetexthf" runat="server" />
            <div class="col-sm-12" id="message" style="display: none">
                <div id="messagetext" class="alert-danger text-center">پیام خطا</div>
            </div>
        </div>
        <!--------------------------------------------------------------------------------------------------------->
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-info">
                    <div class="panel-body">
                        <div class="row">
                            <%--                            <div class="col-sm-12">--%>
                            <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <%-- بخش جستجو در سایت --%>
                                        <div class="form-inline">
                                            <div class="form-group form-group-sm">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <asp:Label ID="lbl_search_text" runat="server" CssClass="alert-warning" Font-Size="Smaller"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <asp:DropDownList ID="ddl_search_field" runat="server" CssClass="form-control" Width="120px"></asp:DropDownList>
                                                        <asp:DropDownList ID="ddl_search_option" runat="server" CssClass="form-control" Width="120px"></asp:DropDownList>
                                                        <asp:TextBox ID="txt_search" runat="server" CssClass="form-control" Width="120px"></asp:TextBox>
                                                        <asp:DropDownList ID="ddl_search_combine" runat="server" CssClass="form-control" Width="120px"></asp:DropDownList>
                                                        <asp:LinkButton ID="lbtn_add_search" runat="server" CssClass="btn btn-primary btn-sm glyphicon glyphicon-plus" data-toggle="tooltip" data-placement="top"  title="اضافه شدن به جستجو" OnClick="lbtn_add_search_Click"></asp:LinkButton>
                                                        <asp:LinkButton ID="LinkButton1" runat="server" CssClass="btn btn-primary btn-sm glyphicon glyphicon-search" data-toggle="tooltip" data-placement="top"  title="جستجو" OnClick="lbtn_search_Click"></asp:LinkButton>
                                                        <asp:LinkButton ID="lbtn_search_clear" runat="server" CssClass="btn btn-danger btn-sm glyphicon glyphicon-remove" data-toggle="tooltip" data-placement="top"  title="پاک کردن جستجو" OnClick="lbtn_search_clear_Click"></asp:LinkButton>
                                                        <asp:LinkButton ID="lbtn_back" runat="server" CssClass="btn btn-warning btn-sm glyphicon glyphicon-backward" data-toggle="tooltip" data-placement="top"  title="بازگشت به صفحه قبل" OnClick="lbtn_back_Click"></asp:LinkButton>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <%-- بخش فیلتر تاریخ در سایت --%>
<%--                                    <div class="form-inline">
                                        <div class="form-group form-group-sm">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <asp:LinkButton ID="lbtn_lastweek" runat="server" CssClass="btn  btn-primary btn-sm glyphicon glyphicon-arrow-left" data-toggle="tooltip" data-placement="top" title="هفته قبل" OnClick="lbtn_lastweek_Click"></asp:LinkButton>
                                                    <asp:Label ID="lbl_lastweek" runat="server">از</asp:Label>
                                                    <asp:TextBox ID="txt_lastweek" runat="server" CssClass="form-control" Width="90px"></asp:TextBox>
                                                    <asp:Label ID="lbl_nextweek" runat="server">تا</asp:Label>
                                                    <asp:TextBox ID="txt_nextweek" runat="server" CssClass="form-control" Width="90px"></asp:TextBox>
                                                    <asp:LinkButton ID="lbtn_nextweek" runat="server" CssClass="btn  btn-primary btn-sm glyphicon glyphicon-arrow-right" data-toggle="tooltip" data-placement="top" title="هفته بعد" OnClick="lbtn_nextweek_Click"></asp:LinkButton>
                                                    <asp:LinkButton ID="lbtn_okweek" runat="server" CssClass="btn  btn-primary btn-sm glyphicon glyphicon-ok" data-toggle="tooltip" data-placement="top" title="تائید تاریخ" OnClick="lbtn_okweek_Click"></asp:LinkButton>
                                                    <asp:LinkButton ID="lbtn_search" runat="server" CssClass="btn btn-primary btn-sm glyphicon glyphicon-search" data-toggle="tooltip" data-placement="top" title="جستجو" OnClick="lbtn_search_Click"></asp:LinkButton>
                                                    <asp:LinkButton ID="lbtn_input_letter" runat="server" CssClass="btn  btn-warning btn-sm glyphicon glyphicon-save-file" data-toggle="tooltip" data-placement="top" title="ثبت نامه وارده"></asp:LinkButton>
                                                    <asp:LinkButton ID="lbtn_output_letter" runat="server" CssClass="btn  btn-success btn-sm glyphicon glyphicon-open-file" data-toggle="tooltip" data-placement="top" title="ثبت نامه صادره"></asp:LinkButton>
                                                </div>
                                            </div>
                                        </div>
                                    </div>--%>
                                    <asp:GridView ID="gvdata" runat="server" CellPadding="3" GridLines="Horizontal" AutoGenerateColumns="False"
                                        Width="98%" UseAccessibleHeader="False" DataKeyNames="id_letter" BackColor="White" BorderColor="#E7E7FF"
                                        BorderStyle="None" BorderWidth="1px" AllowPaging="True"
                                        OnPageIndexChanging="gvdata_PageIndexChanging" OnRowCommand="gvdata_RowCommand"
                                        OnRowDataBound="gvdata_RowDataBound"
                                        AllowSorting="True" OnSorting="gvdata_Sorting" PageSize="7">
                                        <AlternatingRowStyle BackColor="#F7F7F7" />
                                        <Columns>
                                            <asp:TemplateField HeaderText="ردیف" Visible="false">
                                                <ItemTemplate><%# Container.DataItemIndex +1 %></ItemTemplate>
                                                <ItemStyle Width="2%" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="شماره اندیکاتور" SortExpression="id_letter">
                                                <ItemTemplate><%# (Eval("id_letter").ToString())%></ItemTemplate>
                                                <ItemStyle Width="10%" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="شماره نامه" SortExpression="sh_letter">
                                                <ItemTemplate><%# (Eval("sh_letter").ToString())%></ItemTemplate>
                                                <ItemStyle Width="10%" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="تاریخ نامه">
                                                <ItemTemplate><%#Eval("dte_letter").ToString()%></ItemTemplate>
                                                <ItemStyle Width="5%" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="موضوع نامه">
                                                <ItemTemplate><%#MyObjects.myclass.SubString(Eval("moz_letter").ToString(),30)%></ItemTemplate>
                                                <ItemStyle Width="25%" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="فرستنده نامه">
                                                <ItemTemplate><%#MyObjects.myclass.SubString(Eval("tbl_fer.nam_fergir").ToString(),20)%></ItemTemplate>
                                                <ItemStyle Width="10%" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="گیرنده نامه">
                                                <ItemTemplate><%#MyObjects.myclass.SubString(Eval("tbl_gir.nam_fergir").ToString(),20)%></ItemTemplate>
                                                <ItemStyle Width="10%" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="متن نامه" Visible="false">
                                                <ItemTemplate><%#Eval("con_letter").ToString()%></ItemTemplate>
                                                <ItemStyle Width="10%" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="پیوست نامه" Visible="false">
                                                <ItemTemplate><%#Eval("att_letter")==null?"ندارد":Eval("att_letter").ToString()%></ItemTemplate>
                                                <ItemStyle Width="10%" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="تاریخ ثبت نامه">
                                                <ItemTemplate><%#Eval("dte_sabt_letter").ToString()%></ItemTemplate>
                                                <ItemStyle Width="5%" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="عملیات">
                                                <ItemTemplate>
                                                    <asp:LinkButton runat="server" ID="lbt_edit_letter" CommandArgument='<%#Eval("id_letter")%>' CommandName="do_edit_letter" CssClass="btn btn-success btn-xs glyphicon glyphicon-edit" data-id='<%#Eval("id_letter")%>' Width="30px"></asp:LinkButton>
                                                    <asp:LinkButton runat="server" ID="lbt_delete_letter" CommandArgument='<%#Eval("id_letter")%>' CommandName="do_delete_letter" CssClass="btn btn-danger btn-xs glyphicon glyphicon-trash" data-id='<%#Eval("id_letter")%>' Width="30px"></asp:LinkButton>
                                                    <asp:LinkButton runat="server" ID="lbt_content_letter_dialog" CommandArgument='<%#Eval("id_letter")%>' CommandName="do_content_letter_dialog" CssClass="btn btn-primary btn-xs glyphicon glyphicon-pencil" data-id='<%#Eval("id_letter")%>' Width="30px"></asp:LinkButton>
                                                    <asp:LinkButton runat="server" ID="lbt_print_letter" CommandArgument='<%#Eval("id_letter")%>' CommandName="do_print_letter" CssClass="btn btn-info btn-xs glyphicon glyphicon-print" data-id='<%#Eval("id_letter")%>' Width="30px"></asp:LinkButton>
                                                </ItemTemplate>
                                                <ItemStyle Width="13%" />
                                            </asp:TemplateField>
                                        </Columns>
                                        <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
                                        <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" HorizontalAlign="Center" VerticalAlign="Middle" Font-Size="Small" />
                                        <PagerSettings Position="TopAndBottom" />
                                        <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Center" VerticalAlign="Middle" Font-Bold="true" CssClass="mypager" />
                                        <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Center" VerticalAlign="Middle" Font-Size="small" Font-Bold="true" Height="30px" />
                                        <SelectedRowStyle BackColor="#738A9C" ForeColor="#F7F7F7" Font-Bold="True" />
                                        <SortedAscendingCellStyle BackColor="#F4F4FD" />
                                        <SortedAscendingHeaderStyle BackColor="#5A4C9D" />
                                        <SortedDescendingCellStyle BackColor="#D8D8F0" />
                                        <SortedDescendingHeaderStyle BackColor="#3E3277" />
                                    </asp:GridView>
                                </ContentTemplate>
                                <Triggers>
                                    <%--                                        <asp:PostBackTrigger ControlID="lbt_print_letter" />--%>
                                </Triggers>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
            <%--</div>--%>
        </div>
    </div>
    <%-- End Grid view                          -------------%>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0px; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <%--<span style="border-width: 0px; position: fixed; padding: 50px; background-color: #FFFFFF; font-size: 36px; left: 40%; top: 40%;">Loading ...</span>--%>
                <%--<img alt="" src="../images/loader.gif" />--%>
                <div class="loader" style="top: 300px">Loading...</div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
