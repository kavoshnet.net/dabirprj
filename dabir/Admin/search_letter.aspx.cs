﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Globalization;
using EntityFramework.Extensions;
using MyObjects;
using DataLayer.DataAccessLayer;
using DomainClass.DomainModel;
using NReco.PdfGenerator;
using System.Web.Script.Serialization;
using System.IO;

namespace dabir.Admin
{
    /// <summary>
    /// کنترل صفحات در این قسمت انجام میگیرد
    /// </summary>
    public partial class search_letter : System.Web.UI.Page
    {
        /// <summary>
        /// متغیری برای دسترسی به کل مدل تولید شده
        /// </summary>
        DataBaseContext _edbcontext = new DataBaseContext();
        #region userfunction
        /// <summary>
        /// تنظیم کردن ویرایشگر
        /// </summary>
        protected void CKEditorConfig()
        {
            //CKEditor1.config.uiColor = "#BFEE62";
            //txt_con_letter.config.language = "fa";
            //            txt_con_letter_ed.config.font_names = "Tahoma";
            txt_con_letter_ed.config.font_defaultLabel = "Tahoma";
            txt_con_letter_ed.config.fontSize_defaultLabel = "16";
            txt_con_letter_ed.config.language = "fa";
            //txt_con_letter_ed.config.bodyClass = "{font-family:Tahoma}";
            txt_con_letter_ed.ResizeEnabled = false;
            txt_con_letter_ed.config.toolbar = new object[]
            {
                new object[]{ "NewPage", "Preview", "-", "Templates"},
                new object[]{ "Cut","Copy","Paste","PasteText","PasteWord","-","Undo","Redo"},
                new object[]{ "SelectAll","RemoveFormat"},
                new object[]{ "Bold","Italic","Underline"},
                new object[]{ "NumberedList", "BulletedList", "-", "Outdent", "Indent","-","Blockquote"},
                new object[]{ "JustifyLeft","JustifyCenter","JustifyRight", "JustifyBlock", "JustifyFull","-","BidiLtr","BidiRtl"},
                new object[]{ "Styles","Format","Font","FontSize"},
                new object[]{ "TextColor","BGColor"},
                new object[]{ "Table"},
            };
            //CKEditor1.config.removePlugins = "link";
            //CKEditor1.config.enterMode = EnterMode.BR;
            //CKEditor1.config.toolbar = new object[]
            //{
            // new object[] { "Source", "-", "Save", "NewPage", "Preview", "-", "Templates" },
            // new object[] { "Link", "Unlink", "Anchor" },
            //};

            //txt_con_letter_ed.config.toolbar = new object[]
            //{
            //    new object[]{ "Source", "DocProps", "-", "Save", "NewPage", "Preview", "-", "Templates"},
            //    new object[]{ "Cut","Copy","Paste","PasteText","PasteWord","-","Print","SpellCheck"},
            //    new object[]{"Undo","Redo","-","Find","Replace","-","SelectAll","RemoveFormat"},
            //    new object[]{"Form","Checkbox","Radio","TextField","Textarea","Select","Button","ImageButton","HiddenField"},
            //    "/",
            //    new object[]{"Bold","Italic","Underline","StrikeThrough","-","Subscript","Superscript"},
            //    new object[]{"OrderedList","UnorderedList","-","Outdent","Indent","Blockquote"},
            //    new object[]{"JustifyLeft","JustifyCenter","JustifyRight","JustifyFull"},
            //    new object[]{"Link","Unlink","Anchor"},
            //    new object[]{"Image","Flash","Table","Rule","Smiley","SpecialChar","PageBreak"},
            //    "/",
            //    new object[]{"Style","FontFormat","FontName","FontSize"},
            //    new object[]{"TextColor","BGColor"},
            //    new object[]{"FitWindow","ShowBlocks","-","About"}, // No comma for the last row.
            //};

            //CKEditor1.config.toolbar = new object[]
            //{
            //   new object[] { "Cut", "Copy", "Paste", "PasteText", "PasteWord" },
            //   new object[] { "Undo","Redo","-","Bold","Italic","Underline","StrikeThrough" },
            //   "/",
            //   new object[] {"OrderedList","UnorderedList","-","Outdent","Indent" },
            //   new object[] {"Link","Unlink","Anchor" },
            //   "/",
            //   new object[] {"Style" },
            //   new object[] {"Table","Image","Flash","Rule","SpecialChar" },
            //   new object[] {"About" }
            //};
        }
        /// <summary>
        /// تابعی برای پر کردن اطلاعات گرید ویو
        /// که بر اساس عنوان صفحه مرتب می گردد
        /// </summary>
        protected void SetGVdata(string VStateSearch, string SortExp, string SortDir)
        {
            if (VStateSearch != string.Empty)
            {
                string laststring = VStateSearch.Substring(0, VStateSearch.Length - 1).Split(' ').Last();
                if (laststring != "AND" && laststring != "OR")
                {
                    string sqlquery = "SELECT  * FROM V_tbl_letter WHERE " + ViewState["search"].ToString();
                    _edbcontext.Letters.SqlQuery(sqlquery).ToList();
                    gvdata.DataSource = _edbcontext.Letters.SqlQuery(sqlquery).ToList();
                }
            }
            else
            {
                string sqlquery = "SELECT  * FROM V_tbl_letter WHERE id_letter = -1";
                _edbcontext.Letters.SqlQuery(sqlquery).ToList();
                gvdata.DataSource = _edbcontext.Letters.SqlQuery(sqlquery).ToList();
            }
            gvdata.DataBind();
        }
        /// <summary>
        /// تابعی برای پر کردن لیست کشویی گیرنده ها
        /// </summary>
        protected void setddl_fergir_letter_out()
        {
            ddl_gir_id_letter_out.DataSource = (from e in _edbcontext.Offices
                                                orderby e.ID ascending
                                                select e).ToList();
            ddl_gir_id_letter_out.DataTextField = "nam_fergir";
            ddl_gir_id_letter_out.DataValueField = "id_fergir";
            ddl_gir_id_letter_out.DataBind();
            ddl_gir_id_letter_out.Items.Insert(0, new ListItem("انتخاب شود", "-1"));
            ddl_gir_id_letter_out.SelectedIndex = -1;
        }
        /// <summary>
        /// تابعی برای پر کردن لیست کشویی فرستنده ها
        /// </summary>
        protected void setddl_fergir_letter_in()
        {
            ddl_fer_id_letter_in.DataSource = (from e in _edbcontext.Offices
                                               orderby e.ID ascending
                                               select e).ToList();
            ddl_fer_id_letter_in.DataTextField = "nam_fergir";
            ddl_fer_id_letter_in.DataValueField = "id_fergir";
            ddl_fer_id_letter_in.DataBind();
            ddl_fer_id_letter_in.Items.Insert(0, new ListItem("انتخاب شود", "-1"));
            ddl_fer_id_letter_in.SelectedIndex = -1;
        }
        /// <summary>
        /// متدی برای پر کردن لیست کشویی فیلدها
        /// </summary>
        protected void setddl_search_field()
        {
            //ddl_search_field.DataSource = (from e in _edbcontext.tbl_fergir
            //                                orderby e.id_fergir ascending
            //                                select e).ToList();
            //ddl_search_field.DataTextField = "nam_fergir";
            //ddl_search_field.DataValueField = "id_fergir";
            //ddl_search_field.DataBind();
            ddl_search_field.Items.Insert(0, new ListItem("انتخاب شود", "-1"));
            ddl_search_field.Items.Insert(1, new ListItem("شماره اندیکاتور", "id_letter"));
            ddl_search_field.Items.Insert(2, new ListItem("شماره نامه", "sh_letter"));
            ddl_search_field.Items.Insert(3, new ListItem("موضوع نامه", "moz_letter"));
            ddl_search_field.Items.Insert(4, new ListItem("فرستنده", "[tbl_fer.nam_fergir]"));
            ddl_search_field.Items.Insert(5, new ListItem("گیرنده", "[tbl_gir.nam_fergir]"));
            ddl_search_field.Items.Insert(6, new ListItem("تاریخ ثبت نامه", "dte_sabt_letter"));
            ddl_search_field.SelectedIndex = 0;
        }
        /// <summary>
        /// متدی برای پر کردن لیست کشویی گزینه های مقایسه ای
        /// </summary>
        protected void setddl_search_option()
        {
            //ddl_search_field.DataSource = (from e in _edbcontext.tbl_fergir
            //                                orderby e.id_fergir ascending
            //                                select e).ToList();
            //ddl_search_field.DataTextField = "nam_fergir";
            //ddl_search_field.DataValueField = "id_fergir";
            //ddl_search_field.DataBind();
            ddl_search_option.Items.Insert(0, new ListItem("انتخاب شود", "-1"));
            ddl_search_option.Items.Insert(1, new ListItem("شامل", "LIKE"));
            ddl_search_option.Items.Insert(2, new ListItem("برابر", "="));
            ddl_search_option.Items.Insert(3, new ListItem("نابرابر", "<>"));
            ddl_search_option.Items.Insert(4, new ListItem("کوچکتراز", "<"));
            ddl_search_option.Items.Insert(5, new ListItem("کوچکترمساوی", "<="));
            ddl_search_option.Items.Insert(6, new ListItem("بزرگتراز", ">"));
            ddl_search_option.Items.Insert(7, new ListItem("بزرگترمساوی", ">="));
            //ddl_search_option.Items.Insert(0, new ListItem("شروع با", "7"));
            //ddl_search_option.Items.Insert(0, new ListItem("شروع نشود با", "8"));
            //ddl_search_option.Items.Insert(0, new ListItem("اتمام با", "9"));
            //ddl_search_option.Items.Insert(0, new ListItem("تمام نشود با", "10"));
            //ddl_search_option.Items.Insert(0, new ListItem("نباشد حاوی", "11"));
            //ddl_search_option.Items.Insert(0, new ListItem("خالی باشد", "12"));
            //ddl_search_option.Items.Insert(0, new ListItem("خالی نباشد", "13"));
            //ddl_search_option.Items.Insert(0, new ListItem("عضو این باشد", "14"));
            //ddl_search_option.Items.Insert(0, new ListItem("عضو این نباشد", "15"));
            ddl_search_option.SelectedIndex = 0;
        }
        /// <summary>
        /// متدی برای پر کردن لیست کشویی ترکیب جستجو ها
        /// </summary>
        protected void setddl_search_combine()
        {
            //ddl_search_field.DataSource = (from e in _edbcontext.tbl_fergir
            //                                orderby e.id_fergir ascending
            //                                select e).ToList();
            //ddl_search_field.DataTextField = "nam_fergir";
            //ddl_search_field.DataValueField = "id_fergir";
            //ddl_search_field.DataBind();
            ddl_search_combine.Items.Insert(0, new ListItem("انتخاب شود", "-1"));
            ddl_search_combine.Items.Insert(1, new ListItem("و", "AND"));
            ddl_search_combine.Items.Insert(2, new ListItem("یا", "OR"));
            ddl_search_combine.SelectedIndex = 0;
        }

        /// <summary>
        /// تابعی جهت انجام اعمال مورد نیاز در هنگام لود اولیه صفحه
        /// </summary>
        protected void initialstate()
        {
            setddl_fergir_letter_out();
            setddl_fergir_letter_in();
            setddl_search_field();
            setddl_search_option();
            setddl_search_combine();
            CKEditorConfig();
            Session["addday"] = 0;
            ViewState["search"] = "";
            //SetGVdata("", "", "");
        }
        /// <summary>
        /// تغییر مقدار متغیر جلسه در حالت ایجکس
        /// </summary>
        /// <param name="session_name"></param>
        /// <param name="session_value"></param>
        /// <returns></returns>
        [WebMethod]
        public static string SetSession(string session_name, string session_value)
        {
            HttpContext.Current.Session[session_name] = session_value;
            return "true";
        }
        /// <summary>
        /// تابعی جهت ورود و ویرایش اطلاعات نامه صادره به صورت ایجکس
        /// </summary>
        /// <param name="id_letter"></param>
        /// <param name="moz_letter"></param>
        /// <param name="gir_id_letter"></param>
        /// <param name="user_id_letter"></param>
        /// <returns></returns>
        [WebMethod]
        public static string sabt_out_letter_method(string id_letter, /*string sh_leter, string DateLetter,*/
            string moz_letter, string gir_id_letter,
            string user_id_letter/*, string con_letter*//*, string att_letter*/)
        {
            DataBaseContext _edbcontext = new DataBaseContext();
            if (int.Parse(id_letter) == -1)
            {
                Letter _letter = new Letter();
                _letter.Number = "صادره";
                _letter.DateLetter = "9999/99/99";
                _letter.Subject = moz_letter;
                //_letter.fer_id_letter = 0;
                //_letter.gir_id_letter = Convert.ToDecimal(gir_id_letter);
                //_letter.user_id_letter = Convert.ToDecimal(user_id_letter);
                _letter.DateRegister = MyObjects.myclass.shortshamsidate(DateTime.Now);
                _letter.Content = string.Empty;
                _letter.Attribute = "صادره";


                _edbcontext.Letters.Add(_letter);
                _edbcontext.SaveChanges();
                return "true";
            }
            else
            {
                int myid_letter = int.Parse(id_letter);
                var query = from l in _edbcontext.Letters
                            where (l.ID == myid_letter)
                            select l;
                var _letter = query.FirstOrDefault();
                if (_letter != null)
                {
                    //_letter.sh_letter = sh_leter;
                    //_letter.DateLetter = DateLetter;
                    _letter.Subject = moz_letter;
                    //_letter.fer_id_letter = 0;
                    //_letter.gir_id_letter = Convert.ToInt64(gir_id_letter);
                    //_letter.user_id_letter = Convert.ToInt64(user_id_letter);
                    //_letter.dte_sabt_letter = MyObjects.myclass.shortshamsidate(DateTime.Now);
                    //_letter.con_letter = con_letter;
                    //_letter.att_letter = att_letter;
                }
                _edbcontext.SaveChanges();
                return "true";
            }

            //return "false";
        }
        /// <summary>
        /// تابعی جهت ورود یا ویرایش اطلاعات نامه وارده به صورت ایجکس
        /// </summary>
        /// <param name="id_letter"></param>
        /// <param name="sh_leter"></param>
        /// <param name="DateLetter"></param>
        /// <param name="moz_letter"></param>
        /// <param name="fer_id_letter"></param>
        /// <param name="user_id_letter"></param>
        /// <returns></returns>
        [WebMethod]
        public static string sabt_in_letter_method(string id_letter, string sh_leter, string DateLetter,
            string moz_letter, string fer_id_letter,
            string user_id_letter/*, string con_letter*//*, string att_letter*/)
        {
            DataBaseContext _edbcontext = new DataBaseContext();
            if (int.Parse(id_letter) == -1)
            {
                Letter _letter = new Letter();
                _letter.Number = sh_leter;
                _letter.DateLetter = DateLetter;
                _letter.Subject = moz_letter;
                //_letter.fer_id_letter = Convert.ToDecimal(fer_id_letter); ;
                //_letter.gir_id_letter = 0;
                //_letter.user_id_letter = Convert.ToDecimal(user_id_letter);
                _letter.DateRegister = MyObjects.myclass.shortshamsidate(DateTime.Now);
                _letter.Content = string.Empty;
                _letter.Attribute = "وارده";


                _edbcontext.Letters.Add(_letter);
                _edbcontext.SaveChanges();
                return "true";
            }
            else
            {
                int myid_letter = int.Parse(id_letter);
                var query = from l in _edbcontext.Letters
                            where (l.ID == myid_letter)
                            select l;
                var _letter = query.FirstOrDefault();
                if (_letter != null)
                {
                    _letter.Number = sh_leter;
                    _letter.DateLetter = DateLetter;
                    _letter.Subject = moz_letter;
                    //_letter.fer_id_letter = Convert.ToDecimal(fer_id_letter);
                    //_letter.gir_id_letter = 0;
                    //_letter.user_id_letter = Convert.ToDecimal(user_id_letter);
                    //_letter.dte_sabt_letter = MyObjects.myclass.shortshamsidate(DateTime.Now);
                    //_letter.con_letter = con_letter;
                    //_letter.att_letter = att_letter;
                }
                _edbcontext.SaveChanges();
                return "true";
            }

            //return "false";
        }
        /// <summary>
        /// تابعی جهت ویرایش متن نامه به صورت ایجکس
        /// </summary>
        /// <param name="id_letter"></param>
        /// <param name="con_letter"></param>
        /// <returns></returns>
        [WebMethod]
        public static string sabt_out_content_method(string id_letter, string con_letter)
        {
            int myid_letter = int.Parse(id_letter);
            DataBaseContext _edbcontext = new DataBaseContext();
            var query = from l in _edbcontext.Letters
                        where (l.ID == myid_letter)
                        select l;
            var _letter = query.FirstOrDefault();
            if (_letter != null)
            {
                _letter.Content = con_letter;

                //_letter.gir_id_letter = 0;
                //_letter.dte_sabt_letter = MyObjects.myclass.shortshamsidate(DateTime.Now);
            }
            _edbcontext.SaveChanges();
            return "true";
            //return "false";
        }
        //[WebMethod]
        //public static string PrintMethod(string id_letter, string con_letter)
        //{
        //    int myid_letter = int.Parse(id_letter);
        //    dabirmodel.dabirDBEntities _edbcontext = new dabirmodel.dabirDBEntities();
        //    var query = from l in _edbcontext.tbl_letter
        //                where (l.id_letter == myid_letter)
        //                select l;
        //    var _letter = query.FirstOrDefault();
        //    if (_letter != null)
        //    {
        //        var htmlContent = String.Format(_letter.con_letter);
        //        htmlContent = "<meta http-equiv='content-type'content='text/html;charset=UTF-8'>" + htmlContent;
        //        var htmlToPdf = new NReco.PdfGenerator.HtmlToPdfConverter();
        //        var pdfBytes = htmlToPdf.GeneratePdf(htmlContent);

        //        HttpContext Context = HttpContext.Current;
        //        Context.Response.ContentType = "application/pdf";
        //        Context.Response.AddHeader("content-length", pdfBytes.Length.ToString());
        //        Context.Response.BinaryWrite(pdfBytes);
        //        HttpContext.Current.Session["letter"] = pdfBytes;
        //        return "true";
        //    }
        //    return "false";
        //}
        /// <summary>
        /// تابعی جهت بدست آوردن متن یک نامه به صورت ایجکس
        /// </summary>
        /// <param name="id_letter"></param>
        /// <returns></returns>
        [WebMethod]
        public static string get_content_method(string id_letter)
        {
            int myid_letter = int.Parse(id_letter);
            DataBaseContext _edbcontext = new DataBaseContext();
            var query = from l in _edbcontext.Letters
                        where (l.ID == myid_letter)
                        select l;
            var _letter = query.FirstOrDefault();
            if (_letter != null)
            {
                return _letter.Content;
            }
            return string.Empty;
        }
        /// <summary>
        /// تابعی جهت بدست آوردن اطلاعات یک نامه با شماره یکه آن به صورت ایجکس
        /// </summary>
        /// <param name="id_letter"></param>
        /// <returns></returns>
        [WebMethod]
        public static string get_letter_byid(string id_letter)
        {
            int myid_letter = int.Parse(id_letter);
            DataBaseContext _edbcontext = new DataBaseContext();
            var query = from l in _edbcontext.Letters
                        where (l.ID == myid_letter)
                        select new
                        {
                            l.ID,
                            l.Number,
                            l.DateLetter,
                            l.Subject,
                            //l.fer_id_letter,
                            //l.gir_id_letter,
                            //l.user_id_letter,
                            l.DateRegister,
                            l.Content,
                            l.Attribute,
                            //nam_fer = l.tbl_fer.nam_fergir,
                            //nam_gir = l.tbl_gir.nam_fergir,
                            fname_user = l.User.Fname,
                            name_user = l.User.Lname
                        };
            var _letter = query.ToList();
            if (_letter != null)
            {
                JavaScriptSerializer s = new JavaScriptSerializer();
                s.MaxJsonLength = int.MaxValue;
                return s.Serialize(_letter);
            }
            return null;
        }
        /// <summary>
        /// حذف نامه به صورت ایجکس
        /// </summary>
        /// <param name="id_letter"></param>
        /// <returns></returns>
        [WebMethod]
        public static string delete_letter_method(string id_letter)
        {
            DataBaseContext _edbcontext = new DataBaseContext();
            if (int.Parse(id_letter) != -1)
            {
                int myid_letter = int.Parse(id_letter);
                var query = from c in _edbcontext.Letters
                            where (c.ID == myid_letter)
                            select c;
                var _letter = query.FirstOrDefault();
                if (_letter != null)
                {
                    _edbcontext.Letters.Remove(_letter);
                    _edbcontext.SaveChanges();
                    return "true";
                }
                return "false";

            }
            return "false";
        }
        #endregion
        #region systemmethode
        /// <summary>
        /// متد لود صفحه
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if ((Session["vorod"] != null))
            {
                if ((Session["vorod"].ToString() == "ok"))
                {
                    if ((int.Parse(Session["role_id"].ToString()) == (int)MyObjects.RolName.Admin) ||
                        (int.Parse(Session["role_id"].ToString()) == (int)MyObjects.RolName.SuperAdmin)
                        )
                    {
                        if (!IsPostBack)
                        {
                            //ScriptManager.GetCurrent(this.Page).RegisterPostBackControl();
                            initialstate();
                        }
                        else
                        {
                            SetGVdata(ViewState["search"].ToString(), "", "");
                        }
                    }
                }
                else
                {
                    Response.Redirect("../login.aspx");
                }
            }
        }
        /// <summary>
        /// متد تغییرات ایندکس صفحه
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvdata_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            SetGVdata(ViewState["search"].ToString(), "", "");
            gvdata.PageIndex = e.NewPageIndex;
            gvdata.DataBind();
            System.Threading.Thread.Sleep(500);
        }

        /// <summary>
        /// متدی برای چاپ متن نامه هنگام انتخاب کلید چاپ
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvdata_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            DataBaseContext _edbcontext = new DataBaseContext();
            if (e.CommandName == "do_print_letter")
            {
                int index = Convert.ToInt32(e.CommandArgument.ToString());
                int myid_letter = index;
                var query = from l in _edbcontext.Letters
                            where (l.ID == myid_letter)
                            select l;
                var _letter = query.FirstOrDefault();
                if (_letter != null)
                {
                    var htmlContent = String.Format(_letter.Content);
                    htmlContent = "<body dir='rtl'><meta http-equiv='content-type'content='text/html;charset=UTF-8'>" + htmlContent + "</body>";
                    var htmlToPdf = new NReco.PdfGenerator.HtmlToPdfConverter();
                    var pdfBytes = htmlToPdf.GeneratePdf(htmlContent);
                    Response.ContentType = "application/pdf";
                    string filename = _letter.ID.ToString();
                    //Response.AddHeader("content-disposition", "attachment;filename=" + filename);
                    Response.AddHeader("content-length", pdfBytes.Length.ToString());
                    Response.Buffer = true;
                    Response.Clear();
                    Response.BinaryWrite(pdfBytes);
                    //System.Threading.Thread.Sleep(1000);
                    Response.End();
                }
            }
        }

        /// <summary>
        /// متدی برای تنظیم کلید چاپ برای اجرا در آپدیت پنل
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvdata_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton linkbtn = (LinkButton)e.Row.FindControl("lbt_print_letter");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                if (scriptManager != null && linkbtn != null)
                {
                    scriptManager.RegisterPostBackControl(linkbtn);
                }
            }

        }


        protected void gvdata_Sorting(object sender, GridViewSortEventArgs e)
        {
            //SetGVdata(txt_lastweek.Text, txt_nextweek.Text, e.SortExpression, e.SortDirection.ToString());
        }

        /// <summary>
        /// شروع عمل جستجو
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lbtn_search_Click(object sender, EventArgs e)
        {
            if (ViewState["search"].ToString() != string.Empty)
            {
                //string tt = ViewState["search"].ToString();
                //string sqlquery = "select * from V_tbl_letter where " + ViewState["search"].ToString();
                //_edbcontext.tbl_letter.SqlQuery(sqlquery).ToList();
                //gvdata.DataSource = _edbcontext.tbl_letter.SqlQuery(sqlquery).ToList();
                //gvdata.DataBind();
                SetGVdata(ViewState["search"].ToString(), "", "");
                System.Threading.Thread.Sleep(500);
            }
        }

        /// <summary>
        /// اضافه کردن یک جستجو به جستجو های قبلی
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lbtn_add_search_Click(object sender, EventArgs e)
        {
            if (ViewState["search"].ToString() != string.Empty)
            {
                string laststring = ViewState["search"].ToString().Substring(0, ViewState["search"].ToString().Length - 1).Split(' ').Last();
                if (laststring != "AND" && laststring != "OR")
                {
                    ViewState["search"] = "";
                    lbl_search_text.Text = "";
                }
            }

            if (ddl_search_combine.SelectedValue != "-1")
            {
                if (ddl_search_field.SelectedValue != "-1" && ddl_search_option.SelectedValue != "-1" && txt_search.Text != "")
                {
                    if (ddl_search_option.SelectedValue.ToString() == "LIKE")
                    {
                        ViewState["search"] = ViewState["search"] + ddl_search_field.SelectedValue.ToString() + " " + ddl_search_option.SelectedValue.ToString() + " " + "N'%" + txt_search.Text + "%'" + " " + ddl_search_combine.SelectedValue.ToString() + " ";
                    }
                    else
                    {
                        ViewState["search"] = ViewState["search"] + ddl_search_field.SelectedValue.ToString() + " " + ddl_search_option.SelectedValue.ToString() + " " + "N'" + txt_search.Text + "'" + " " + ddl_search_combine.SelectedValue.ToString() + " ";
                    }
                    lbl_search_text.Text = lbl_search_text.Text + ddl_search_field.SelectedItem.ToString() + " " + ddl_search_option.SelectedItem.ToString() + " " + "'" + txt_search.Text + "'" + " " + ddl_search_combine.SelectedItem.ToString() + " ";
                }
            }
            else
            {
                if (ddl_search_field.SelectedValue != "-1" && ddl_search_option.SelectedValue != "-1" && txt_search.Text != "")
                {
                    if (ddl_search_option.SelectedValue.ToString() == "LIKE")
                    {
                        ViewState["search"] = ViewState["search"] + ddl_search_field.SelectedValue.ToString() + " " + ddl_search_option.SelectedValue.ToString() + " " + "N'%" + txt_search.Text + "%'" + " ";
                    }
                    else
                    {
                        ViewState["search"] = ViewState["search"] + ddl_search_field.SelectedValue.ToString() + " " + ddl_search_option.SelectedValue.ToString() + " " + "N'" + txt_search.Text + "'" + " ";
                    }
                    lbl_search_text.Text = lbl_search_text.Text + ddl_search_field.SelectedItem.ToString() + " " + ddl_search_option.SelectedItem.ToString() + " " + "'" + txt_search.Text + "'" + " " + ddl_search_combine.SelectedItem.ToString() + " ";
                }
            }
            ddl_search_field.SelectedIndex = -1;
            ddl_search_option.SelectedIndex = -1;
            ddl_search_combine.SelectedIndex = -1;
            txt_search.Text = "";

        }

        /// <summary>
        /// پاک کردن جستجوی قبلی
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lbtn_search_clear_Click(object sender, EventArgs e)
        {
            if (ViewState["search"].ToString() != "")
            {
                ddl_search_field.SelectedIndex = -1;
                ddl_search_option.SelectedIndex = -1;
                ddl_search_combine.SelectedIndex = -1;
                txt_search.Text = "";
                ViewState["search"] = "";
                SetGVdata("", "", "");
                lbl_search_text.Text = "";
                System.Threading.Thread.Sleep(500);
            }
        }

        /// <summary>
        /// برگشتن به صفحه قبل
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lbtn_back_Click(object sender, EventArgs e)
        {
            if (Session["backpage"] != null)
            {
                ViewState["search"] = "";
                Response.Redirect(Session["backpage"].ToString());
            }
        }
    }

    #endregion
}