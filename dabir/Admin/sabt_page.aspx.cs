﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Globalization;
using EntityFramework.Extensions;
using MyObjects;
using DataLayer.DataAccessLayer;
using DomainClass.DomainModel;
using NReco.PdfGenerator;
using System.Web.Script.Serialization;
using System.IO;

namespace dabir.Admin
{
    /// <summary>
    /// کنترل صفحات در این قسمت انجام میگیرد
    /// </summary>
    public partial class sabt_page : System.Web.UI.Page
    {
        #region userfunction
        /// <summary>
        /// تابعی برای پر کردن اطلاعات گرید ویو
        /// که بر اساس عنوان صفحه مرتب می گردد
        /// </summary>
        protected void CKEditorConfig()
        {
            txt_header.config.font_defaultLabel = "Tahoma";
            txt_header.config.fontSize_defaultLabel = "16";
            txt_header.config.language = "fa";
            txt_header.config.bodyClass = "{font-family:Tahoma}";
            txt_header.ResizeEnabled = false;
            txt_header.config.toolbar = new object[]
            {
                new object[]{ "Source", "NewPage", "Preview", "-", "Templates"},
                new object[]{ "Cut","Copy","Paste","PasteText","PasteWord","-","Undo","Redo"},
                new object[]{ "SelectAll","RemoveFormat"},
                new object[]{ "Bold","Italic","Underline"},
                new object[]{ "NumberedList", "BulletedList", "-", "Outdent", "Indent","-","Blockquote"},
                new object[]{ "JustifyLeft","JustifyCenter","JustifyRight", "JustifyBlock", "JustifyFull","-","BidiLtr","BidiRtl"},
                new object[]{ "Styles","Format","Font","FontSize"},
                new object[]{ "TextColor","BGColor"},
                new object[]{ "Table"}
            };
            txt_message.config.font_defaultLabel = "Tahoma";
            txt_message.config.fontSize_defaultLabel = "16";
            txt_message.config.language = "fa";
            txt_message.config.bodyClass = "{font-family:Tahoma}";
            txt_message.ResizeEnabled = false;
            txt_message.config.toolbar = new object[]
            {
                new object[]{ "Source", "NewPage", "Preview", "-", "Templates"},
                new object[]{ "Cut","Copy","Paste","PasteText","PasteWord","-","Undo","Redo"},
                new object[]{ "SelectAll","RemoveFormat"},
                new object[]{ "Bold","Italic","Underline"},
                new object[]{ "NumberedList", "BulletedList", "-", "Outdent", "Indent","-","Blockquote"},
                new object[]{ "JustifyLeft","JustifyCenter","JustifyRight", "JustifyBlock", "JustifyFull","-","BidiLtr","BidiRtl"},
                new object[]{ "Styles","Format","Font","FontSize"},
                new object[]{ "TextColor","BGColor"},
                new object[]{ "Table"}
            };
            txt_footer.config.font_defaultLabel = "Tahoma";
            txt_footer.config.fontSize_defaultLabel = "16";
            txt_footer.config.language = "fa";
            txt_footer.config.bodyClass = "{font-family:Tahoma}";
            txt_footer.ResizeEnabled = false;
            txt_footer.config.toolbar = new object[]
            {
                new object[]{ "Source", "NewPage", "Preview", "-", "Templates"},
                new object[]{ "Cut","Copy","Paste","PasteText","PasteWord","-","Undo","Redo"},
                new object[]{ "SelectAll","RemoveFormat"},
                new object[]{ "Bold","Italic","Underline"},
                new object[]{ "NumberedList", "BulletedList", "-", "Outdent", "Indent","-","Blockquote"},
                new object[]{ "JustifyLeft","JustifyCenter","JustifyRight", "JustifyBlock", "JustifyFull","-","BidiLtr","BidiRtl"},
                new object[]{ "Styles","Format","Font","FontSize"},
                new object[]{ "TextColor","BGColor"},
                new object[]{ "Table"}
            };
            txt_body.config.font_defaultLabel = "Tahoma";
            txt_body.config.fontSize_defaultLabel = "16";
            txt_body.config.language = "fa";
            txt_body.config.bodyClass = "{font-family:Tahoma}";
            txt_body.ResizeEnabled = false;
            txt_body.config.toolbar = new object[]
            {
                new object[]{ "Source", "NewPage", "Preview", "-", "Templates"},
                new object[]{ "Cut","Copy","Paste","PasteText","PasteWord","-","Undo","Redo"},
                new object[]{ "SelectAll","RemoveFormat"},
                new object[]{ "Bold","Italic","Underline"},
                new object[]{ "NumberedList", "BulletedList", "-", "Outdent", "Indent","-","Blockquote"},
                new object[]{ "JustifyLeft","JustifyCenter","JustifyRight", "JustifyBlock", "JustifyFull","-","BidiLtr","BidiRtl"},
                new object[]{ "Styles","Format","Font","FontSize"},
                new object[]{ "TextColor","BGColor"},
                new object[]{ "Table"}
            };
        }
        protected void SetGVdata()
        {
            using (var _edbcontext = new DataBaseContext())
            {
                gvdata.DataSource =
                    (from t in _edbcontext.Pages
                     orderby t.ID ascending
                     select t).ToList();
                gvdata.DataBind();
            }
        }
        protected void setddl_orientation()
        {
            ddl_orientation.Items.Insert(0, new ListItem("پیش فرض", "0"));
            ddl_orientation.Items.Insert(1, new ListItem("افقی", "1"));
            ddl_orientation.Items.Insert(2, new ListItem("عمودی", "2"));
            ddl_orientation.SelectedIndex = 0;
        }
        protected void initialstate()
        {
            gvdata.PageSize = int.Parse(System.Configuration.ConfigurationManager.AppSettings["PageSize"]);
            CKEditorConfig();
            setddl_orientation();
            SetGVdata();
        }
        [WebMethod]
        public static string SetSession(string session_name, string session_value)
        {
            HttpContext.Current.Session[session_name] = session_value;
            return "true";
        }
        /// <summary>
        /// تابعی جهت وارد کردن داده ها در بانک اطلاعاتی به صورت ایجکس
        /// </summary>
        /// <param name="Body">بدنه متن ورودی</param>
        /// <param name="ID">todo: describe ID parameter on sabt_page_method</param>
        /// <param name="Name">todo: describe Name parameter on sabt_page_method</param>
        /// <param name="Width">todo: describe Width parameter on sabt_page_method</param>
        /// <param name="Height">todo: describe Height parameter on sabt_page_method</param>
        /// <param name="Orientation">todo: describe Orientation parameter on sabt_page_method</param>
        /// <param name="Margin_Top">todo: describe Margin_Top parameter on sabt_page_method</param>
        /// <param name="Margin_Bottom">todo: describe Margin_Bottom parameter on sabt_page_method</param>
        /// <param name="Margin_Right">todo: describe Margin_Right parameter on sabt_page_method</param>
        /// <param name="Margin_Left">todo: describe Margin_Left parameter on sabt_page_method</param>
        /// <param name="Header">todo: describe Header parameter on sabt_page_method</param>
        /// <param name="Message">todo: describe Message parameter on sabt_page_method</param>
        /// <param name="Footer">todo: describe Footer parameter on sabt_page_method</param>
        /// <returns></returns>
        [WebMethod]
        public static string sabt_page_method(string ID, string Name, string Width, string Height, string Orientation,string Margin_Top,
            string Margin_Bottom, string Margin_Right, string Margin_Left, string Header, string Message, string Body, string Footer,string BGPath)
        {
            using (var _edbcontext = new DataBaseContext())
            {
                if (Int64.Parse(ID) == -1)
                {
                    try
                    {
                        var _page = new DomainClass.DomainModel.Page
                        {
                            Name = Name,
                            Width = int.Parse(Width),
                            Height = int.Parse(Height),
                            Orientation = int.Parse(Orientation),
                            Margin_Top = int.Parse(Margin_Top),
                            Margin_Bottom = int.Parse(Margin_Bottom),
                            Margin_Right = int.Parse(Margin_Right),
                            Margin_Left = int.Parse(Margin_Left),
                            Header = Header,
                            Message = Message,
                            Body = Body,
                            Footer = Footer,
                            BGPath = BGPath
                            
                        };


                        _edbcontext.Pages.Add(_page);
                        _edbcontext.SaveChanges();
                        return _page.ID.ToString();
                    }
                    catch (Exception ex)
                    {
                        Log.LogErrorMessage(ex.Message);
                        return "-1";
                    }
                }
                else
                {
                    try
                    {
                        var myid_page = long.Parse(ID);
                        var query = from l in _edbcontext.Pages
                                    where (l.ID == myid_page)
                                    select l;
                        var _page = query.FirstOrDefault();
                        if (_page != null)
                        {
                            _page.Name = Name;
                            _page.Width = int.Parse(Width);
                            _page.Height = int.Parse(Height);
                            _page.Margin_Top = int.Parse(Margin_Top);
                            _page.Orientation = int.Parse(Orientation);
                            _page.Margin_Bottom = int.Parse(Margin_Bottom);
                            _page.Margin_Right = int.Parse(Margin_Right);
                            _page.Margin_Left = int.Parse(Margin_Left);
                            _page.Header = Header;
                            _page.Message = Message;
                            _page.Body = Body;
                            _page.Footer = Footer;
                            _page.BGPath = BGPath;
                        }
                        _edbcontext.SaveChanges();
                        return _page.ID.ToString();
                    }
                    catch (Exception ex)
                    {
                        Log.LogErrorMessage(ex.Message);
                        return "-1";
                    }
                    //return "true";
                }
            }

            //return "false";
        }
        [WebMethod]
        public static string get_page_byid(string ID)
        {
            var myid_page = long.Parse(ID);
            using (var _edbcontext = new DataBaseContext())
            {
                var query = from l in _edbcontext.Pages
                            where (l.ID == myid_page)
                            select new
                            {
                                l.ID,
                                l.Name,
                                l.Width,
                                l.Height,
                                l.Orientation,
                                l.Margin_Top,
                                l.Margin_Bottom,
                                l.Margin_Right,
                                l.Margin_Left,
                                l.Header,
                                l.Message,
                                l.Body,
                                l.Footer,
                                l.BGPath
                            };
                var _page = query.ToList();
                if (_page != null)
                {
                    var s = new JavaScriptSerializer
                    {
                        MaxJsonLength = int.MaxValue
                    };
                    return s.Serialize(_page);
                }
                return null;
            }
        }
        [WebMethod]
        public static string delete_page_method(string ID)
        {
            using (var _edbcontext = new DataBaseContext())
            {
                if (long.Parse(ID) != -1)
                {
                    var myid_page = long.Parse(ID);
                    var query = from c in _edbcontext.Pages
                                where (c.ID == myid_page)
                                select c;
                    var _page = query.FirstOrDefault();
                    if (_page != null)
                    {
                        _edbcontext.Pages.Remove(_page);
                        _edbcontext.SaveChanges();
                        return "true";
                    }
                    return "false";

                }
                return "false";
            }
        }
        #endregion
        #region systemmethode
        protected void Page_Load(object sender, EventArgs e)
        {
            if ((Session["vorod"] != null))
            {
                if ((Session["vorod"].ToString() == "ok"))
                {
                    if ((int.Parse(Session["role_id"].ToString()) == (int)MyObjects.RolName.Admin) ||
                        (int.Parse(Session["role_id"].ToString()) == (int)MyObjects.RolName.SuperAdmin)
                        )
                    {
                        if (!IsPostBack)
                        {
                            //ScriptManager.GetCurrent(this.Page).RegisterPostBackControl();
                            initialstate();
                        }
                        else
                        {
                            SetGVdata();
                        }
                    }
                }
                else
                {
                    Response.Redirect("../login.aspx");
                }
            }
        }

        protected void gvdata_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            SetGVdata();
            gvdata.PageIndex = e.NewPageIndex;
            gvdata.DataBind();
            System.Threading.Thread.Sleep(500);
        }

        #endregion
    }
}