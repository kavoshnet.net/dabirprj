﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MyObjects;

namespace dabir.Admin
{
    public partial class MasterPageAdmin : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["vorod"] != null)
            {
                if (Session["vorod"].ToString() == "ok")
                {
                    sabt_letter.Visible = Convert.ToBoolean(Session["Sabt_Name"].ToString());
                    sabt_fergir.Visible = Convert.ToBoolean(Session["Sabt_FerGir"].ToString());
                    sabt_user.Visible = Convert.ToBoolean(Session["Sabt_User"].ToString());
                    sabt_page.Visible = Convert.ToBoolean(Session["Sabt_Page"].ToString());
                    action .Visible = Convert.ToBoolean(Session["Action_Menu"].ToString());
                    //Label1.Text = string.Format("کاربر محترم {0} به سامانه ثبت کارتهای امحایی خوش آمدید",
                    //    Session["lfname_user"].ToString());
                    //Label2.Text = myclass.longshamsidate(DateTime.Now);//string.Format("امروز:{0}", BijanComponents.ShamsiDate.GetShamsiDate(DateTime.Now));
                }
            }
            else
              Response.Redirect("../Logout.aspx");
        }
    }
}