﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pdf.aspx.cs" Inherits="dabir.Admin.pdf" %>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- JQUERY class -->
    <script src="../style/jQuery.2.1.4/Content/Scripts/jquery-2.1.4.min.js" type="text/javascript"></script>
    <!-- JQUERY UI class -->
    <link href="../style/jquery-ui-1.11.4/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="../style/jquery-ui-themes-1.11.4/themes/redmond/jquery-ui.css" rel="stylesheet" type="text/css" />
    <!-- BOOTSTARP class -->
    <link href="../style/bootstrap3.3.2/Content/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="../style/bootstrap3.3.2/Content/bootstrap.rtl.css" rel="stylesheet" type="text/css" />
    <script src="../style/bootstrap3.3.2/Scripts/bootstrap.js" type="text/javascript"></script>
    <link href="../style/bootstrap3.3.2/Content/bootstrapValidator.css" rel="stylesheet" type="text/css" />
    <script src="../style/bootstrap3.3.2/Scripts/bootstrap.rtl.js" type="text/javascript"></script>
    <script src="../style/bootstrap3.3.2/Scripts/bootstrapValidator.js" type="text/jscript"></script>

    <!-- Custom class -->
    <script src="../style/MyJS/jquery-ui.js" type="text/javascript"></script>
    <link href="../style/MyStyle/custom.css" rel="stylesheet" type="text/css" />
    <!-- MENU class -->
    <link href="../style/MyStyle/menu.css" rel="stylesheet" type="text/css" />
    <!-- JQGride class -->
<%--    <link href="../style/Guriddo_jqGrid_JS_5.0.1/css/ui.jqgrid-bootstrap-ui.css" rel="stylesheet" type="text/css"/>
    <link href="../style/Guriddo_jqGrid_JS_5.0.1/css/ui.jqgrid-bootstrap.css" rel="stylesheet" type="text/css"/>
    <link href="../style/Guriddo_jqGrid_JS_5.0.1/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
    <script src="../style/Guriddo_jqGrid_JS_5.0.1/js/i18n/grid.locale-fa.js" type="text/javascript"></script>
    <script src="../style/Guriddo_jqGrid_JS_5.0.1/js/jquery.jqGrid.min.js" type="text/javascript"></script>--%>

    <script src="../ckeditor/ckeditor.js" type="text/javascript"></script>
    <script src="../ckeditor/adapters/jquery.js" type="text/javascript"></script>
    <!--[if lt IE 9]>
      <script src="../style/bootstrap3.3.2/Scripts/html5shiv.js"></script>
      <script src="../style/bootstrap3.3.2/Scripts/respond.min.js"></script>
   <![endif]-->
    <title>صفحه اصلی</title>
    <script type="text/javascript">
        //validator
        $(document).ready(function () {
            $("#form1").bootstrapValidator({
                message: "این مقدار صحیح نمیباشد",
                fields: {
                    "<%= mytext.UniqueID %>": {
                        message: "این مقدار صحیح نیست",
                        validators: {
                            notEmpty: {
                                message: "این فیلد نمیتواند خالی باشد"
                            },
                            stringLength: {
                                min: 4,
                                max: 30,
                                message: "تعداد ورودی باید بیش از 4 و کمتر از 30 باشد"
                            },
                            regexp: {
                                regexp: /^[a-zA-Z0-9_\.]+$/,
                                message: "تنها مقادیر حرفی ، عددی ، نقطه و خط زیر صحیح میباشد"
                            }
                        }
                    }

                }
            });
        });
        //validator
    </script>

</head><body>
    <form id="form1" runat="server">
    <div>
                    <asp:Label ID="Label11" runat="server" Text="متن نامه"></asp:Label>
                    <%--<CKEditor:CKEditorControl ID="txt_con_letter_ed" runat="server" Height="445" ClientIDMode="Static" Skin="moono"></CKEditor:CKEditorControl>--%>
        <asp:TextBox ID="mytext" runat="server"></asp:TextBox>
        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
        <asp:Button ID="mybtn" runat="server" Text="تست" OnClick="mybtn_Click"/>
    
                    <br />
                    <asp:Button ID="Button1" runat="server" Text="Button" />
    
    </div>
    </form>
</body>
</html>
