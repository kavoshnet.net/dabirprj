﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace dabir.Admin
{
    public partial class pdf : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if ((Session["vorod"] != null))
            {
                if ((Session["vorod"].ToString() == "ok"))
                {
                    if ((int.Parse(Session["role_id"].ToString()) == (int)MyObjects.RolName.Admin) ||
                        (int.Parse(Session["role_id"].ToString()) == (int)MyObjects.RolName.SuperAdmin)
                        )
                    {
                        if (!IsPostBack)
                        {

                            if (Session["letter"] != null)
                            {
                                var pdfBytes = (byte[])(Session["letter"]);
                                Response.ContentType = "application/pdf";
                                Response.AddHeader("content-length", pdfBytes.Length.ToString());
                                Response.BinaryWrite(pdfBytes);
                            }
                        }
                    }
                }
                else
                {
                    Response.Redirect("../login.aspx");
                }
            }
        }
        protected void mybtn_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Admin/pdf.aspx");
        }
    }
}