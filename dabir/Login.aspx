﻿<%@ Page Language="C#" MasterPageFile="~/LoginMaster.master" AutoEventWireup="true"
    CodeBehind="Login.aspx.cs" Inherits="dabir.Login" Title="صفحه ورود به سامانه" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!--------------------------------------------------------------------------------------------------------->
    <!--------------------------------------------------------------------------------------------------------->
    <!--------------------------------------------------------------------------------------------------------->
    <script type="text/javascript">
        //validator
        $(document).ready(function () {
            $('#' + document.forms.item(0).id).bootstrapValidator({
                message: "این مقدار صحیح نمیباشد",
                fields: {
                    "<%= txtuname_per.UniqueID %>": {
                        message: "این مقدار صحیح نیست",
                        validators: {
                            notEmpty: {
                                message: "این فیلد نمیتواند خالی باشد"
                            },
                            stringLength: {
                                min: 4,
                                max: 30,
                                message: "تعداد ورودی باید بیش از 4 و کمتر از 30 باشد"
                            },
                            regexp: {
                                regexp: /^[a-zA-Z0-9_\.]+$/,
                                message: "تنها مقادیر حرفی ، عددی ، نقطه و خط زیر صحیح میباشد"
                            }
                        }
                    },
                    "<%= txtpas_per.UniqueID %>": {
                        message: "این مقدار صحیح نیست",
                        validators: {
                            notEmpty: {
                                message: "این فیلد نمیتواند خالی باشد"
                            },
                            stringLength: {
                                min: 4,
                                max: 30,
                                message: "تعداد ورودی باید بیش از 4 و کمتر از 30 باشد"
                            },
                            regexp: {
                                regexp: /^[a-zA-Z0-9_\.]+$/,
                                message: "تنها مقادیر حرفی ، عددی ، نقطه و خط زیر صحیح میباشد"
                            }
                        }

                    }
                }
            });
        });
        //validator
    </script>
    <!---------------------------------------validator--------------------------------------------------------->
    <!---------------------------------------Display Errore Message-------------------------------------------->
    <script type="text/javascript">
        $(document).ready(function () {
            //errore message
            if ($("#<%=messagehf.ClientID %>").val() == "1") {
                $("#messagetitle").text($("#<%=messagetitlehf.ClientID %>").val());
                $("#messagetext").text($("#<%=messagetexthf.ClientID %>").val());
                $("#message").fadeIn().delay(3000).fadeOut();
                $("#<%=messagehf.ClientID %>").val("");
            }
        });
    </script>
    <!---------------------------------------Display Errore Message-------------------------------------------->
    <script type="text/javascript">
        $(document).ready(function () {
            $('#btnlogin').keypress(function (e) {
                if (e.keyCode == 13)
                    document.getElementById("<%= btnlogin.ClientID %>").click();
                //$('#btnlogin').click();
            });
            $('#txtuname_per').keypress(function (e) {
                if (e.keyCode == 13) {
                    document.getElementById("<%= btnlogin.ClientID %>").click();
                    //$('#btnlogin').click();
                }
            });
            $('#txtpas_per').keypress(function (e) {
                if (e.keyCode == 13) {
                    document.getElementById("<%= btnlogin.ClientID %>").click();
                    //$('#btnlogin').click();
                }
            });
        });
    </script>
    <div class="container">
        <!--------------------------------------------------Message Box--------------------------------------------->
        <div class="row">
            <asp:HiddenField ID="messagehf" runat="server" />
            <asp:HiddenField ID="messagetitlehf" runat="server" />
            <asp:HiddenField ID="messagetexthf" runat="server" />
            <div class="col-sm-6 col-sm-offset-3" id="message" style="display: none">
                <div class="panel panel-danger">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <div id="messagetitle" class="text-center">عنوان خطا</div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div id="messagetext" class="text-center">پیام خطا</div>
                    </div>
                </div>
            </div>
        </div>
        <!--------------------------------------------------------------------------------------------------------->
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="panel-title">
                            ورود به سامانه
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="col-sm-12">
                            <img src="Images/signin.png" alt="" />
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <asp:Label ID="Label2" runat="server" Text="نام کاربری:"></asp:Label>
                                    <asp:TextBox ID="txtuname_per" runat="server" Width="175px" ClientIDMode="Static" CssClass="form-control" placeholder="نام کاربری را وارد کنید" lang="fa"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <asp:Label ID="Label3" runat="server" Text="کلمه عبور:"></asp:Label>
                                    <asp:TextBox ID="txtpas_per" runat="server" Width="175px" ClientIDMode="Static" CssClass="form-control" placeholder="کلمه عبور را وارد کنید" TextMode="Password"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <br />
                                <asp:Button ID="btnlogin" runat="server" CssClass="btn btn-primary" OnClick="btnlogin_Click" Text="ورود" Width="175px" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
