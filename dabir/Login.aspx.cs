﻿using System;
using System.Linq;
using MyObjects;
using DataLayer;
using DataLayer.DataAccessLayer;
using DomainClass;
namespace dabir
{
    /// <summary>
    /// بررسی ورود کاربر
    /// </summary>
    public partial class Login : System.Web.UI.Page
    {
        /// <summary>
        /// نمایش خطا با استفاده از jquery
        /// </summary>
        /// <param name="showmessage">مقدار True یا False گرفته و مشخص میکند که خطا نمایش داده شود یا نه</param>
        /// <param name="messagetitle">عنوان خطا</param>
        /// <param name="messagetext">متن خطا</param>
        protected void showerrore(string showmessage, string messagetitle, string messagetext)
        {
            messagehf.Value = showmessage;
            messagetitlehf.Value = messagetitle;
            messagetexthf.Value = messagetext;
        }
        public void SetRol(int RoleID)
        {
            using (DataBaseContext _edbcontext = new DataBaseContext())
            {
                var UserRole = (from p in _edbcontext.Roles
                                where p.ID == RoleID
                                select p).FirstOrDefault();
                Session["Sabt_Sadere"] =            Convert.ToBoolean(UserRole.Sabt_Sadere);
                Session["Sabt_Varede"] =            Convert.ToBoolean(UserRole.Sabt_Varede);
                Session["Virayeshe_Moshakhasat"] =  Convert.ToBoolean(UserRole.Virayeshe_Moshakhasat);
                Session["Hazf"] =                   Convert.ToBoolean(UserRole.Hazf);
                Session["Virayeshe_Matn"] =         Convert.ToBoolean(UserRole.Virayeshe_Matn);
                Session["Erja"] =                   Convert.ToBoolean(UserRole.Erja);
                Session["Chap"] =                   Convert.ToBoolean(UserRole.Chap);
                Session["Preview"] =                Convert.ToBoolean(UserRole.Preview);
                Session["Sabt_Name"] =              Convert.ToBoolean(UserRole.Sabt_Name);
                Session["Sabt_FerGir"] =            Convert.ToBoolean(UserRole.Sabt_FerGir);
                Session["Sabt_User"] =              Convert.ToBoolean(UserRole.Sabt_User);
                Session["Sabt_Page"] =              Convert.ToBoolean(UserRole.Sabt_Page);
                Session["Action_Menu"] =            Convert.ToBoolean(UserRole.Action_Menu);
                Session["Action_Letter"] =          Convert.ToBoolean(UserRole.Action_Letter);
                Session["Edit_Referenced_Letter"] = Convert.ToBoolean(UserRole.Edit_Referenced_Letter);
            }
            //    switch (RoleID)
            //{
            //    case (int)MyObjects.RolName.SuperAdmin:
            //        {
            //            Session["Sabt_Sadere"] = true;
            //            Session["Sabt_Varede"] = true;
            //            Session["Virayeshe_Moshakhasat"] = true;
            //            Session["Hazf"] = true;
            //            Session["Virayeshe_Matn"] = true;
            //            Session["Erja"] = true;
            //            Session["Chap"] = true;
            //            Session["Sabt_Name"] = true;
            //            Session["Sabt_FerGir"] = true;
            //            Session["Sabt_User"] = true;
            //            Session["Sabt_Page"] = true;
            //            Session["Action_Menu"] = true;
            //            Session["Action_Letter"] = true;
            //            Session["Edit_Referenced_Letter"] = true;

            //        }
            //        break;
            //    case (int)MyObjects.RolName.Admin:
            //        {
            //            Session["Sabt_Sadere"] = true;
            //            Session["Sabt_Varede"] = true;
            //            Session["Virayeshe_Moshakhasat"] = true;
            //            Session["Hazf"] = true;
            //            Session["Virayeshe_Matn"] = true;
            //            Session["Erja"] = true;
            //            Session["Chap"] = true;
            //            Session["Sabt_Name"] = true;
            //            Session["Sabt_FerGir"] = true;
            //            Session["Sabt_User"] = false;
            //            Session["Sabt_Page"] = false;
            //            Session["Action_Menu"] = true;
            //            Session["Action_Letter"] = false;
            //            Session["Edit_Referenced_Letter"] = false;

            //        }
            //        break;
            //    case (int)MyObjects.RolName.SuperUser:
            //        {
            //            Session["Sabt_Sadere"] = true;
            //            Session["Sabt_Varede"] = true;
            //            Session["Virayeshe_Moshakhasat"] = true;
            //            Session["Hazf"] = true;
            //            Session["Virayeshe_Matn"] = true;
            //            Session["Erja"] = true;
            //            Session["Chap"] = true;
            //            Session["Sabt_Name"] = false;
            //            Session["Sabt_FerGir"] = false;
            //            Session["Sabt_User"] = false;
            //            Session["Sabt_Page"] = false;
            //            Session["Action_Menu"] = false;
            //            Session["Action_Letter"] = false;
            //            Session["Edit_Referenced_Letter"] = false;

            //        }
            //        break;
            //    case (int)MyObjects.RolName.User:
            //        {
            //            Session["Sabt_Sadere"] = false;
            //            Session["Sabt_Varede"] = false;
            //            Session["Virayeshe_Moshakhasat"] = false;
            //            Session["Hazf"] = false;
            //            Session["Virayeshe_Matn"] = false;
            //            Session["Erja"] = true;
            //            Session["Chap"] = true;
            //            Session["Sabt_Name"] = false;
            //            Session["Sabt_FerGir"] = false;
            //            Session["Sabt_User"] = false;
            //            Session["Sabt_Page"] = false;
            //            Session["Action_Menu"] = false;
            //            Session["Action_Letter"] = false;
            //            Session["Edit_Referenced_Letter"] = false;

            //        }
            //        break;
            //}


        }

        /// <summary>
        /// بررسی صحت ورود کاربر
        /// </summary>
        /// <param name="username">نام کاربری</param>
        /// <param name="password">کلمه عبور</param>
        /// <returns></returns>
        public bool CheckUser(string username, string password)
        {
            using (DataBaseContext _edbcontext = new DataBaseContext())
            {
                try
                {
                    if (username == string.Empty || password == string.Empty)
                    {
                        showerrore("1", "خطای ورود کاربر", "اطلاعات را تکمیل کنید");
                        Log.LogErrorMessage("اطلاعات را تکمیل کنید - خطای ورود کاربر");
                        return false;
                    }


                    var userdata = (from p in _edbcontext.Users
                                    where p.UserName == username && p.Password == password
                                    select new { p.ID, lfnameuser = p.Fname + " " + p.Lname, p.Shmeli, p.RoleID,p.Role.RoleNameFa }).FirstOrDefault();
                    if (userdata == null)
                    {
                        showerrore("1", "خطای ورود کاربر", "مجوز ورود شما تائید نشد");
                        Log.LogErrorMessage("مجوز ورود شما تائید نشد - خطای ورود کاربر");
                        return false;
                    }
                    else
                    {
                        Session["vorod"] = "ok";
                        Session["id_user"] = userdata.ID.ToString();
                        Session["lfname_user"] = userdata.lfnameuser;
                        Session["role_id"] = userdata.RoleID.ToString();
                        Session["role_namefa"] = userdata.RoleNameFa;
                        SetRol(int.Parse(userdata.RoleID.ToString()));
                        return true;
                    }
                }
                catch (Exception ex)
                {
                    showerrore("1", "خطای ورود کاربر", "در ورود کاربر خطا رخ داده است");
                    Log.LogErrorMessage(ex);
                    return false;
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Session.Clear();
            txtuname_per.Focus();
            //Response.Redirect("Login.aspx");
            //txtuname_per.Text = "U1111";
            //txtpas_per.Text = "U1111";
            //btnlogin_Click(sender, e);
        }

        /// <summary>
        ///با انتخاب این کلید وارد برنامه میشویم
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnlogin_Click(object sender, EventArgs e)
        {
            if (CheckUser(txtuname_per.Text, txtpas_per.Text))
            {
                switch (int.Parse(Session["role_id"].ToString()))
                {
                    case (int)MyObjects.RolName.User:
                    case (int)MyObjects.RolName.SuperUser:
                        {
                            Response.Redirect("~/Admin/sabt_letter.aspx");
                            //Context.ApplicationInstance.CompleteRequest();
                        }
                        break;
                    case (int)MyObjects.RolName.Admin:
                    case (int)MyObjects.RolName.SuperAdmin:
                        {
                            Response.Redirect("~/Admin/sabt_letter.aspx");
                            //Context.ApplicationInstance.CompleteRequest();
                        }
                        break;
                }
            }
            else
            {
                txtuname_per.Text = string.Empty;
                txtpas_per.Text = string.Empty;
                showerrore("1", "خطای ورود کاربر", "مجوز ورود شما تائید نشد");
                Log.LogErrorMessage("مجوز ورود شما تائید نشد - خطای ورود کاربر");
                //Response.Redirect("~/login.aspx", false);
                //Context.ApplicationInstance.CompleteRequest();
            }
        }
    }
}
