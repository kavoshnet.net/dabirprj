﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Users/MasterPageUsers.Master" AutoEventWireup="true" CodeBehind="sabt_letter.aspx.cs" Inherits="dabir.Users.sabt_letter" %>

<%@ Register Assembly="DropDownChosen" Namespace="CustomDropDown" TagPrefix="ucc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!---------------------------------------Display Errore Message-------------------------------------------->
    <script type="text/javascript">
        function checkShamsi(str) {
            //بدون در نظر گرفتن دورقمی بودن ماه و روز
            //var patt = /(13|14)([0-9][0-9])\/(((0?[1-6])\/((0?[1-9])|([12][0-9])|(3[0-1])))|(((0?[7-9])|(1[0-2]))\/((0?[1-9])|([12][0-9])|(30))))/g;
            var patt = /(13|14)([0-9][0-9])\/(((0[1-6])\/((0[1-9])|([12][0-9])|(3[0-1])))|(((0[7-9])|(1[0-2]))\/((0[1-9])|([12][0-9])|(30))))/g;
            var result = patt.test(str);
            if (result) {
                var pos = str.indexOf('/');
                var year = str.substring(0, pos);
                var nextPos = str.indexOf('/', pos + 1);
                var month = str.substring(pos + 1, nextPos);
                var day = str.substring(nextPos + 1);
                if (month == 12 && (year + 1) % 4 != 0 && day == 30) { // kabise = 1379, 1383, 1387,... (year +1) divides on 4 remains 0
                    result = false;
                }
                return result;
            }
        }
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(document).ready(function () {
                var myvalue1 = $("#<%=txt_lastweek.ClientID %>").val();
                var myvalue2 = $("#<%=txt_nextweek.ClientID %>").val();
                if (!checkShamsi(myvalue1) || !checkShamsi(myvalue2)) {
                    $("#messagetext").text("تاریخ وارد شده صحیح نیست");
                    $("#message").fadeIn().delay(3000).fadeOut();
                    //alert("تاریخ وارد شده معتبر نیست!");
                    return false;
                }
            });
        });
    </script>
    <%--    <script type="text/javascript">
        function myfunc() {
            $(document).ready(function () {
                if ($("#<%=hfield_search.ClientID %>").val() == "search_panel") {
                    $("#search_panel").show();
                    $("#sabt_panel").hide(500);
                }
                else {
                    $("#sabt_panel").show();
                    $("#search_panel").hide(500);
                }
                $(document).on("click", "[id*=lbtn_search_call]", function (e) {
                    //alert("search_panel");
                    $("#<%=hfield_search.ClientID %>").val("search_panel");
                    //$("#search_panel").show();
                    //$("#sabt_panel").hide(500);
                });

                $(document).on("click", "[id*=lbtn_back]", function (e) {
                    //alert("sabt_panel");
                    $("#<%=hfield_search.ClientID %>").val("sabt_panel");
                    //$("#sabt_panel").show();
                    //$("#search_panel").hide(500);
                });
            });
        }
    </script>--%>
    <%-- Start Validation                      --------------%>
    <%--    <script type="text/javascript">
        $(document).ready(function () {
            $('#' + document.forms.item(0).id).bootstrapValidator({
                //message: "این مقدار صحیح نمیباشد",
                //container: 'این مقدار صحیح نمیباشد',
                //feedbackIcons: {
                //    valid: 'glyphicon glyphicon-ok',
                //    invalid: 'glyphicon glyphicon-remove',
                //    validating: 'glyphicon glyphicon-refresh'
                //},
                fields: {
                    "<%= txt_lastweek.UniqueID %>": {
                        message: "این مقدار صحیح نیست",
                        validators: {
                            notEmpty: {
                                message: "این فیلد نمیتواند خالی باشد"
                            },
                            regexp: {
                                regexp: /^1[34][0-9][0-9]\/((0[1-6]\/((3[0-1])|([1-2][0-9])|(0[1-9])))|((1[0-2]|(0[7-9]))\/(30|([1-2][0-9])|(0[1-9]))))+$/,
                                message: "تاریخ صحیح نمیباشد (--/--/--13)"
                            }
                        }

                    },
                    "<%= txt_nextweek.UniqueID %>": {
                        message: "این مقدار صحیح نیست",
                        validators: {
                            notEmpty: {
                                message: "این فیلد نمیتواند خالی باشد"
                            },
                            regexp: {
                                regexp: /^1[34][0-9][0-9]\/((0[1-6]\/((3[0-1])|([1-2][0-9])|(0[1-9])))|((1[0-2]|(0[7-9]))\/(30|([1-2][0-9])|(0[1-9]))))+$/,
                                message: "تاریخ صحیح نمیباشد (--/--/--13)"
                            }
                        }

                    }
                }
            });
        });
    </script>--%>
    <%-- Start Dialog Function                 --------------%>
    <%-- Dialog Sabt Out    --%>
    <script type="text/javascript">
        // cal ajax methode for save data
        $(document).ready(function () {
            $("#sabt_out_letter_dialog").dialog(
          {
              autoOpen: false,
              resizable: false,
              modal: true,
              bgiframe: true,
              width: 700,
              height: 300,
              title: "ثبت نامه صادره",
              open: function (event, ui) { $(".ui-dialog-titlebar-close").hide(); },
              show: {
                  effect: "fade",
                  duration: 50
              },
              hide: {
                  effect: "fade",
                  //effect: "fade",
                  duration: 50
              },
              close: function (event, ui) {
<%--                  var UpdatePanel1 = '<%=UpdatePanel1.ClientID%>';
                  if (UpdatePanel1 != null)
                      __doPostBack(UpdatePanel1, '');--%>
                  //$("#form1").data("bootstrapValidator").resetForm(true);
              },
              buttons: {
                  'ثبت و ادامه': function () {
                      //var ckeditor = $('#txt_con_letter').val();
                      var myvalue1 = $("#<%=txt_dte_sabt_letter_out.ClientID %>").val();
                      if (!checkShamsi(myvalue1)) {
                          $("#messagetext").text("تاریخ وارد شده صحیح نیست");
                          $("#message").fadeIn().delay(3000).fadeOut();
                          //alert("تاریخ وارد شده معتبر نیست!");
                          return false;
                      }

                      $.ajax({
                          type: 'POST',
                          contentType: "application/json; charset=utf-8",
                          url: 'sabt_letter.aspx/sabt_out_letter_method',
                          data: "{" +
                              "'ID':'" + $('#hfid_letter').val() + "'," +
                              //"'Number':'" + $('#txt_sh_letter').val() + "'," +
                              // "'DateLetter':'" + $('#txt_dte_letter').val() + "'," +
                               "'Subject':'" + $('#txt_moz_letter_out').val() + "'," +
                               //"'SenderOfficeID':'" + $('#ddl_fer_id_letter').val() + "'," +
                               "'ReceiverOfficeID':'" + $('#ddl_gir_id_letter_out').val() + "'," +
                              "'user_id_letter':'" +<%=Session["id_user"].ToString()%> +"'," +
                               "'DateRegister':'" + $('#txt_dte_sabt_letter_out').val() + "'," +
                               "'PageID':'" + $('#ddl_page_id_letter_out').val() + "'" +
                               //"'Content':'" + CKEDITOR.instances['txt_con_letter'].getData() + "'," +
                               //"'att_letter':'" + $('#txt_att_letter').val() + "'" +
                           "}",
                          async: false,
                          success: function (response) {
                              if (response.d != '-1') {
                                  //$('#txt_sh_letter').val('');
                                  //$('#txt_dte_letter').val('');
                                  //$('#txt_moz_letter_out').val('');
                                  //$('#ddl_gir_id_letter_out').val('');
                                  ////CKEDITOR.instances['txt_con_letter'].setData('');
                                  //$('#txt_att_letter').val('');

                                  alert("نامه با شماره اندیکاتور<< " + response.d + " >> ثبت شد");
                                  var UpdatePanel1 = '<%=UpdatePanel1.ClientID%>';
                                  if (UpdatePanel1 != null)
                                      __doPostBack(UpdatePanel1, '');
                                  //$("#form1").data("bootstrapValidator").resetForm(true);
                                  //if ($('#hfid_letter').val() != '-1')
                                  jQuery('#sabt_out_letter_dialog').dialog('close');
                                  return true;
                              }
                              else {
                                  alert("خطا در ثبت داده ها");
                                  return false;
                              }

                          },
                          error: function ()
                          { alert("خطا در ثبت داده ها"); jQuery('#sabt_out_letter_dialog').dialog('close'); return false; }
                      });

                  },
                  'انصراف': function () {
<%--                      var UpdatePanel1 = '<%=UpdatePanel1.ClientID%>';
                      if (UpdatePanel1 != null)
                          __doPostBack(UpdatePanel1, '');--%>
                      //$("#form1").data("bootstrapValidator").resetForm(true);
                      jQuery('#sabt_out_letter_dialog').dialog('close');
                  }
              }
          });
        });
    </script>
    <%-- Dialog Sabt Erja    --%>
    <script type="text/javascript">
        // cal ajax methode for save data
        $(document).ready(function () {
            $("#sabt_erja_letter_dialog").dialog(
          {
              autoOpen: false,
              resizable: false,
              modal: true,
              bgiframe: true,
              width: 700,
              height: 300,
              title: "ثبت ارجاع",
              open: function (event, ui) { $(".ui-dialog-titlebar-close").hide(); },
              show: {
                  effect: "fade",
                  duration: 50
              },
              hide: {
                  effect: "fade",
                  //effect: "fade",
                  duration: 50
              },
              close: function (event, ui) {
<%--                  var UpdatePanel1 = '<%=UpdatePanel1.ClientID%>';
                  if (UpdatePanel1 != null)
                      __doPostBack(UpdatePanel1, '');--%>
                  //$("#form1").data("bootstrapValidator").resetForm(true);
              },
              buttons: {
                  'ثبت ارجاع': function () {
                      $.ajax({
                          type: 'POST',
                          contentType: "application/json; charset=utf-8",
                          url: 'sabt_letter.aspx/sabt_erja_letter_method',
                          data: "{" +
                               "'ID':'" + $('#hfid_letter').val() + "'," +
                               "'Subject':'" + $('#txt_moz_erja').val() + "'," +
                               "'UserIDReceiver':'" + $('#ddl_ugir_erja_id').val() + "'," +
                               "'UserIDSender':'" +<%=Session["id_user"].ToString()%> +"'" +
                           "}",
                          async: false,
                          success: function (response) {
                              if (response.d != '-1') {
                                  alert("نامه با شماره اندیکاتور<< " + response.d + " >> ارجاع داده شد ");
                                  var UpdatePanel1 = '<%=UpdatePanel1.ClientID%>';
                                  if (UpdatePanel1 != null)
                                      __doPostBack(UpdatePanel1, '');
                                  jQuery('#sabt_erja_letter_dialog').dialog('close');
                                  return true;
                              }
                              else {
                                  alert("خطا در ثبت داده ها");
                                  return false;
                              }

                          },
                          error: function ()
                          { alert("خطا در ثبت داده ها"); jQuery('#sabt_erja_letter_dialog').dialog('close'); return false; }
                      });

                  },
                  'انصراف': function () {
<%--                      var UpdatePanel1 = '<%=UpdatePanel1.ClientID%>';
                      if (UpdatePanel1 != null)
                          __doPostBack(UpdatePanel1, '');--%>
                      //$("#form1").data("bootstrapValidator").resetForm(true);
                      jQuery('#sabt_erja_letter_dialog').dialog('close');
                  }
              }
          });
        });
    </script>
    <%-- Dialog Sabt In     --%>
    <script type="text/javascript">
        // cal ajax methode for save data
        $(document).ready(function () {
            $("#sabt_in_letter_dialog").dialog(
          {
              autoOpen: false,
              resizable: false,
              modal: false,
              bgiframe: true,
              width: 700,
              height: 350,
              title: "ثبت نامه وارده",
              open: function (event, ui) { $(".ui-dialog-titlebar-close").hide(); },
              show: {
                  effect: "fade",
                  duration: 50
              },
              hide: {
                  effect: "explode",
                  //effect: "fade",
                  duration: 50
              },
              close: function (event, ui) {
<%--                  var UpdatePanel1 = '<%=UpdatePanel1.ClientID%>';
                  if (UpdatePanel1 != null)
                      __doPostBack(UpdatePanel1, '');--%>
                  //setTimeout("location.reload(true);", 2000);
                  //$("#form1").data("bootstrapValidator").resetForm(true);
              },
              buttons: {
                  'ثبت و ادامه': function () {
                      var myvalue1 = $("#<%=txt_dte_sabt_letter_in.ClientID %>").val();
                      if (!checkShamsi(myvalue1)) {
                          $("#messagetext").text("تاریخ وارد شده صحیح نیست");
                          $("#message").fadeIn().delay(3000).fadeOut();
                          //alert("تاریخ وارد شده معتبر نیست!");
                          return false;
                      }
                      //var ckeditor = $('#txt_con_letter').val();
                      $.ajax({
                          type: 'POST',
                          contentType: "application/json; charset=utf-8",
                          url: 'sabt_letter.aspx/sabt_in_letter_method',
                          data: "{" +
                              "'ID':'" + $('#hfid_letter').val() + "'," +
                              "'Number':'" + $('#txt_sh_letter_in').val() + "'," +
                               "'DateLetter':'" + $('#txt_dte_letter_in').val() + "'," +
                               "'Subject':'" + $('#txt_moz_letter_in').val() + "'," +
                               "'SenderOfficeID':'" + $('#ddl_fer_id_letter_in').val() + "'," +
                               //"'ReceiverOfficeID':'" + $('#ddl_gir_id_letter_in').val() + "'," +
                               "'user_id_letter':'" +<%=Session["id_user"].ToString()%> +"'," +
                               "'DateRegister':'" + $('#txt_dte_sabt_letter_in').val() + "'," +
                               "'PageID':'" + $('#ddl_page_id_letter_in').val() + "'" +
                               //"'Content':'" + CKEDITOR.instances['txt_con_letter'].getData() + "'," +
                               //"'att_letter':'" + $('#txt_att_letter').val() + "'" +
                           "}",
                          async: false,
                          success: function (response) {
                              if (response.d != 'false') {
                                  //$('#txt_sh_letter').val('');
                                  //$('#txt_dte_letter').val('');
                                  //$('#txt_moz_letter_out').val('');
                                  //$('#ddl_gir_id_letter_out').val('');
                                  ////CKEDITOR.instances['txt_con_letter'].setData('');
                                  //$('#txt_att_letter').val('');

                                  alert("نامه با شماره اندیکاتور<< " + response.d + " >> ثبت شد");
                                  var UpdatePanel1 = '<%=UpdatePanel1.ClientID%>';
                                  if (UpdatePanel1 != null)
                                      __doPostBack(UpdatePanel1, '');
                                  //$("#form1").data("bootstrapValidator").resetForm(true);
                                  //if ($('#hfid_letter').val() != '-1')
                                  jQuery('#sabt_in_letter_dialog').dialog('close');
                                  return true;
                              }
                              else {
                                  alert("خطا در ثبت داده ها");
                                  return false;
                              }

                          },
                          error: function ()
                          { alert("خطا در ثبت داده ها"); jQuery('#sabt_in_letter_dialog').dialog('close'); return false; }
                      });

                  },
                  'انصراف': function () {
<%--                      var UpdatePanel1 = '<%=UpdatePanel1.ClientID%>';
                      if (UpdatePanel1 != null)
                          __doPostBack(UpdatePanel1, '');--%>
                      //$("#form1").data("bootstrapValidator").resetForm(true);
                      jQuery('#sabt_in_letter_dialog').dialog('close');
                  }
              }
          });
        });
    </script>
    <%-- Dialog Delete      --%>
    <script type="text/javascript">
        // cal ajax methode for delete data
        $(document).ready(function () {
            $('#delete_letter_dialog').dialog(
{
    autoOpen: false,
    resizable: true,
    modal: true,
    bgiframe: true,
    width: 300,
    height: 175,
    title: "حذف داده ها",
    open: function (event, ui) { $(".ui-dialog-titlebar-close").hide(); },
    show: {
        effect: "fade",
        duration: 50
    },
    hide: {
        effect: "explode",
        //effect: "fade",
        duration: 50
    },
    close: function (event, ui) {
        var UpdatePanel1 = '<%=UpdatePanel1.ClientID%>';
        if (UpdatePanel1 != null)
            __doPostBack(UpdatePanel1, '');
        //$("#form1").data("bootstrapValidator").resetForm(true);
    },
    buttons: {
        'حذف': function () {
            $.ajax({
                type: 'POST',
                contentType: "application/json; charset=utf-8",
                url: 'sabt_letter.aspx/delete_letter_method',
                data: "{'ID':'" + $('#hfid_letter').val() + "'" +
                           "}",
                async: false,
                success: function (response) {
                    if (response.d == 'true') {
                        alert("داده با موفقیت حذف شد");
                        jQuery('#delete_letter_dialog').dialog('close');
                        return true;
                    }
                    else if (response.d == '-1')  {
                        alert("نامه دارای ارجاع میباشد شما مجوز حذف ندارید");
                        jQuery('#delete_letter_dialog').dialog('close');
                        return false;
                    }
                    else {
                        alert("خطا در حذف داده ها");
                        jQuery('#delete_letter_dialog').dialog('close');
                        return false;
                    }

                },
                error: function ()
                { alert("خطا در حذف داده ها"); jQuery('#delete_letter_dialog').dialog('close'); return false; }
            });

        },
        'انصراف': function () {
<%--            var UpdatePanel1 = '<%=UpdatePanel1.ClientID%>';
            if (UpdatePanel1 != null)
                __doPostBack(UpdatePanel1, '');--%>
            //$("#form1").data("bootstrapValidator").resetForm(true);
            jQuery('#delete_letter_dialog').dialog('close');
        }
    }
});

        });
    </script>
    <%-- Dialog Content     --%>
    <script type="text/javascript">
        $(document).ready(function () {
            //cal ajax method for content letter
            $("#content_letter_dialog").dialog(
          {
              autoOpen: false,
              resizable: false,
              modal: false,
              bgiframe: true,
              width: 1000,
              height: 600,
              title: "ثبت متن نامه",
              open: function (event, ui) {
                  $(".ui-dialog-titlebar-close").hide();
                  $.ajax({
                      type: "POST",
                      contentType: "application/json; charset=utf-8",
                      url: 'sabt_letter.aspx/get_content_method',
                      data: "{" +
                          "'ID':'" + $('#hfid_letter').val() + "'" +
                       "}",
                      async: false,
                      success: function (response) {
                          if (response.d != '') {
                              var Content = response.d;
                              CKEDITOR.instances['txt_con_letter_ed'].setData(Content);
                          }
                          else {
                              CKEDITOR.instances['txt_con_letter_ed'].setData("");
                          }

                      },
                      error: function ()
                      { alert("خطا در ثبت داده ها"); return false; }
                  });

              },
              show: {
                  effect: "fade",
                  duration: 50
              },
              hide: {
                  effect: "explode",
                  //effect: "fade",
                  duration: 50
              },
              close: function (event, ui) {
                  var UpdatePanel1 = '<%=UpdatePanel1.ClientID%>';
                  if (UpdatePanel1 != null)
                      __doPostBack(UpdatePanel1, '');
                  //$("#form1").data("bootstrapValidator").resetForm(true);
              },
              buttons: {
                  'ثبت و ادامه': function () {
                      //var ckeditor = $('#txt_con_letter').val();
                      $.ajax({
                          type: 'POST',
                          contentType: "application/json; charset=utf-8",
                          url: 'sabt_letter.aspx/sabt_out_content_method',
                          data: "{" +
                              "'ID':'" + $('#hfid_letter').val() + "'," +
                               "'Content':'" + CKEDITOR.instances['txt_con_letter_ed'].getData() + "'" +
                           "}",
                          async: false,
                          success: function (response) {
                              if (response.d == 'true') {
                                  //CKEDITOR.instances['txt_con_letter_ed'].setData('');
                                  alert("داده ها با موفقیت ثبت شد");
<%--                                  var UpdatePanel1 = '<%=UpdatePanel1.ClientID%>';
                                  if (UpdatePanel1 != null)
                                      __doPostBack(UpdatePanel1, '');--%>
                                  //$("#form1").data("bootstrapValidator").resetForm(true);
                                  //if ($('#hfid_letter').val() != '-1')
                                  jQuery('#content_letter_dialog').dialog('close');
                                  return true;
                              }
                              else {
                                  alert("خطا در ثبت داده ها");
                                  jQuery('#content_letter_dialog').dialog('close');
                                  return false;
                              }

                          },
                          error: function ()
                          { alert("خطا در ثبت داده ها"); jQuery('#content_letter_dialog').dialog('close'); return false; }
                      });

                  },
                  'انصراف': function () {
<%--                      var UpdatePanel1 = '<%=UpdatePanel1.ClientID%>';
                      if (UpdatePanel1 != null)
                          __doPostBack(UpdatePanel1, '');--%>
                      //$("#form1").data("bootstrapValidator").resetForm(true);
                      jQuery('#content_letter_dialog').dialog('close');
                  },

              }
          });

        });
    </script>
    <%-- Start Call Dialog Update,Save,Delete   -------------%>
    <%-- SaveDialog Call Out    --%>
    <script type="text/javascript">
        $(document).ready(function () {
            //Sabt Dialog Call 
            //
            $(document).on("click", "[id*=lbtn_output_letter]", function () {
                //$('#txt_sh_letter').val('');
                //$('#txt_dte_letter').val('');
                $('#txt_moz_letter_out').val('');

                //$('#ddl_gir_id_letter_out').val(-1);
                $('#ddl_gir_id_letter_out').val(-1).trigger("change");
                $("#ddl_gir_id_letter_out").select2();
                //CKEDITOR.instances['txt_con_letter'].setData('');
                //alert(PersianDatePicker.Show());
                //alert("<%=PersianDateTime.Now.ToString(PersianDateTimeFormat.Date) %>");
                $('#txt_dte_sabt_letter_out').val('<%=PersianDateTime.Now.ToString(PersianDateTimeFormat.Date) %>');
                //$('#txt_dte_sabt_letter_out').val('');
                //$('#txt_att_letter').val('');
                $('#ddl_page_id_letter_out').val(-1);
                $("[id*=ddl_page_id_letter_out]").removeAttr("disabled");
                $("[id*=ddl_page_id_letter_out]").attr("enabled", "enabled");

                $('#txt_moz_letter_out').focus();
                //$('#txt_sh_letter').focus();
                $('#hfid_letter').val('-1');  //ClientIDMode="Static"
                //$("[id*=hfid_carddabir]").val('-1'); without ClientIDMode="Static"
                $('#sabt_out_letter_dialog').dialog('open'); return false;
            });
        });
    </script>
    <%-- SaveDialog Call In     --%>
    <script type="text/javascript">
        $(document).ready(function () {
            //Sabt Dialog Call 
            //
            $(document).on("click", "[id*=lbtn_input_letter]", function () {
                $('#txt_sh_letter_in').val('');
                $('#txt_dte_letter_in').val('');
                $('#txt_moz_letter_in').val('');
                //$('#ddl_fer_id_letter_in').val(-1);
                $('#ddl_fer_id_letter_in').val(-1).trigger("change");
                $("#ddl_fer_id_letter_in").select2();

                //CKEDITOR.instances['txt_con_letter'].setData('');
                $('#txt_dte_sabt_letter_in').val('<%=PersianDateTime.Now.ToString(PersianDateTimeFormat.Date) %>');
                //$('#txt_dte_sabt_letter_in').val('');
                //$('#txt_att_letter').val('');
                $('#ddl_page_id_letter_in').val(-1);
                $("[id*=ddl_page_id_letter_in]").removeAttr("disabled");
                $("[id*=ddl_page_id_letter_in]").attr("enabled", "enabled");

                $('#txt_moz_letter_in').focus();
                //$('#txt_sh_letter').focus();
                $('#hfid_letter').val('-1');  //ClientIDMode="Static"
                //$("[id*=hfid_carddabir]").val('-1'); without ClientIDMode="Static"
                $('#sabt_in_letter_dialog').dialog('open'); return false;
            });
        });
    </script>
    <%-- Update Dialog Call     --%>
    <script type="text/javascript">
        // cal ajax methode for save data
        $(document).ready(function () {

            //Update Dialog Call 
            //

            $(document).on("click", "[id*=lbtn_edit_letter]", function (e) {
                var mydata = ($(this).attr('data-id'));
                $.ajax({
                    type: 'POST',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    url: 'sabt_letter.aspx/get_letter_byid',
                    data: "{" +
                        "'ID':'" + mydata + "'" +
                     "}",
                    async: false,
                    success: function (response) {
                        $.map($.parseJSON(response.d), function (item) {
                            $("[id*=txt_sh_letter_in]").val(item.Number);
                            $("[id*=txt_dte_letter_in]").val(item.DateLetter);
                            $("[id*=txt_moz_letter_in]").val(item.Subject);
                            $("[id*=txt_moz_letter_out]").val(item.Subject);
                            $("[id*=txt_dte_sabt_letter_out]").val(item.DateRegister);
                            $("[id*=txt_dte_sabt_letter_in]").val(item.DateRegister);

                            $('#ddl_gir_id_letter_out').val(item.ReceiverOfficeID).trigger("change");
                            $("#ddl_gir_id_letter_out").select2();

                            //$("[id*=ddl_gir_id_letter_out] option").filter(function () {
                            //    return $(this).text() == item.nam_gir;
                            //}).prop('selected', true);

                            $('#ddl_fer_id_letter_in').val(item.SenderOfficeID).trigger("change");
                            $("#ddl_fer_id_letter_in").select2();

                            //$("[id*=ddl_fer_id_letter_in] option").filter(function () {
                            //    return $(this).text() == item.nam_fer;
                            //}).prop('selected', true);


                            $("[id*=ddl_page_id_letter_out] option").filter(function () {
                                return $(this).text() == item.page_name;
                            }).prop('selected', true);
                            $("[id*=ddl_page_id_letter_in] option").filter(function () {
                                return $(this).text() == item.page_name;
                            }).prop('selected', true);
                            $("[id*=hfid_letter]").val(mydata);

                            $("[id*=ddl_page_id_letter_out]").attr("disabled", "disabled");
                            $("[id*=ddl_page_id_letter_in]").attr("disabled", "disabled");

                            if (item.Number == 'صادره' && item.DateLetter == '9999/99/99')
                                $('#sabt_out_letter_dialog').dialog('open');
                            else
                                $('#sabt_in_letter_dialog').dialog('open');

                            return true;
                        });
                    },
                    error: function () {
                        alert("خطای نا شناخته در باز کردن پنجره ویرایش");
                        return false;
                    }
                });
                //var row = $(this).parents("tr:first");
                ////var Number = row.children("td:eq(1)").text();
                ////var DateLetter = row.children("td:eq(2)").text();
                //var Subject = row.children("td:eq(3)").text();
                //var ReceiverOfficeID = row.children("td:eq(4)").text();
                ////var Content = row.children("td:eq(5)").html();
                ////var att_letter = row.children("td:eq(5)").text();

                ////$("[id*=txt_sh_letter]").val(Number);
                ////$("[id*=txt_dte_letter]").val(DateLetter);
                //$("[id*=txt_moz_letter_out]").val(Subject);
                //$("[id*=ddl_gir_id_letter_out] option").filter(function () {
                //    return $(this).text() == ReceiverOfficeID;
                //}).prop('selected', true);

                ////CKEDITOR.instances['txt_con_letter'].setData(Content)

                ////$("[id*=txt_att_letter]").val(att_letter);
                //$("[id*=hfid_letter]").val(mydata);
                //$('#sabt_out_letter_dialog').dialog('open'); return false;
            });
        });
    </script>
    <%-- Delete Dialog Call     --%>
    <script type="text/javascript">
        // cal ajax methode for save data
        $(document).ready(function () {
            //Delete Dialog Call 
            //
            $(document).on("click", "[id*=lbtn_delete_letter]", function (e) {
                var mydata = ($(this).attr('data-id'));
                $("[id*=hfid_letter]").val(mydata);
                $('#delete_letter_dialog').dialog('open'); return false;
            });
        });
    </script>
    <%-- Content Dialog Call    --%>
    <script type="text/javascript">
        // cal ajax methode for save data
        $(document).ready(function () {

            //Content Dialog Call 
            //

            $(document).on("click", "[id*=lbtn_content_letter_dialog]", function (e) {
                var mydata = ($(this).attr('data-id'));
                //var row = $(this).parents("tr:first");
                //var Content = row.children("td:eq(5)").html();

                //CKEDITOR.instances['txt_con_letter_ed'].setData(Content)

                $("[id*=hfid_letter]").val(mydata);
                $('#content_letter_dialog').dialog('open'); return false;
            });
        });
    </script>
    <%-- Erja Dialog Call    --%>
    <script type="text/javascript">
        // cal ajax methode for save data
        $(document).ready(function () {

            //Erja Dialog Call 
            //
            $(document).on("click", "[id*=lbtn_erja_letter]", function (e) {
                var mydata = ($(this).attr('data-id'));
                //var row = $(this).parents("tr:first");
                //var Content = row.children("td:eq(5)").html();

                //CKEDITOR.instances['txt_con_letter_ed'].setData(Content)

                $("[id*=hfid_letter]").val(mydata);
                //$('#ddl_ugir_erja_id').val(item.ReceiverOfficeID).trigger("change");
                //$("#ddl_ugir_erja_id").select2();


                $('#sabt_erja_letter_dialog').dialog('open'); return false;
            });
        });
    </script>
    <%-- Print Dialog Call      --%>
    <script type="text/javascript">
        // cal ajax methode for print data
        $(document).ready(function () {
            //Print Call 
            //

            //    $(document).on("click", "[id*=lbtn_print_letter]", function (e) {
            //        var mydata = ($(this).attr('data-id'));
            //        var row = $(this).parents("tr:first");
            //        $("[id*=hfid_letter]").val(mydata);
            //        $.ajax({
            //            type: 'POST',
            //            contentType: "application/json; charset=utf-8",
            //            url: 'sabt_letter.aspx/PrintMethod',
            //            data: "{" +
            //                "'ID':'" + $('#hfid_letter').val() + "'," +
            //                 "'Content':'" + CKEDITOR.instances['txt_con_letter_ed'].getData() + "'" +
            //             "}",
            //            async: false,
            //            success: function (response) {
            //                if (response.d == 'true') {
            //                    return true;
            //                }
            //                else {
            //                    alert("خطا در چاپ داده ها");
            //                    return false;
            //                }

            //            },
            //            error: function ()
            //            { alert("خطا در چاپ داده ها"); return false; }
            //        });
            //    });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            //$("#ddl_gir_id_letter_out").select2();
            //$("#ddl_fer_id_letter_in").select2();
            //$("#ddl_page_id_letter_in").select2();
            //$("#ddl_page_id_letter_out").select2();
            //$("#ddl_search_combine").select2();
            //$("#ddl_search_field").select2();
            //$("#ddl_search_option").select2();
            //$("#ddl_ugir_erja_id").select2();
        });
    </script>



    <asp:HiddenField ID="hfid_letter" runat="server" ClientIDMode="Static" />
    <%--<asp:HiddenField ID="hfield_search" runat="server" ClientIDMode="Static"/>--%>
    <%-- Start Message Box Sabt,Update          -------------%>
    <div id="sabt_out_letter_dialog" style="display: none">
        <div class="form-group  form-group-sm">
            <%--            <div class="row">
                <div class="col-sm-6">
                    <asp:HiddenField ID="hfid_letter" runat="server" ClientIDMode="Static" />
                    <asp:Label ID="Label1" runat="server" Text="شماره نامه"></asp:Label>
                    <asp:TextBox ID="txt_sh_letter" runat="server" Width="220px" CssClass="form-control" ClientIDMode="Static" placeholder="شماره نامه را وارد کنید" lang="fa"></asp:TextBox>
                </div>
                <div class="col-sm-6">
                    <asp:Label ID="Label2" runat="server" Text="تاریخ نامه"></asp:Label>
                    <asp:TextBox ID="txt_dte_letter" runat="server" Width="220px" CssClass="form-control" ClientIDMode="Static" placeholder="تاریخ نامه را وارد کنید" lang="fa"></asp:TextBox>
                </div>
            </div>--%>
            <div class="row">
                <div class="col-sm-12">
                    <asp:Label ID="Label3" runat="server" Text="موضوع نامه"></asp:Label>
                    <asp:TextBox ID="txt_moz_letter_out" runat="server" Width="565px" CssClass="form-control" ClientIDMode="Static" placeholder="موضوع نامه را وارد کنید" lang="fa"></asp:TextBox>
                </div>
                <div class="col-sm-12">
                    <asp:Label ID="Label4" runat="server" Text="گیرنده نامه"></asp:Label>
                    <br />
                    <asp:DropDownList ID="ddl_gir_id_letter_out" runat="server" Width="565px" ClientIDMode="Static" lang="fa"></asp:DropDownList>
                </div>
                <div class="col-sm-6">
                    <asp:Label ID="Label7" runat="server" Text="تاریخ ثبت نامه"></asp:Label>
                    <asp:TextBox ID="txt_dte_sabt_letter_out" runat="server" Width="150px" CssClass="form-control" ClientIDMode="Static" placeholder="تاریخ ثبت نامه را وارد کنید" lang="fa"></asp:TextBox>
                </div>
                <div class="col-sm-6">
                    <asp:Label ID="Label9" runat="server" Text="نوع الگو"></asp:Label>
                    <asp:DropDownList ID="ddl_page_id_letter_out" runat="server" Width="220px" CssClass="form-control" ClientIDMode="Static" lang="fa"></asp:DropDownList>
                </div>
            </div>
            <div class="row">
                <%--                <div class="col-sm-12">
                    <asp:Label ID="Label6" runat="server" Text="متن نامه"></asp:Label>
                    <CKEditor:CKEditorControl ID="txt_con_letter" runat="server" Height="200" ClientIDMode="Static"></CKEditor:CKEditorControl>
                </div>--%>
                <%--                <div class="col-sm-6">
                    <asp:Label ID="Label7" runat="server" Text="پیوست نامه"></asp:Label>
                    <asp:TextBox ID="txt_att_letter" runat="server" Width="220px" CssClass="form-control" ClientIDMode="Static" placeholder="پیوست نامه را وارد کنید" MaxLength="10" lang="fa"></asp:TextBox>
                </div>--%>
            </div>

        </div>
    </div>
    <%-- End Message Box Sabt,Update            -------------%>
    <%-- Start Message Box Sabt,Update          -------------%>
    <div id="sabt_in_letter_dialog" style="display: none">
        <div class="form-group  form-group-sm">
            <div class="row">
                <div class="col-sm-6">
                    <asp:Label ID="Label1" runat="server" Text="شماره نامه"></asp:Label>
                    <asp:TextBox ID="txt_sh_letter_in" runat="server" Width="220px" CssClass="form-control" ClientIDMode="Static" placeholder="شماره نامه را وارد کنید" lang="fa"></asp:TextBox>
                </div>
                <div class="col-sm-6">
                    <asp:Label ID="Label2" runat="server" Text="تاریخ نامه"></asp:Label>
                    <asp:TextBox ID="txt_dte_letter_in" runat="server" Width="220px" CssClass="form-control" ClientIDMode="Static" placeholder="تاریخ نامه را وارد کنید" lang="fa"></asp:TextBox>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <asp:Label ID="Label5" runat="server" Text="موضوع نامه"></asp:Label>
                    <asp:TextBox ID="txt_moz_letter_in" runat="server" Width="565px" CssClass="form-control" ClientIDMode="Static" placeholder="موضوع نامه را وارد کنید" lang="fa"></asp:TextBox>
                </div>
                <div class="col-sm-6">
                    <asp:Label ID="Label6" runat="server" Text="فرستنده نامه"></asp:Label>
                    <br />
                    <asp:DropDownList ID="ddl_fer_id_letter_in" runat="server" Width="565px" ClientIDMode="Static" lang="fa"></asp:DropDownList>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <asp:Label ID="Label8" runat="server" Text="تاریخ ثبت نامه"></asp:Label>
                    <asp:TextBox ID="txt_dte_sabt_letter_in" runat="server" Width="150px" CssClass="form-control" ClientIDMode="Static" placeholder="تاریخ ثبت نامه را وارد کنید" lang="fa"></asp:TextBox>
                </div>
                <div class="col-sm-6">
                    <asp:Label ID="Label10" runat="server" Text="نوع الگو"></asp:Label>
                    <asp:DropDownList ID="ddl_page_id_letter_in" runat="server" Width="220px" CssClass="form-control" ClientIDMode="Static" lang="fa"></asp:DropDownList>
                </div>
            </div>
            <div class="row">
                <%--                <div class="col-sm-12">
                    <asp:Label ID="Label6" runat="server" Text="متن نامه"></asp:Label>
                    <CKEditor:CKEditorControl ID="txt_con_letter" runat="server" Height="200" ClientIDMode="Static"></CKEditor:CKEditorControl>
                </div>--%>
                <%--                <div class="col-sm-6">
                    <asp:Label ID="Label7" runat="server" Text="پیوست نامه"></asp:Label>
                    <asp:TextBox ID="txt_att_letter" runat="server" Width="220px" CssClass="form-control" ClientIDMode="Static" placeholder="پیوست نامه را وارد کنید" MaxLength="10" lang="fa"></asp:TextBox>
                </div>--%>
            </div>

        </div>
    </div>
    <%-- End Message Box Sabt,Update            -------------%>
    <%-- Start Message Box Sabt Erja          -------------%>
    <div id="sabt_erja_letter_dialog" style="display: none">
        <div class="form-group  form-group-sm">
            <div class="row">
                <div class="col-sm-12">
                    <asp:Label ID="Label14" runat="server" Text="موضوع ارجاع"></asp:Label>
                    <asp:TextBox ID="txt_moz_erja" runat="server" Width="565px" CssClass="form-control" ClientIDMode="Static" placeholder="موضوع ارجاع را وارد کنید" lang="fa"></asp:TextBox>
                </div>
                <div class="col-sm-6">
                    <asp:Label ID="Label15" runat="server" Text="گیرنده ارجاع"></asp:Label>
                    <br/>
                    <asp:DropDownList ID="ddl_ugir_erja_id" runat="server" CssClass=" form-control chosen-rtl" Width="565px" ClientIDMode="Static" lang="fa"></asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
    <%-- End Message Box Sabt,Update            -------------%>
    <%-- Start Message Box Content Letter       -------------%>
    <div id="content_letter_dialog" style="display: none">
        <div class="form-group  form-group-sm">
            <div class="row">
                <div class="col-sm-12">
                    <asp:Label ID="Label11" runat="server" Text="متن نامه"></asp:Label>
                    <CKEditor:CKEditorControl ID="txt_con_letter_ed" runat="server" Height="330" ClientIDMode="Static" Skin="moono" lang="fa"></CKEditor:CKEditorControl>
                    <%--                                        <asp:TextBox ID="txt_con_letter" runat="server" Width="220px" CssClass="form-control" ClientIDMode="Static" placeholder="متن نامه را وارد کنید" MaxLength="10"></asp:TextBox>--%>
                </div>
            </div>

        </div>
    </div>
    <%-- End Message Box Content Letter         -------------%>
    <%-- Start Message Box Delete               -------------%>
    <div id="delete_letter_dialog" style="display: none">
        <p>
            <br />
            آیا برای حذف اطمینان دارید؟
        </p>
    </div>
    <%-- End Message Box Delete                 -------------%>
    <%-- Start Grid view                        -------------%>
    <div class="container full-width">
        <!--------------------------------------------------Message Box--------------------------------------------->
        <div class="row">
            <asp:HiddenField ID="messagetexthf" runat="server" />
            <div class="col-sm-12" id="message" style="display: none">
                <div id="messagetext" class="alert-danger text-center">پیام خطا</div>
            </div>
        </div>
        <!--------------------------------------------------------------------------------------------------------->
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-info">
                    <div class="panel-body">
                        <div class="row">
                            <%--                            <div class="col-sm-12">--%>
                            <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <%--                                    <script type="text/javascript" language="javascript">
                                        Sys.Application.add_load(myfunc);
                                    </script>--%>
                                    <%-- بخش جستجو در سایت --%>
                                    <div id="search_panel" style="display: block; margin-left: 5px;">
                                        <div class="form-inline">
                                            <div class="form-group form-group-sm">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <asp:Label ID="lbl_search_text" runat="server" CssClass="alert-warning" Font-Size="Smaller"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <asp:DropDownList ID="ddl_search_field" runat="server" CssClass="form-control" Width="120px"></asp:DropDownList>
                                                        <asp:DropDownList ID="ddl_search_option" runat="server" CssClass="form-control" Width="120px"></asp:DropDownList>
                                                        <asp:TextBox ID="txt_search" runat="server" CssClass="form-control" Width="120px"></asp:TextBox>
                                                        <asp:DropDownList ID="ddl_search_combine" runat="server" CssClass="form-control" Width="120px"></asp:DropDownList>
                                                        <asp:LinkButton ID="lbtn_add_search" runat="server" CssClass="btn btn-primary btn-sm glyphicon glyphicon-plus" data-toggle="tooltip" data-placement="top" title="اضافه شدن به جستجو" OnClick="lbtn_add_search_Click"></asp:LinkButton>
                                                        <asp:LinkButton ID="lbtn_search" runat="server" CssClass="btn btn-primary btn-sm glyphicon glyphicon-search" data-toggle="tooltip" data-placement="top" title="جستجو" OnClick="lbtn_search_Click"></asp:LinkButton>
                                                        <asp:LinkButton ID="lbtn_search_clear" runat="server" CssClass="btn btn-danger btn-sm glyphicon glyphicon-remove" data-toggle="tooltip" data-placement="top" title="پاک کردن جستجو" OnClick="lbtn_search_clear_Click"></asp:LinkButton>
                                                        <%--<asp:LinkButton ID="lbtn_back" runat="server" CssClass="btn btn-warning btn-sm glyphicon glyphicon-backward" data-toggle="tooltip" data-placement="top" title="بازگشت به صفحه قبل" OnClick="lbtn_back_Click"></asp:LinkButton>--%>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <%-- بخش جستجو در سایت --%>
                                    <%-- بخش ثبت اطلاعات --%>
                                    <div id="sabt_panel" style="display: block; margin: 5px;">
                                        <div class="form-inline">
                                            <div class="form-group form-group-sm">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <asp:LinkButton ID="lbtn_lastweek" runat="server" CssClass="btn  btn-primary btn-sm glyphicon glyphicon-arrow-left" data-toggle="tooltip" data-placement="top" title="هفته قبل" OnClick="lbtn_lastweek_Click"></asp:LinkButton>
                                                        <asp:Label ID="lbl_lastweek" runat="server">از</asp:Label>
                                                        <asp:TextBox ID="txt_lastweek" runat="server" CssClass="form-control" Width="90px"></asp:TextBox>
                                                        <asp:Label ID="lbl_nextweek" runat="server">تا</asp:Label>
                                                        <asp:TextBox ID="txt_nextweek" runat="server" CssClass="form-control" Width="90px"></asp:TextBox>
                                                        <asp:LinkButton ID="lbtn_nextweek" runat="server" CssClass="btn  btn-primary btn-sm glyphicon glyphicon-arrow-right" data-toggle="tooltip" data-placement="top" title="هفته بعد" OnClick="lbtn_nextweek_Click"></asp:LinkButton>
                                                        <asp:LinkButton ID="lbtn_okweek" runat="server" CssClass="btn  btn-primary btn-sm glyphicon glyphicon-ok" data-toggle="tooltip" data-placement="top" title="تائید تاریخ" OnClick="lbtn_okweek_Click"></asp:LinkButton>
                                                        <%--<asp:LinkButton ID="lbtn_search_call" runat="server" CssClass="btn btn-primary btn-sm glyphicon glyphicon-search" data-toggle="tooltip" data-placement="top" title="جستجو" OnClick="lbtn_search_call_Click"></asp:LinkButton>--%>
                                                        <asp:LinkButton ID="lbtn_input_letter" runat="server" CssClass="btn  btn-warning btn-sm glyphicon glyphicon-save-file" data-toggle="tooltip" data-placement="top" title="ثبت نامه وارده"></asp:LinkButton>
                                                        <asp:LinkButton ID="lbtn_output_letter" runat="server" CssClass="btn  btn-success btn-sm glyphicon glyphicon-open-file" data-toggle="tooltip" data-placement="top" title="ثبت نامه صادره"></asp:LinkButton>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <%-- بخش ثبت اطلاعات --%>
                                    <asp:GridView ID="gvdata" runat="server" CellPadding="3" GridLines="Horizontal" AutoGenerateColumns="False"
                                        Width="100%" UseAccessibleHeader="False" DataKeyNames="ID" BackColor="White" BorderColor="#E7E7FF"
                                        BorderStyle="None" BorderWidth="1px" AllowPaging="True"
                                        OnPageIndexChanging="gvdata_PageIndexChanging" OnRowCommand="gvdata_RowCommand"
                                        OnRowDataBound="gvdata_RowDataBound"
                                        AllowSorting="True" OnSorting="gvdata_Sorting" PageSize="7">
                                        <AlternatingRowStyle BackColor="#F7F7F7" />
                                        <Columns>
                                            <%--                                            <asp:TemplateField HeaderText="ردیف" Visible="false">
                                                <ItemTemplate><%# Container.DataItemIndex +1 %></ItemTemplate>
                                                <ItemStyle Width="2%" />
                                            </asp:TemplateField>--%>
                                            <asp:TemplateField HeaderText="شماره" SortExpression="LetterID">
                                                <ItemTemplate><%# (Eval("LetterID").ToString())%></ItemTemplate>
                                                <ItemStyle Width="1%"/>
                                            </asp:TemplateField>
<%--                                            <asp:TemplateField HeaderText="نوع" SortExpression="Letter.Attribute">
                                                <ItemTemplate><%# (Eval("Letter.DateLetter").ToString()=="9999/99/99"?"صادره":"وارده")%></ItemTemplate>
                                                <ItemStyle Width="4%" />
                                            </asp:TemplateField>--%>
                                            <%--                                            <asp:TemplateField HeaderText="شماره نامه" SortExpression="Number">
                                                <ItemTemplate><%# (Eval("Number").ToString())%></ItemTemplate>
                                                <ItemStyle Width="10%" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="تاریخ نامه">
                                                <ItemTemplate><%#Eval("DateLetter").ToString()%></ItemTemplate>
                                                <ItemStyle Width="5%" />
                                            </asp:TemplateField>--%>
                                            <asp:TemplateField HeaderText="موضوع" SortExpression="Letter.Subject">
                                                <ItemTemplate><%#MyObjects.myclass.SubString(Eval("Letter.Subject").ToString(),30)%></ItemTemplate>
                                                <ItemStyle Width="15%" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="از" SortExpression="Letter.SenderOffice.NameOffice">
                                                <ItemTemplate><%#MyObjects.myclass.SubString(Eval("Letter.SenderOffice.NameOffice").ToString(),20)%></ItemTemplate>
                                                <ItemStyle Width="7%" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="به" SortExpression="Letter.ReceiverOffice.NameOffice">
                                                <ItemTemplate><%#MyObjects.myclass.SubString(Eval("Letter.ReceiverOffice.NameOffice").ToString(),20)%></ItemTemplate>
                                                <ItemStyle Width="7%" />
                                            </asp:TemplateField>
                                            <%--                                            <asp:TemplateField HeaderText="متن نامه" Visible="false">
                                                <ItemTemplate><%#Eval("Content").ToString()%></ItemTemplate>
                                                <ItemStyle Width="10%" />
                                            </asp:TemplateField>--%>
                                            <%--                                            <asp:TemplateField HeaderText="پیوست نامه" Visible="false">
                                                <ItemTemplate><%#Eval("att_letter").ToString()%></ItemTemplate>
                                                <ItemStyle Width="10%" />
                                            </asp:TemplateField>--%>
                                            <asp:TemplateField HeaderText="تاریخ ثبت" SortExpression="DateReference">
                                                <ItemTemplate><%#Eval("DateReference").ToString()%></ItemTemplate>
                                                <ItemStyle Width="3%" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="فرستنده" SortExpression="UserSender.FullName">
                                                <ItemTemplate><%# (Eval("UserSender.FullName").ToString())%></ItemTemplate>
                                                <ItemStyle Width="5%" />
                                            </asp:TemplateField>
                                            <%--                                            <asp:TemplateField HeaderText="نوع الگو">
                                                <ItemTemplate><%#Eval("tbl_page.page_name").ToString()%></ItemTemplate>
                                                <ItemStyle Width="5%" />
                                            </asp:TemplateField>--%>
                                            <asp:TemplateField HeaderText="عملیات">
                                                <ItemTemplate>
                                                    <asp:LinkButton runat="server" ID="lbtn_edit_letter" CommandArgument='<%#Eval("Letter.ID")%>' CommandName="do_edit_letter" CssClass="btn btn-success btn-xs glyphicon glyphicon-edit" data-id='<%#Eval("Letter.ID")%>' data-toggle="tooltip" data-placement="top" title="ویرایش نامه" Width="30px" Enabled='<%#(Eval("Letter.KindLetter").ToString()=="1"?false:true) || Convert.ToBoolean(Session["Action_Letter"].ToString())%>'></asp:LinkButton>
                                                    <asp:LinkButton runat="server" ID="lbtn_delete_letter" CommandArgument='<%#Eval("Letter.ID")%>' CommandName="do_delete_letter" CssClass="btn btn-danger btn-xs glyphicon glyphicon-trash" data-id='<%#Eval("Letter.ID")%>' data-toggle="tooltip" data-placement="top" title="حذف نامه" Width="30px" Enabled='<%#(Eval("Letter.KindLetter").ToString()=="1"?false:true) || Convert.ToBoolean(Session["Action_Letter"].ToString())%>'></asp:LinkButton>
                                                    <asp:LinkButton runat="server" ID="lbtn_content_letter_dialog" CommandArgument='<%#Eval("Letter.ID")%>' CommandName="do_content_letter_dialog" CssClass="btn btn-primary btn-xs glyphicon glyphicon-pencil" data-id='<%#Eval("Letter.ID")%>' data-toggle="tooltip" data-placement="top" title="ویرایش متن نامه" Width="30px" Enabled='<%#(Eval("Letter.KindLetter").ToString()=="1"?false:true) || Convert.ToBoolean(Session["Action_Letter"].ToString())%>'></asp:LinkButton>
                                                    <asp:LinkButton runat="server" ID="lbtn_erja_letter" CommandArgument='<%#Eval("Letter.ID")%>' CommandName="do_erja_letter" CssClass="btn btn-info btn-xs glyphicon glyphicon-move" data-id='<%#Eval("Letter.ID")%>' data-toggle="tooltip" data-placement="top" title="ارجاع نامه" Width="30px" Enabled='<%#(Eval("Letter.KindLetter").ToString()=="1"?false:true) || Convert.ToBoolean(Session["Action_Letter"].ToString())%>'></asp:LinkButton>
                                                    <asp:LinkButton runat="server" ID="lbtn_print_letter" CommandArgument='<%#Eval("Letter.ID")%>' CommandName="do_print_letter" CssClass="btn btn-info btn-xs glyphicon glyphicon-print" data-id='<%#Eval("Letter.ID")%>' data-toggle="tooltip" data-placement="top" title='<%# " پرینت با "+ Eval("Letter.Page.Name") %>' Width="30px" Enabled='<%#(Eval("Letter.KindLetter").ToString()=="2"?false:true) || Convert.ToBoolean(Session["Action_Letter"].ToString())%>'></asp:LinkButton>
                                                </ItemTemplate>
                                                <ItemStyle Width="13%" />
                                            </asp:TemplateField>
                                        </Columns>
                                        <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
                                        <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" HorizontalAlign="Center" VerticalAlign="Middle" Font-Size="Smaller" />
                                        <PagerSettings Position="TopAndBottom" />
                                        <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Center" VerticalAlign="Middle" Font-Bold="true" CssClass="mypager" />
                                        <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Center" VerticalAlign="Middle" Font-Size="Smaller" Font-Bold="true" Height="30px" />
                                        <SelectedRowStyle BackColor="#738A9C" ForeColor="#F7F7F7" Font-Bold="True" />
                                        <SortedAscendingCellStyle BackColor="#F4F4FD" />
                                        <SortedAscendingHeaderStyle BackColor="#5A4C9D" />
                                        <SortedDescendingCellStyle BackColor="#D8D8F0" />
                                        <SortedDescendingHeaderStyle BackColor="#3E3277" />
                                    </asp:GridView>
                                </ContentTemplate>
                                <Triggers>
                                    <%--                                        <asp:PostBackTrigger ControlID="lbtn_print_letter" />--%>
                                </Triggers>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
            <%--</div>--%>
        </div>
    </div>
    <%-- End Grid view                          -------------%>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0px; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <%--<span style="border-width: 0px; position: fixed; padding: 50px; background-color: #FFFFFF; font-size: 36px; left: 40%; top: 40%;">Loading ...</span>--%>
                <%--<img alt="" src="../images/loader.gif" />--%>
                <div class="loader" style="top: 300px">Loading...</div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
