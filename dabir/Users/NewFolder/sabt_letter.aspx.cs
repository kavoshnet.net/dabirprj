﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Globalization;
using EntityFramework.Extensions;
using MyObjects;
using DataLayer.DataAccessLayer;
using DomainClass.DomainModel;
using NReco.PdfGenerator;
using System.Web.Script.Serialization;
using System.IO;

namespace dabir.Users
{
    /// <summary>
    /// کنترل صفحات در این قسمت انجام میگیرد
    /// </summary>
    public partial class sabt_letter : System.Web.UI.Page
    {
        /// <summary>
        /// متغیری برای دسترسی به کل مدل تولید شده
        /// </summary>
        DataBaseContext _edbcontext = new DataBaseContext();
        #region userfunction
        /// <summary>
        /// تابعی برای پر کردن اطلاعات گرید ویو
        /// که بر اساس عنوان صفحه مرتب می گردد
        /// </summary>
        protected void SetGVdata(string lastweek, string nextweek)
        {

            int id_user = Convert.ToInt16(Session["id_user"].ToString());
            var temp = (from t in _edbcontext.References select t);
            if (ViewState["search"] != null && ViewState["search"].ToString() != string.Empty)
            {
                string VStateSearch = ViewState["search"].ToString();
                string laststring = VStateSearch.Substring(0, VStateSearch.Length - 1).Split(' ').Last();
                if (laststring != "AND" && laststring != "OR")
                {
                    string sqlquery = "SELECT  * FROM V_tbl_erja WHERE " + ViewState["search"].ToString() + " AND ugir_erja_id = " + id_user.ToString();
                    //gvdata.DataSource= _edbcontext.tbl_letter.SqlQuery(sqlquery).ToList();
                    temp = _edbcontext.References.SqlQuery(sqlquery).AsQueryable<Reference>();
                }
            }
            else
            {
                if (lastweek != string.Empty && nextweek != string.Empty)
                {
                    temp = (from t in _edbcontext.References
                            where (
                                   (string.Compare(t.DateReference, lastweek) >= 0 && string.Compare(t.DateReference, nextweek) <= 0) &&
                                   t.UserReceiverID == id_user
                                  )
                            select t);
                }
                else
                {
                    temp = (from t in _edbcontext.References
                            where (
                                   t.UserReceiverID == id_user
                                  )
                            select t);

                }
            }
            if (ViewState["sortexpression"] != null)
            {
                switch (ViewState["sortexpression"].ToString().ToLower())
                {
                    case "letter_erja_id asc":
                        temp = temp.OrderBy(u => u.LetterID);
                        break;
                    case "letter_erja_id desc":
                        temp = temp.OrderByDescending(u => u.LetterID);
                        break;
                    case "moz_erja asc":
                        temp = temp.OrderBy(u => u.Subject);
                        break;
                    case "moz_erja desc":
                        temp = temp.OrderByDescending(u => u.Subject);
                        break;
                    case "dte_erja asc":
                        temp = temp.OrderBy(u => u.DateReference);
                        break;
                    case "dte_erja desc":
                        temp = temp.OrderByDescending(u => u.DateReference);
                        break;
                }
            }
            gvdata.DataSource = temp.ToList();
            gvdata.DataBind();
        }
        /// <summary>
        /// متدی برای پر کردن لیست کشویی فیلدها
        /// </summary>
        protected void setddl_search_field()
        {
            ddl_search_field.Items.Insert(0, new ListItem("انتخاب شود", "-1"));
            ddl_search_field.Items.Insert(1, new ListItem("شماره اندیکاتور", "letter_erja_id"));
            ddl_search_field.Items.Insert(2, new ListItem("موضوع ارجاع", "moz_erja"));
            ddl_search_field.Items.Insert(3, new ListItem("فرستنده", "[tbl_user_fer.lname_user]"));
            ddl_search_field.Items.Insert(4, new ListItem("تاریخ ارجاع", "dte_erja"));
            ddl_search_field.SelectedIndex = 0;
        }
        /// <summary>
        /// متدی برای پر کردن لیست کشویی گزینه های مقایسه ای
        /// </summary>
        protected void setddl_search_option()
        {
            ddl_search_option.Items.Insert(0, new ListItem("انتخاب شود", "-1"));
            ddl_search_option.Items.Insert(1, new ListItem("شامل", "LIKE"));
            ddl_search_option.Items.Insert(2, new ListItem("برابر", "="));
            ddl_search_option.Items.Insert(3, new ListItem("نابرابر", "<>"));
            ddl_search_option.Items.Insert(4, new ListItem("کوچکتراز", "<"));
            ddl_search_option.Items.Insert(5, new ListItem("کوچکترمساوی", "<="));
            ddl_search_option.Items.Insert(6, new ListItem("بزرگتراز", ">"));
            ddl_search_option.Items.Insert(7, new ListItem("بزرگترمساوی", ">="));
            //ddl_search_option.Items.Insert(0, new ListItem("شروع با", "7"));
            //ddl_search_option.Items.Insert(0, new ListItem("شروع نشود با", "8"));
            //ddl_search_option.Items.Insert(0, new ListItem("اتمام با", "9"));
            //ddl_search_option.Items.Insert(0, new ListItem("تمام نشود با", "10"));
            //ddl_search_option.Items.Insert(0, new ListItem("نباشد حاوی", "11"));
            //ddl_search_option.Items.Insert(0, new ListItem("خالی باشد", "12"));
            //ddl_search_option.Items.Insert(0, new ListItem("خالی نباشد", "13"));
            //ddl_search_option.Items.Insert(0, new ListItem("عضو این باشد", "14"));
            //ddl_search_option.Items.Insert(0, new ListItem("عضو این نباشد", "15"));
            ddl_search_option.SelectedIndex = 0;
        }
        /// <summary>
        /// متدی برای پر کردن لیست کشویی ترکیب جستجو ها
        /// </summary>
        protected void setddl_search_combine()
        {
            ddl_search_combine.Items.Insert(0, new ListItem("انتخاب شود", "-1"));
            ddl_search_combine.Items.Insert(1, new ListItem("و", "AND"));
            ddl_search_combine.Items.Insert(2, new ListItem("یا", "OR"));
            ddl_search_combine.SelectedIndex = 0;
        }

        /// <summary>
        /// تابعی جهت انجام اعمال مورد نیاز در هنگام لود اولیه صفحه
        /// </summary>
        public void inittextweek()
        {
            if (Session["addday"] == null)
            {
                Session["addday"] = 0;
            }
            var DayRange = int.Parse(System.Configuration.ConfigurationManager.AppSettings["DayRange"]);
            var nextweek = PersianDateTime.Now.AddDays((int)Session["addday"]).ToString(PersianDateTimeFormat.Date);
            var lastweek = PersianDateTime.Now.AddDays(((int)Session["addday"]) - DayRange).ToString(PersianDateTimeFormat.Date);
            txt_nextweek.Attributes["onclick"] = "PersianDatePicker.Show(this,'" + nextweek + "');";
            txt_lastweek.Attributes["onclick"] = "PersianDatePicker.Show(this,'" + lastweek + "');";
            txt_lastweek.Attributes["onclick"] = "PersianDatePicker.Show(this,'" + lastweek + "');";

            settextweek();
        }
        public void settextweek()
        {
            if (Session["addday"] == null)
            {
                Session["addday"] = 0;
            }
            var DayRange = int.Parse(System.Configuration.ConfigurationManager.AppSettings["DayRange"]);
            //txt_lastweek.Text = MyObjects.myclass.shortshamsidate(DateTime.Now.AddDays((int)Session["addday"]));
            //txt_nextweek.Text = MyObjects.myclass.shortshamsidate(DateTime.Now.AddDays(((int)Session["addday"]) + 7));
            var nextweek = PersianDateTime.Now.AddDays((int)Session["addday"]).ToString(PersianDateTimeFormat.Date);
            var lastweek = PersianDateTime.Now.AddDays(((int)Session["addday"]) - DayRange).ToString(PersianDateTimeFormat.Date);
            txt_nextweek.Text = nextweek;
            txt_lastweek.Text = lastweek;

        }
        protected void initialstate()
        {
            hfield_Taed.Value = Convert.ToInt16(MyObjects.LetterKind.Taed).ToString();
            hfield_Bargasht.Value = Convert.ToInt16(MyObjects.LetterKind.Bargasht).ToString();
            gvdata.PageSize = int.Parse(System.Configuration.ConfigurationManager.AppSettings["PageSize"]);
            setddl_search_field();
            setddl_search_option();
            setddl_search_combine();
            Session["addday"] = 0;
            ViewState["search"] = "";
            inittextweek();
            SetGVdata(txt_lastweek.Text, txt_nextweek.Text);
        }
        [WebMethod]
        public static string get_erja_byid(string id_erja)
        {
            Int64 myid_erja = Int64.Parse(id_erja);
            DataBaseContext _edbcontext = new DataBaseContext();
            var query = from l in _edbcontext.References
                        where (l.ID == myid_erja)
                        select new
                        {
                            l.ID,
                            l.Subject,
                            l.LetterID,
                            l.UserSenderID,
                            l.UserReceiverID,
                            l.DateReference,
                            l.DateResponse
                        };
            var _erja = query.ToList();
            if (_erja != null)
            {
                JavaScriptSerializer s = new JavaScriptSerializer();
                s.MaxJsonLength = int.MaxValue;
                return s.Serialize(_erja);
            }
            return null;
        }

        [WebMethod]
        public static string sabt_req_letter_method(string id_erja, string letter_erja_id, string reqcode)
        {
            Int64 myid_letter = Int64.Parse(letter_erja_id);
            DataBaseContext _edbcontext = new DataBaseContext();
            var query = from l in _edbcontext.Letters
                        where (l.ID == myid_letter)
                        select l;
            var _letter = query.FirstOrDefault();
            if (_letter != null)
            {
                _letter.KindLetter = int.Parse(reqcode);
            }
            _edbcontext.SaveChanges();

            //sabt in tbl_erja
            Int64 myid_erja = Int64.Parse(id_erja);
            var queryerja = from l in _edbcontext.References
                            where (l.ID == myid_erja)
                            select l;
            var _erja = queryerja.FirstOrDefault();
            if (_erja != null)
            {
                _erja.DateResponse = MyObjects.myclass.shortshamsidate(DateTime.Now);
            }
            _edbcontext.SaveChanges();
            return letter_erja_id;
        }
        #endregion
        #region systemmethode
        protected void Page_Load(object sender, EventArgs e)
        {
            if ((Session["vorod"] != null))
            {
                if ((Session["vorod"].ToString() == "ok"))
                {
                    if ((int.Parse(Session["role_id"].ToString()) == (int)MyObjects.RolName.User) ||
                        (int.Parse(Session["role_id"].ToString()) == (int)MyObjects.RolName.SuperUser)
                        )
                    {
                        if (!IsPostBack)
                        {
                            //ScriptManager.GetCurrent(this.Page).RegisterPostBackControl();
                            initialstate();
                        }
                        else
                        {
                            SetGVdata(txt_lastweek.Text, txt_nextweek.Text);
                        }
                    }
                }
                else
                {
                    Response.Redirect("../login.aspx");
                }
            }
        }

        protected void gvdata_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            SetGVdata(txt_lastweek.Text, txt_nextweek.Text);
            gvdata.PageIndex = e.NewPageIndex;
            gvdata.DataBind();
            System.Threading.Thread.Sleep(500);
        }

        protected void gvdata_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "do_print_letter")
            {
                Int64 index = Convert.ToInt64(e.CommandArgument.ToString());
                Int64 myid_letter = index;
                DataBaseContext _edbcontext = new DataBaseContext();
                var query = from l in _edbcontext.Letters
                            where (l.ID == myid_letter)
                            select l;
                var _letter = query.FirstOrDefault();
                if (_letter != null)
                {
                    //unicode header
                    var orginal_header = "<html><head><meta http-equiv='content-type'content='text/html;charset=UTF-8'></head><body dir='rtl'>";
                    var orginal_footer = "</body></html>";
                    ///////////////////////////////////////set olgo//////////////////////////////////////////////
                    var page = from p in _edbcontext.Pages
                               where (p.ID == _letter.PageID)
                               select p;
                    var _page = page.FirstOrDefault();
                    if (_page != null)
                    {
                        //merge field replace
                        var page_header = _page.Header
                            .Replace("MYDATE", _letter.DateRegister.ToString())
                            .Replace("MYNUMBER", _letter.ID.ToString())
                            .Replace("MYATT", "ندارد")
                            ;

                        ///////////////////////////////////////set olgo//////////////////////////////////////////////
                        var html_content = orginal_header + page_header + _page.Message + _letter.Content + _page.Footer + orginal_footer;
                        var htmlToPdf = new NReco.PdfGenerator.HtmlToPdfConverter();
                        //page size
                        htmlToPdf.PageWidth = _page.Width;//210//148
                        htmlToPdf.PageHeight = _page.Height;//296//210
                        //margin
                        htmlToPdf.Margins.Top = _page.Margin_Top;
                        htmlToPdf.Margins.Bottom = _page.Margin_Bottom;
                        htmlToPdf.Margins.Right = _page.Margin_Right;
                        htmlToPdf.Margins.Left = _page.margin_left;
                        htmlToPdf.Orientation = (NReco.PdfGenerator.PageOrientation)_page.Orientation;
                        //header and footer
                        //htmlToPdf.PageHeaderHtml = header + _page.header;
                        //htmlToPdf.PageFooterHtml = _page.footer + footer;

                        //create pdf
                        var pdfBytes = htmlToPdf.GeneratePdf(html_content);

                        //open by browser or pdf reader
                        Response.Buffer = true;
                        Response.Cache.SetCacheability(HttpCacheability.NoCache);

                        this.Context.ApplicationInstance.CompleteRequest();
                        Response.Clear();
                        Response.ClearContent();
                        Response.ClearHeaders();
                        Response.ContentType = "application/pdf";
                        string filename = _letter.ID.ToString();
                        //Response.AddHeader("content-disposition", "attachment;filename=" + filename);
                        //Response.AddHeader("Content-Disposition", "attachment; filename="+filename+".pdf");
                        Response.AppendHeader("content-length", pdfBytes.Length.ToString());
                        Response.BinaryWrite(pdfBytes);
                        Response.Flush();
                        Response.Close();
                        Response.End();
                        //Response.TransmitFile(filename);
                        //System.Threading.Thread.Sleep(1000);
                        //Response.End();
                    }
                }
            }
        }

        protected void gvdata_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton linkbtn = (LinkButton)e.Row.FindControl("lbtn_print_letter");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                if (scriptManager != null && linkbtn != null)
                {
                    scriptManager.RegisterPostBackControl(linkbtn);
                }
            }
            if (Convert.ToInt16(DataBinder.Eval(e.Row.DataItem, "tbl_letter.kind_letter")) == (int)LetterKind.Taed)
            {
                e.Row.BackColor = System.Drawing.Color.PowderBlue;
            }
            if (Convert.ToInt16(DataBinder.Eval(e.Row.DataItem, "tbl_letter.kind_letter")) == (int)LetterKind.Bargasht)
            {
                e.Row.BackColor = System.Drawing.Color.Gray;
            }

        }

        protected void lbtn_lastweek_Click(object sender, EventArgs e)
        {
            if (Session["addday"] == null)
            {
                Session["addday"] = 0;
            }
            var DayRange = int.Parse(System.Configuration.ConfigurationManager.AppSettings["DayRange"]);
            System.Threading.Thread.Sleep(500);
            Session["addday"] = ((int)Session["addday"]) - DayRange;
            settextweek();
            SetGVdata(txt_lastweek.Text, txt_nextweek.Text);
        }

        protected void lbtn_nextweek_Click(object sender, EventArgs e)
        {
            if (Session["addday"] == null)
            {
                Session["addday"] = 0;
            }
            var DayRange = int.Parse(System.Configuration.ConfigurationManager.AppSettings["DayRange"]);
            System.Threading.Thread.Sleep(500);
            Session["addday"] = ((int)Session["addday"]) + DayRange;
            settextweek();
            SetGVdata(txt_lastweek.Text, txt_nextweek.Text);
        }
        protected void lbtn_okweek_Click(object sender, EventArgs e)
        {
            if (!MyObjects.myclass.PersianDateValid(txt_lastweek.Text) || !MyObjects.myclass.PersianDateValid(txt_nextweek.Text))
                return;
            else
            {
                System.Threading.Thread.Sleep(500);
                SetGVdata(txt_lastweek.Text, txt_nextweek.Text);
            }
        }

        protected void gvdata_Sorting(object sender, GridViewSortEventArgs e)
        {
            if (ViewState["sortdirection"] == null)
            {
                ViewState["sortdirection"] = "ASC";
            }
            if (ViewState["sortdirection"].ToString() == "ASC")
                ViewState["sortdirection"] = "DESC";
            else
                ViewState["sortdirection"] = "ASC";
            ViewState["sortexpression"] = e.SortExpression.ToString() + " " + ViewState["sortdirection"].ToString();
            //if(ViewState["search"]!=null && ViewState["search"].ToString()!=string.Empty)
            //    SetGVdata("","");
            //else
            SetGVdata(txt_lastweek.Text, txt_nextweek.Text);
        }

        //protected void lbtn_search_call_Click(object sender, EventArgs e)
        //{
        //    Session["backpage"] = "sabt_letter.aspx";
        //    Response.Redirect("search_letter.aspx");
        //}
        protected void lbtn_add_search_Click(object sender, EventArgs e)
        {
            if (ViewState["search"].ToString() != string.Empty)
            {
                string laststring = ViewState["search"].ToString().Substring(0, ViewState["search"].ToString().Length - 1).Split(' ').Last();
                if (laststring != "AND" && laststring != "OR")
                {
                    ViewState["search"] = "";
                    lbl_search_text.Text = "";
                }
            }

            if (ddl_search_combine.SelectedValue != "-1")
            {
                if (ddl_search_field.SelectedValue != "-1" && ddl_search_option.SelectedValue != "-1" && txt_search.Text != "")
                {
                    if (ddl_search_option.SelectedValue.ToString() == "LIKE")
                    {
                        ViewState["search"] = ViewState["search"] + ddl_search_field.SelectedValue.ToString() + " " + ddl_search_option.SelectedValue.ToString() + " " + "N'%" + txt_search.Text + "%'" + " " + ddl_search_combine.SelectedValue.ToString() + " ";
                    }
                    else
                    {
                        ViewState["search"] = ViewState["search"] + ddl_search_field.SelectedValue.ToString() + " " + ddl_search_option.SelectedValue.ToString() + " " + "N'" + txt_search.Text + "'" + " " + ddl_search_combine.SelectedValue.ToString() + " ";
                    }
                    lbl_search_text.Text = lbl_search_text.Text + ddl_search_field.SelectedItem.ToString() + " " + ddl_search_option.SelectedItem.ToString() + " " + "'" + txt_search.Text + "'" + " " + ddl_search_combine.SelectedItem.ToString() + " ";
                }
            }
            else
            {
                if (ddl_search_field.SelectedValue != "-1" && ddl_search_option.SelectedValue != "-1" && txt_search.Text != "")
                {
                    if (ddl_search_option.SelectedValue.ToString() == "LIKE")
                    {
                        ViewState["search"] = ViewState["search"] + ddl_search_field.SelectedValue.ToString() + " " + ddl_search_option.SelectedValue.ToString() + " " + "N'%" + txt_search.Text + "%'" + " ";
                    }
                    else
                    {
                        ViewState["search"] = ViewState["search"] + ddl_search_field.SelectedValue.ToString() + " " + ddl_search_option.SelectedValue.ToString() + " " + "N'" + txt_search.Text + "'" + " ";
                    }
                    lbl_search_text.Text = lbl_search_text.Text + ddl_search_field.SelectedItem.ToString() + " " + ddl_search_option.SelectedItem.ToString() + " " + "'" + txt_search.Text + "'" + " " + ddl_search_combine.SelectedItem.ToString() + " ";
                }
            }
            ddl_search_field.SelectedIndex = -1;
            ddl_search_option.SelectedIndex = -1;
            ddl_search_combine.SelectedIndex = -1;
            txt_search.Text = "";

        }

        /// <summary>
        /// پاک کردن جستجوی قبلی
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lbtn_search_clear_Click(object sender, EventArgs e)
        {
            if (ViewState["search"].ToString() != "")
            {
                ddl_search_field.SelectedIndex = -1;
                ddl_search_option.SelectedIndex = -1;
                ddl_search_combine.SelectedIndex = -1;
                txt_search.Text = "";
                ViewState["search"] = "";
                SetGVdata(txt_lastweek.Text, txt_nextweek.Text);
                lbl_search_text.Text = "";
                System.Threading.Thread.Sleep(500);
            }
        }

        #endregion

        protected void lbtn_search_Click(object sender, EventArgs e)
        {
            if (ViewState["search"].ToString() != string.Empty)
            {
                SetGVdata("", "");
                System.Threading.Thread.Sleep(500);

            }
        }
    }
}