﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Users/MasterPageUsers.Master" AutoEventWireup="true" CodeBehind="sabt_letter.aspx.cs" Inherits="dabir.Users.sabt_letter" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!---------------------------------------Display Errore Message-------------------------------------------->
    <script type="text/javascript">
        function checkShamsi(str) {
            //بدون در نظر گرفتن دورقمی بودن ماه و روز
            //var patt = /(13|14)([0-9][0-9])\/(((0?[1-6])\/((0?[1-9])|([12][0-9])|(3[0-1])))|(((0?[7-9])|(1[0-2]))\/((0?[1-9])|([12][0-9])|(30))))/g;
            var patt = /(13|14)([0-9][0-9])\/(((0[1-6])\/((0[1-9])|([12][0-9])|(3[0-1])))|(((0[7-9])|(1[0-2]))\/((0[1-9])|([12][0-9])|(30))))/g;
            var result = patt.test(str);
            if (result) {
                var pos = str.indexOf('/');
                var year = str.substring(0, pos);
                var nextPos = str.indexOf('/', pos + 1);
                var month = str.substring(pos + 1, nextPos);
                var day = str.substring(nextPos + 1);
                if (month == 12 && (year + 1) % 4 != 0 && day == 30) { // kabise = 1379, 1383, 1387,... (year +1) divides on 4 remains 0
                    result = false;
                }
                return result;
            }
        }
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(document).ready(function () {
                var myvalue1 = $("#<%=txt_lastweek.ClientID %>").val();
                var myvalue2 = $("#<%=txt_nextweek.ClientID %>").val();
                if (!checkShamsi(myvalue1) || !checkShamsi(myvalue2)) {
                    $("#messagetext").text("تاریخ وارد شده صحیح نیست");
                    $("#message").fadeIn().delay(3000).fadeOut();
                    //alert("تاریخ وارد شده معتبر نیست!");
                    return false;
                }
            });
        });
    </script>
    <%-- Taed Call    --%>
    <script type="text/javascript">
        // cal ajax methode for save data
        $(document).ready(function () {

            //Erja Dialog Call 
            //
            $(document).on("click", "[id*=lbtn_ok_letter]", function (e) {

                var mydata = ($(this).attr('data-id'));
                var myletter_erja_id = ($(this).attr('data-id'));

                $("[id*=hfid_erja]").val(mydata);
                $.ajax({
                    type: 'POST',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    url: 'sabt_letter.aspx/get_erja_byid',
                    data: "{" +
                        "'id_erja':'" + mydata + "'" +
                     "}",
                    async: false,
                    success: function (response) {
                        $.map($.parseJSON(response.d), function (item) {
                            myletter_erja_id = (item.letter_erja_id);
                            return true;
                        });
                    },
                    error: function () {
                        alert("خطای نا شناخته رخ داده است");
                        return false;
                    }
                });
                $.ajax({
                    type: 'POST',
                    contentType: "application/json; charset=utf-8",
                    url: 'sabt_letter.aspx/sabt_req_letter_method',
                    data: "{" +
                         "'id_erja':'" + mydata + "'," +
                         "'letter_erja_id':'" + myletter_erja_id + "'," +
                         "'reqcode':'" + $("#<%= hfield_Taed.ClientID %>").val() + "'" +
                     "}",
                    async: false,
                    success: function (response) {
                        if (response.d != '-1') {
                            alert("نامه با شماره اندیکاتور<< " + response.d + " >> تایید شد ");
                            var UpdatePanel1 = '<%=UpdatePanel1.ClientID%>';
                            if (UpdatePanel1 != null)
                                __doPostBack(UpdatePanel1, '');
                            return true;
                        }
                        else {
                            alert("خطا در ثبت داده ها");
                            return false;
                        }

                    },
                    error: function ()
                    { alert("خطا در ثبت داده ها"); return false; }
                });
                return false;
            });
        });
    </script>
    <%-- Bargasht Call    --%>
    <script type="text/javascript">
        // cal ajax methode for save data
        $(document).ready(function () {

            //Erja Dialog Call 
            //
            $(document).on("click", "[id*=lbtn_nok_letter]", function (e) {

                var mydata = ($(this).attr('data-id'));
                var myletter_erja_id = ($(this).attr('data-id'));

                $("[id*=hfid_erja]").val(mydata);
                $.ajax({
                    type: 'POST',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    url: 'sabt_letter.aspx/get_erja_byid',
                    data: "{" +
                        "'id_erja':'" + mydata + "'" +
                     "}",
                    async: false,
                    success: function (response) {
                        $.map($.parseJSON(response.d), function (item) {
                            myletter_erja_id = (item.letter_erja_id);
                            return true;
                        });
                    },
                    error: function () {
                        alert("خطای نا شناخته رخ داده است");
                        return false;
                    }
                });
                $.ajax({
                    type: 'POST',
                    contentType: "application/json; charset=utf-8",
                    url: 'sabt_letter.aspx/sabt_req_letter_method',
                    data: "{" +
                         "'id_erja':'" + mydata + "'," +
                         "'letter_erja_id':'" + myletter_erja_id + "'," +
                         "'reqcode':'" + $("#<%= hfield_Bargasht.ClientID %>").val() + "'" +
                     "}",
                    async: false,
                    success: function (response) {
                        if (response.d != '-1') {
                            alert("نامه با شماره اندیکاتور<< " + response.d + " >> برگشت داده شد ");
                            var UpdatePanel1 = '<%=UpdatePanel1.ClientID%>';
                            if (UpdatePanel1 != null)
                                __doPostBack(UpdatePanel1, '');
                            return true;
                        }
                        else {
                            alert("خطا در ثبت داده ها");
                            return false;
                        }

                    },
                    error: function ()
                    { alert("خطا در ثبت داده ها"); return false; }
                });
                return false;
            });
        });
        $(document).ready(function () {
            $("#ddl_gir_id_letter_out").select2();
            $("#ddl_fer_id_letter_in").select2();
            //$("#ddl_page_id_letter_in").select2();
            //$("#ddl_page_id_letter_out").select2();
            //$("#ddl_search_combine").select2();
            //$("#ddl_search_field").select2();
            //$("#ddl_search_option").select2();
            //$("#ddl_ugir_erja_id").select2();
        });
    </script>

    <asp:HiddenField ID="hfid_erja" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hfield_Taed" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hfield_Bargasht" runat="server" ClientIDMode="Static" />
    <%-- Start Grid view                        -------------%>
    <div class="container full-width">
        <!--------------------------------------------------Message Box--------------------------------------------->
        <div class="row">
            <asp:HiddenField ID="messagetexthf" runat="server" />
            <div class="col-sm-12" id="message" style="display: none">
                <div id="messagetext" class="alert-danger text-center">پیام خطا</div>
            </div>
        </div>
        <!--------------------------------------------------------------------------------------------------------->
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-info">
                    <div class="panel-body">
                        <div class="row">
                            <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <%-- بخش جستجو در سایت --%>
                                    <div id="search_panel" style="display: block; margin-left: 5px;">
                                        <div class="form-inline">
                                            <div class="form-group form-group-sm">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <asp:Label ID="lbl_search_text" runat="server" CssClass="alert-warning" Font-Size="Smaller"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <asp:DropDownList ID="ddl_search_field" runat="server" CssClass="form-control" Width="120px"></asp:DropDownList>
                                                        <asp:DropDownList ID="ddl_search_option" runat="server" CssClass="form-control" Width="120px"></asp:DropDownList>
                                                        <asp:TextBox ID="txt_search" runat="server" CssClass="form-control" Width="120px"></asp:TextBox>
                                                        <asp:DropDownList ID="ddl_search_combine" runat="server" CssClass="form-control" Width="120px"></asp:DropDownList>
                                                        <asp:LinkButton ID="lbtn_add_search" runat="server" CssClass="btn btn-primary btn-sm glyphicon glyphicon-plus" data-toggle="tooltip" data-placement="top" title="اضافه شدن به جستجو" OnClick="lbtn_add_search_Click"></asp:LinkButton>
                                                        <asp:LinkButton ID="lbtn_search" runat="server" CssClass="btn btn-primary btn-sm glyphicon glyphicon-search" data-toggle="tooltip" data-placement="top" title="جستجو" OnClick="lbtn_search_Click"></asp:LinkButton>
                                                        <asp:LinkButton ID="lbtn_search_clear" runat="server" CssClass="btn btn-danger btn-sm glyphicon glyphicon-remove" data-toggle="tooltip" data-placement="top" title="پاک کردن جستجو" OnClick="lbtn_search_clear_Click"></asp:LinkButton>
                                                        <%--<asp:LinkButton ID="lbtn_back" runat="server" CssClass="btn btn-warning btn-sm glyphicon glyphicon-backward" data-toggle="tooltip" data-placement="top" title="بازگشت به صفحه قبل" OnClick="lbtn_back_Click"></asp:LinkButton>--%>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <%-- بخش جستجو در سایت --%>
                                    <%-- بخش ثبت اطلاعات --%>
                                    <div id="sabt_panel" style="display: block; margin: 5px;">
                                        <div class="form-inline">
                                            <div class="form-group form-group-sm">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <asp:LinkButton ID="lbtn_lastweek" runat="server" CssClass="btn  btn-primary btn-sm glyphicon glyphicon-arrow-left" data-toggle="tooltip" data-placement="top" title="هفته قبل" OnClick="lbtn_lastweek_Click"></asp:LinkButton>
                                                        <asp:Label ID="lbl_lastweek" runat="server">از</asp:Label>
                                                        <asp:TextBox ID="txt_lastweek" runat="server" CssClass="form-control" Width="90px"></asp:TextBox>
                                                        <asp:Label ID="lbl_nextweek" runat="server">تا</asp:Label>
                                                        <asp:TextBox ID="txt_nextweek" runat="server" CssClass="form-control" Width="90px"></asp:TextBox>
                                                        <asp:LinkButton ID="lbtn_nextweek" runat="server" CssClass="btn  btn-primary btn-sm glyphicon glyphicon-arrow-right" data-toggle="tooltip" data-placement="top" title="هفته بعد" OnClick="lbtn_nextweek_Click"></asp:LinkButton>
                                                        <asp:LinkButton ID="lbtn_okweek" runat="server" CssClass="btn  btn-primary btn-sm glyphicon glyphicon-ok" data-toggle="tooltip" data-placement="top" title="تائید تاریخ" OnClick="lbtn_okweek_Click"></asp:LinkButton>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <%-- بخش ثبت اطلاعات --%>
                                    <asp:GridView ID="gvdata" runat="server" CellPadding="3" GridLines="Horizontal" AutoGenerateColumns="False"
                                        Width="100%" UseAccessibleHeader="False" DataKeyNames="id_erja" BackColor="White" BorderColor="#E7E7FF"
                                        BorderStyle="None" BorderWidth="1px" AllowPaging="True"
                                        OnPageIndexChanging="gvdata_PageIndexChanging" OnRowCommand="gvdata_RowCommand"
                                        OnRowDataBound="gvdata_RowDataBound"
                                        AllowSorting="True" OnSorting="gvdata_Sorting" PageSize="7">
                                        <AlternatingRowStyle BackColor="#F7F7F7" />
                                        <Columns>
                                            <asp:TemplateField HeaderText="شماره اندیکاتور" SortExpression="letter_erja_id">
                                                <ItemTemplate><%# (Eval("letter_erja_id").ToString())%></ItemTemplate>
                                                <ItemStyle Width="8%" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="موضوع ارجاع" SortExpression="moz_erja">
                                                <ItemTemplate><%#MyObjects.myclass.SubString(Eval("moz_erja").ToString(),30)%></ItemTemplate>
                                                <ItemStyle Width="25%" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="تاریخ ارجاع" SortExpression="dte_erja">
                                                <ItemTemplate><%# (Eval("dte_erja").ToString())%></ItemTemplate>
                                                <ItemStyle Width="4%" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="فرستنده ارجاع">
                                                <ItemTemplate><%#MyObjects.myclass.SubString(Eval("tbl_user_fer.fname_user").ToString() + " " +Eval("tbl_user_fer.lname_user").ToString(),20)%></ItemTemplate>
                                                <ItemStyle Width="15%" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="عملیات">
                                                <ItemTemplate>
                                                    <asp:LinkButton runat="server" ID="lbtn_ok_letter" CommandArgument='<%#Eval("id_erja")%>' CommandName="do_ok_letter" CssClass="btn btn-info btn-xs glyphicon glyphicon-ok" data-id='<%#Eval("id_erja")%>' data-toggle="tooltip" data-placement="top" title="تایید نامه" Width="30px" Enabled='<%#(Eval("tbl_letter.kind_letter").ToString()=="1" ||Eval("tbl_letter.kind_letter").ToString()=="2" ?false:true)%>'></asp:LinkButton>
                                                    <asp:LinkButton runat="server" ID="lbtn_nok_letter" CommandArgument='<%#Eval("id_erja")%>' CommandName="do_nok_letter" CssClass="btn btn-info btn-xs glyphicon glyphicon-remove" data-id='<%#Eval("id_erja")%>' data-toggle="tooltip" data-placement="top" title="عدم تایید نامه" Width="30px" Enabled='<%#(Eval("tbl_letter.kind_letter").ToString()=="1" || Eval("tbl_letter.kind_letter").ToString()=="2"?false:true)%>'></asp:LinkButton>
                                                    <asp:LinkButton runat="server" ID="lbtn_print_letter" CommandArgument='<%#Eval("id_erja")%>' CommandName="do_print_letter" CssClass="btn btn-info btn-xs glyphicon glyphicon-print" data-id='<%#Eval("id_erja")%>' data-toggle="tooltip" data-placement="top" title='<%# " پرینت با "+ Eval("tbl_letter.tbl_page.page_name") %>' Width="30px" Enabled='<%#(Eval("tbl_letter.kind_letter").ToString()=="2"?false:true)%>'></asp:LinkButton>
                                                </ItemTemplate>
                                                <ItemStyle Width="13%" />
                                            </asp:TemplateField>
                                        </Columns>
                                        <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
                                        <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" HorizontalAlign="Center" VerticalAlign="Middle" Font-Size="Small" />
                                        <PagerSettings Position="TopAndBottom" />
                                        <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Center" VerticalAlign="Middle" Font-Bold="true" CssClass="mypager" />
                                        <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Center" VerticalAlign="Middle" Font-Size="small" Font-Bold="true" Height="30px" />
                                        <SelectedRowStyle BackColor="#738A9C" ForeColor="#F7F7F7" Font-Bold="True" />
                                        <SortedAscendingCellStyle BackColor="#F4F4FD" />
                                        <SortedAscendingHeaderStyle BackColor="#5A4C9D" />
                                        <SortedDescendingCellStyle BackColor="#D8D8F0" />
                                        <SortedDescendingHeaderStyle BackColor="#3E3277" />
                                    </asp:GridView>
                                </ContentTemplate>
                                <Triggers>
                                </Triggers>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%-- End Grid view                          -------------%>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0px; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <%--<span style="border-width: 0px; position: fixed; padding: 50px; background-color: #FFFFFF; font-size: 36px; left: 40%; top: 40%;">Loading ...</span>--%>
                <%--<img alt="" src="../images/loader.gif" />--%>
                <div class="loader" style="top: 300px">Loading...</div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
