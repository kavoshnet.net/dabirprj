﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;

namespace DomainClass.DomainModel
{
    public class User
    {
        public User()
        {

        }

        [Key]
        public Int64 ID { get; set; }
        public string Fname { get; set; }
        public string Lname { get; set; }
        public string Shmeli { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }

        public Int64? RoleID { get; set; }
        //if use annotations without using  Fluent API:
        //[ForeignKey(nameof(RoleID))]
        public virtual Role Role { get; set; }

        public IList<Reference> ReferenceSenders { get; set; }
        public IList<Reference> ReferenceReceivers { get; set; }

        public string FullName
        {
            get
            {
                return Fname + " " + Lname;
            }
        }

    }
}
