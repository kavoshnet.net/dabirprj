﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Web;
namespace DomainClass.DomainModel
{
    public class Office
    {
        public Office()
        {

        }
        [Key]
        public Int64 ID { get; set; }
        public string NameOffice { get; set; }
        public virtual IList<Letter> LetterSenders { get; set; }
        public virtual IList<Letter> LetterReceivers { get; set; }

    }
}
