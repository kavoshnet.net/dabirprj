﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;

namespace DomainClass.DomainModel
{
    public class Letter
    {
        public Letter()
        {
            //Date_Register = MyClasses.PersianDate.Now();
            //UserID = 1;
            //SignatoryID = 1;
            //References = new List<Reference>();
        }
        [Key]
        public long ID { get; set; }
        public string Number { get; set; }
        public string DateLetter { get; set; }
        public string Subject { get; set; }
        public string DateRegister { get; set; }
        public string Content { get; set; }
        public long? Attribute { get; set; }
        public Int64 KindLetter { get; set; }

        public Int64? PageID { get; set; }
        //if use annotations without using  Fluent API:
        //[ForeignKey(nameof(PageID))]
        public virtual Page Page { get; set; }

        public Int64? SenderOfficeID { get; set; }
        //if use annotations without using  Fluent API:
        //[ForeignKey(nameof(SenderOfficeID))]
        public virtual Office SenderOffice { get; set; }

        public Int64? ReceiverOfficeID { get; set; }
        //if use annotations without using  Fluent API:
        //[ForeignKey(nameof(ReceiverOfficeID))]
        public virtual Office ReceiverOffice { get; set; }
        public IList<Reference> References { get; set; }

    }
}
