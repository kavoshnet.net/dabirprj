﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;

namespace DomainClass.DomainModel
{
    public class Reference
    {
        public Reference()
        {

        }
        [Key]
        public Int64 ID { get; set; }
        public string Subject { get; set; }
        public string DateReference { get; set; }
        public string DateResponse { get; set; }


        public Int64? LetterID { get; set; }
        //if use annotations without using  Fluent API:
        //[ForeignKey(nameof(LetterID))]
        public virtual Letter Letter { get; set; }

        public Int64? UserIDSender { get; set; }
        //if use annotations without using  Fluent API:
        //[ForeignKey(nameof(UserIDSender))]
        public virtual User UserSender { get; set; }

        public Int64? UserIDReceiver { get; set; }
        //if use annotations without using  Fluent API:
        //[ForeignKey(nameof(UserIDReceiver))]
        public virtual User UserReceiver { get; set; }
    }
}