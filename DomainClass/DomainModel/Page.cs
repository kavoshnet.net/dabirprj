﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
namespace DomainClass.DomainModel
{
    public class Page
    {
        public Page()
        {

        }
        [Key]
        public Int64 ID { get; set; }
        public string Name { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public int Orientation { get; set; }
        public int Margin_Top { get; set; }
        public int Margin_Bottom { get; set; }
        public int Margin_Right { get; set; }
        public int Margin_Left { get; set; }
        public string Header { get; set; }
        public string Message { get; set; }
        public string Body { get; set; }
        public string Footer { get; set; }
        public string BGPath { get; set; }
        public IList<Letter> Letters { get; set; }

    }
}
