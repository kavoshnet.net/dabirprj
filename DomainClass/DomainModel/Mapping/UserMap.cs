using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DomainClass.DomainModel.Mapping
{
    public class UserMap : EntityTypeConfiguration<User>
    {
        public UserMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.UserName)
                .IsRequired();

            this.Property(t => t.Password)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable(nameof(User));
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Fname).HasColumnName("Fname");
            this.Property(t => t.Lname).HasColumnName("Lname");
            this.Property(t => t.Shmeli).HasColumnName("Shmeli");
            this.Property(t => t.UserName).HasColumnName("UserName");
            this.Property(t => t.Password).HasColumnName("Password");
            this.Property(t => t.RoleID).HasColumnName("RoleID");

            // Relationships
            this.HasOptional(t => t.Role)
                .WithMany(t => t.Users)
                .HasForeignKey(d => d.RoleID);

        }
    }
}
