using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DomainClass.DomainModel.Mapping
{
    public class PageMap : EntityTypeConfiguration<Page>
    {
        public PageMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable(nameof(Page));
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Width).HasColumnName("Width");
            this.Property(t => t.Height).HasColumnName("Height");
            this.Property(t => t.Orientation).HasColumnName("Orientation");
            this.Property(t => t.Margin_Top).HasColumnName("Margin_Top");
            this.Property(t => t.Margin_Bottom).HasColumnName("Margin_Bottom");
            this.Property(t => t.Margin_Right).HasColumnName("Margin_Right");
            this.Property(t => t.Margin_Left).HasColumnName("Margin_left");
            this.Property(t => t.Header).HasColumnName("Header");
            this.Property(t => t.Message).HasColumnName("Message");
            this.Property(t => t.Body).HasColumnName("Body");
            this.Property(t => t.Footer).HasColumnName("Footer");
            this.Property(t => t.BGPath).HasColumnName("BGPath");
        }
    }
}
