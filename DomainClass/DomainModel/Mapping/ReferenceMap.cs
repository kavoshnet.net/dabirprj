using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DomainClass.DomainModel.Mapping
{
    public class ReferenceMap : EntityTypeConfiguration<Reference>
    {
        public ReferenceMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.DateReference)
                .IsFixedLength()
                .HasMaxLength(10);

            this.Property(t => t.DateResponse)
                .IsFixedLength()
                .HasMaxLength(10);

            // Table & Column Mappings
            ToTable(nameof(Reference));
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Subject).HasColumnName("Subject");
            this.Property(t => t.LetterID).HasColumnName("LetterID");
            this.Property(t => t.UserIDSender).HasColumnName("UserIDSender");
            this.Property(t => t.UserIDReceiver).HasColumnName("UserIDReceiver");
            this.Property(t => t.DateReference).HasColumnName("DateReference");
            this.Property(t => t.DateResponse).HasColumnName("DateResponse");

            // Relationships
            this.HasOptional(t => t.Letter)
                .WithMany(t => t.References)
                .HasForeignKey(d => d.LetterID).WillCascadeOnDelete(true);
            this.HasOptional(t => t.UserSender)
                .WithMany(t => t.ReferenceSenders)
                .HasForeignKey(d => d.UserIDSender);
            this.HasOptional(t => t.UserReceiver)
                .WithMany(t => t.ReferenceReceivers)
                .HasForeignKey(d => d.UserIDReceiver);

        }
    }
}
