using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DomainClass.DomainModel.Mapping
{
    public class LetterMap : EntityTypeConfiguration<Letter>
    {
        public LetterMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.DateLetter)
                .IsFixedLength()
                .HasMaxLength(10);

            this.Property(t => t.DateRegister)
                .IsFixedLength()
                .HasMaxLength(10);

            // Table & Column Mappings
            this.ToTable(nameof(Letter));
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Number).HasColumnName("Number");
            this.Property(t => t.DateLetter).HasColumnName("DateLetter");
            this.Property(t => t.Subject).HasColumnName("Subject");
            this.Property(t => t.SenderOfficeID).HasColumnName("SenderOfficeID");
            this.Property(t => t.ReceiverOfficeID).HasColumnName("ReceiverOfficeID");
            this.Property(t => t.PageID).HasColumnName("PageID");
            this.Property(t => t.DateRegister).HasColumnName("DateRegister");
            this.Property(t => t.Content).HasColumnName("Content");
            this.Property(t => t.Attribute).HasColumnName("Attribute");
            this.Property(t => t.KindLetter).HasColumnName("KindLetter");

            // Relationships
            this.HasOptional(t => t.SenderOffice)
                .WithMany(t => t.LetterSenders)
                .HasForeignKey(d => d.SenderOfficeID);
            this.HasOptional(t => t.ReceiverOffice)
                .WithMany(t => t.LetterReceivers)
                .HasForeignKey(d => d.ReceiverOfficeID);
            this.HasOptional(t => t.Page)
                .WithMany(t => t.Letters)
                .HasForeignKey(d => d.PageID);

        }
    }
}
