using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DomainClass.DomainModel.Mapping
{
    public class OfficeMap : EntityTypeConfiguration<Office>
    {
        public OfficeMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable(nameof(Office));
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.NameOffice).HasColumnName("NameOffice");
        }
    }
}
