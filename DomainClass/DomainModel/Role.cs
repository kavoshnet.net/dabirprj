﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Web;
namespace DomainClass.DomainModel
{
    public class Role
    {
        public Role()
        {

        }
        [Key]
        public Int64 ID { get; set; }
        public string RoleName { get; set; }
        public string RoleNameFa { get; set; }
        public Boolean Sabt_Sadere { get; set; }
        public Boolean Sabt_Varede { get; set; }
        public Boolean Virayeshe_Moshakhasat { get; set; }
        public Boolean Hazf { get; set; }
        public Boolean Virayeshe_Matn { get; set; }
        public Boolean Erja { get; set; }
        public Boolean Chap { get; set; }
        public Boolean Preview { get; set; }
        public Boolean Sabt_Name { get; set; }
        public Boolean Sabt_FerGir { get; set; }
        public Boolean Sabt_User { get; set; }
        public Boolean Sabt_Page { get; set; }
        public Boolean Action_Menu { get; set; }
        public Boolean Action_Letter { get; set; }
        public Boolean Edit_Referenced_Letter { get; set; }

        public IList<User> Users { get; set; }

    }
}
